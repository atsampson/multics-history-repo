/* Binder control file for bound_lisp_compiler_, the Multics LISP compiler */
/* Created 1/10/74 by David P. Reed */
/* Modified 1/16/74 by David A. Moon to make lisp_cg_utility_ accessible to the compiler */
/* Modified 75.01.29 for lap, which is bound in here since it uses part of the compiler */
/* Modified 10/09/82 by Richard Lamson to add Perprocess_Static */

Objectname:	bound_lisp_compiler_;

Perprocess_Static;

Addname:		lisp_compiler,
		lcp,
		lisp_cg_utility_,
		make_lisp_listing,
		mll,

		lcp_init_,		/* required because a lisp subroutine */
		lcp_cg_,
		lcp_semant_,
		lap,
		get_alm_op_,
		lap_,

		lcp_historian_;		/* Here there be genealogists */
Global:		delete;

Order:		lisp_compiler,
		lcp_semant_,
		lcp_cg_,
		lisp_cg_utility_,
		lcp_init_,
		make_lisp_listing,
		dump_lisp_code_,
		dump_lisp_instruction_,
		convert_sfl_,
		make_lisp_xref_,
		lap_,
		get_alm_op_,
		lcp_historian_;
/* Retain the definitions for the command entry points */

objectname:	lisp_compiler;
synonym:		lcp;

retain:		lisp_compiler,
		lcp,
		lap;



/* Because the binder does not handle the retain bit in definitions at this time, we must force all of the
   definitions in lisp-compiled object segments to be retained, since all have the retain bit on */

objectname:	lcp_cg_;

global:		retain;



objectname:	lcp_init_;

global:		retain;



objectname:	lcp_semant_;

global:		retain;



/* Retain the entry point of procedure called by the lisp code generator to output code */


objectname:	lisp_cg_utility_;

retain:		lisp_cg_utility_;

/* retain the listing package command interface and do the correct synonyming */

objectname:	make_lisp_listing;
synonym:		mll;
retain:		make_lisp_listing,
		mll;

objectname:	dump_lisp_instruction_;
synonym:		dump_lisp_binding_word_;

/* and retain lap_ due to aforementioned bug in binder */
objectname:	lap_;
global:		retain;

objectname:	get_alm_op_;		/* this is a kludge shouldn't have to be retained */
retain:		get_alm_op_;


/* Implement the compiler side of the historian function. */


objectname:	lcp_historian_;
 global:		retain;

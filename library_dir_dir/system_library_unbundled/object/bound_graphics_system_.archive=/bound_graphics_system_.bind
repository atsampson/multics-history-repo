/* ******************************************************
   *                                                    *
   *                                                    *
   * Copyright (c) 1972 by Massachusetts Institute of   *
   * Technology and Honeywell Information Systems, Inc. *
   *                                                    *
   *                                                    *
   ****************************************************** */

/*  Bindfile for Multics Graphics System */

Objectname:	bound_graphics_system_;
Perprocess_Static;		/* setup_graphics and run units don't mix */
Global:		delete;
Addname:		graphic_manipulator_, gm_,
		graphic_compiler_, gc_,
		graphic_operator_, go_,
		graphic_macros_, gmc_,
		setup_graphics, sg,
		remove_graphics, rg,
		list_pgs_contents, lpc,
		graphic_dim_,
		graphic_chars_,
		graphic_terminal_status_,
		graphic_code_util_,
		graphic_matrix_util_,
		graphic_element_length_,
		graphic_decompiler_,
		calcomp_compatible_subrs_, ccs_,
		gui_,
		plot_,
		graphic_error_table_;

Order:		PNOTICE_graphics,
		graphic_manipulator_,
		lsm_,
		lsm_sym_,
		lsm_fs_,
		graphic_compiler_,
		graphic_macros_,
		graphic_chars_,
		graphic_operator_,
		graphic_decompiler_,
		gui_,
		setup_graphics,
		graphic_code_util_,
		graphic_dim_,
		graphic_element_length_,
		graphic_terminal_status_,
		plot_,
		calcomp_compatible_subrs_,
		list_pgs_contents,
		graphic_error_table_;


/* User callable modules */

objectname:	graphic_manipulator_;
 synonym:		gm_;
 global:		retain;

objectname:	graphic_macros_;
 synonym:		gmc_;
 global:		retain;

objectname:	graphic_compiler_;
 synonym:		gc_, graphic_matrix_util_;
 global:		retain;

objectname:	graphic_operator_;
 synonym:		go_;
 global:		retain;

objectname:	plot_;
 global:		retain;

objectname:	calcomp_compatible_subrs_;
 synonym:		ccs_,
		where,		/* These are crocks, but must be here if users are to be able to use */
		scale;		/* links to the entries without explicit segname references, since */
				/* entrynames "where" and "scale" are duplicated in other components */
 global:		retain;

objectname:	gui_;
 global:		retain;

objectname:	graphic_chars_;
 global:		retain;

objectname:	graphic_code_util_;
 global:		retain;

objectname:	graphic_dim_;
 retain:		graphic_dim_attach,
		no_entry,
		not_setup;

objectname:	graphic_element_length_;
 retain:		graphic_element_length_;

objectname:	graphic_terminal_status_;
 global:		retain;

objectname:	graphic_decompiler_;
 retain:		graphic_decompiler_;


/* User commands */

objectname:	setup_graphics;
 synonym:		sg,
		remove_graphics,
		rg;
 retain:		setup_graphics,
		sg,
		remove_graphics,
		rg;

objectname:	list_pgs_contents;
 synonym:		lpc;
 retain:		list_pgs_contents,
		lpc;

/* lsm_ programs: special new-graphics versions */

objectname:	lsm_;
 global:		retain;

objectname:	lsm_sym_;
 global:		retain;

objectname:	lsm_fs_;
 global:		retain;


/* Miscellaneous */

objectname:	graphic_error_table_;
 global:		retain;

No_Table;

/* End of bindfile for Multics Graphics System */

/* ***********************************************************
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   * Copyright (c) 1972 by Massachusetts Institute of        *
   * Technology and Honeywell Information Systems, Inc.      *
   *                                                         *
   *********************************************************** */

/**** format: ind3,ll80,initcol6,indattr,^inddcls,dclind4,idind16	       */
/**** format: struclvlind2,^ifthenstmt,^ifthendo,^ifthen,^indnoniterdo       */
/**** format: ^inditerdo,^indnoniterend,^indthenelse,case,^indproc,^indend   */
/**** format: ^delnl,^insnl,comcol41,^indcom,^indblkcom,linecom,^indcomtxt   */

/*                                              _			       */
/*    _|_              |                         |		       */
/*     |      _      _ |    _            ___     |		       */
/*     |     / \    / \|   / \   \   /   ___\    |		       */
/*     |    (__/   (   |  (__/    \ /   /   |    |		       */
/*     \_    \_/    \_/|   \_/     V    \__/|   _|_		       */
/*                                                    -----		       */
/*							       */

/**** <<<<----- dcl_tedeval_.incl.pl1 tedeval_			       */
tedeval_:				/* process evaluations	       */
      proc (adb_p, ain_p, ain_l, buf_ptr, ams_p, ams_l, result, msg, code);
dcl (
    adb_p		ptr,		/* -> database		       */
    ain_p		ptr,		/* -> evaluation string	       */
    ain_l		fixed bin (21),	/*   length thereof		  [IN] */
				/*   amount used up		 [OUT] */
    buf_ptr	ptr,		/* -> buffer control block	       */
    ams_p		ptr,		/* -> matched string in \g{...}      */
				/*    null otherwise	       */
    ams_l		fixed bin (21),	/*  length of string in \g{...}      */
				/* <0 in \{...}, 0 otherwise	       */
    result	char (500) var,	/* output string, if any	       */
    msg		char (168) var,	/* error message, if any	       */
    code		fixed bin (35)	/* return code		       */
    )		parm;		/* ----->>>>		       */

/* stk(top) corresponds to the rightmost symbol in the production	       */
/*  (rule,alternative) being "applied".				       */

dcl 1 s1		like ls based (s1_ptr);
dcl 1 s2		like ls based (s2_ptr);
dcl 1 sr		like ls based (sr_ptr);
dcl (s1_ptr, s2_ptr, sr_ptr) ptr;

dcl ex_sw		bit (1);
dcl ch2		char (1);

dcl ind		fixed bin (21);
dcl cat_p		ptr;
dcl cat_l		fixed bin (21);
dcl 1 catv	based (cat_p),
      2 link	ptr,		/* pointer to next temporary	       */
      2 len	fixed bin (21),
      2 text	char (cat_l refer (catv.len));
dcl ii		fixed bin (21);
dcl lval_ptr	ptr;
dcl 1 val		based (lval_ptr),
      2 temp	ptr,		/* pointer to temp variable list     */
      2 version	fixed bin,
      2 avar	bit (18) aligned,
      2 spare	(123) bit (36) aligned,
      2 av	(-200:200) fixed bin (24),
      2 k		(-200:200) char (32) var,
      2 K		(-10:30) char (500) var,
      2 cata	area;
dcl nextab	bit (18);
dcl avar_len	fixed bin (21);
dcl avar_ptr	ptr;
dcl unary		bit (1);
dcl char16	char (16) var;
dcl 1 avar	based (avar_ptr),
      2 next	bit (18) aligned,
      2 name	char (16),
      2 type	fixed bin,
      2 num	fixed bin (35),
      2 txt_r	bit (18);		/* offset of catv if any	       */

dcl alb		fixed bin static internal init (-200),
    aub		fixed bin static internal init (200),
    klb		fixed bin static internal init (-200),
    kub		fixed bin static internal init (200),
    Klb		fixed bin static internal init (-10),
    Kub		fixed bin static internal init (30),
    ns_string	char (256) var,
    ns_num	fixed bin;
dcl define_area_	entry (ptr, fixed bin(35));
dcl iox_$put_chars entry (ptr, ptr, fixed bin (21), fixed bin (35));
dcl tedwhere_	entry (ptr);
dcl conc_sw	bit (1);

      code = 1;			/* FAILURE.		       */
      dbase_p = adb_p;
      bp = dbase.eval_p;
      lval_ptr = b.cur.sp;
      if (lval_ptr = null())
      then do;
         call tedget_segment_ (dbase_p, b.cur.sp, b.cur.sn);
         lval_ptr = b.cur.sp;
         val.version = 1;
         val.temp = null ();
         val.avar = "0"b;
         ai.version = area_info_version_1;
         ai.extend = "0"b;
         ai.zero_on_alloc = "1"b;
         ai.zero_on_free = "0"b;
         ai.dont_free = "0"b;
         ai.no_freeing = "0"b;
         ai.owner = dbase.tedname;
         ai.size = sys_info$max_seg_size - 8901;
         ai.areap = addr (cata);
         call define_area_ (addr (ai), code);
         if (code ^= 0)
         then do;
	  msg = "Error defining eval area.";
	  return;
         end;
dcl 1 ai		like area_info;
%include area_info;
dcl sys_info$max_seg_size fixed bin ext static;
      end;


      bp = buf_ptr;
      conc_sw = "1"b;
/****      IP = ain_p;		/* Point at the input.	       */
/****      ti = 1;						       */
/****      te = ain_l;					       */
				/* Initialize variables.	       */
      do l = lbound (ls, 1) to hbound (ls, 1);
         ls.pt (l) = null ();
      end;
      ex_sw = "0"b;
      level = -1;
      lnl = 0;			/* where is last NL char	       */
      call ns_alt (ain_p, 1, ain_l);	/* setup level 0 execute string      */
      ind = 0;
      result = "";
      code = 0;
      unary = "0"b;
      if (substr (is, 1, 3) = "{?}")
      then do;
         ain_l = 3;
         call ioa_ ("Type ""help <eval>"" for more help");
         return;
      end;

%include ted_eval_p_;
%page;
scanner: proc returns (fixed bin (21));

dcl ret_val	fixed bin;	/* hold ret val during unary check   */

/*   Return one of the following encodings:
   0	EOI	End Of Input.
   1	<integer>	0->9...
   2	<string>	pl1 string.
   3	]	Array right bracket.
   4	,
   5	{
   6	}
   7	(
   8	)
   9	:
   10	;
   11	:=	Assignment.
   12	*	Multiply.
   13	/
   14	|	Mod.
   15	+
   16	-
   17	<
   18	>
   19	=
   20	<=
   21	>=
   22	^=
   23	a[	Array of numbers.
   24	k[	Array of short strings.
   25	K[	Array of long strings.
   26	Ks Kt	addressed string
   27	be	buffer end, last byte in buffer
   28	bn	buffer name
   29	fl	function, length
   30	fs	function, substr
   31	lb	line begin, first line addressed
   32	le	linne end, last line addressed
   33	sb	string begin, first byte addressed
   34	se	string end, last byte addressed
   35	da	dump a-var
   36	dk	dump k-var
   37	dK	dump K-var
   38	dn	directory name
   39	en	entry name
   40	sn	subfile name
   41	fak	function, a to k
   42	fka	function, k to a
   43	em	error message
   44	fi	function, index
   45	fir	function, index-reverse
   46	fv	function, verify
   47	fvr	function, verify-reverse
   48	ff	function, find
   49	ffr	function, find-reverse
   50	fln	function, linenumber
   51	sk	component kind
   52	J	special compare indicator
   53	Kl	line reference
   54	Kb	buffer reference
   55	if	if command
   56	ex	execute MACRO
   57	ag	number of arguments to ted
   58	cs	collate9() value
   59	<set>	set description
   60	pn	parameter number (% call)
   61	p[	parameter reference (% call)
   62	fmx	function, max
   63	fmn	function, min
   64	frs	function, rearrange string
   65	||	concatenate
   66	<var>	variable
   67     d (   	dump function
   68	mct (	match count (substitute)
   69	emt (	error message text
   70	emc ( 	error message code
   71	<u+>	unary plus
   72	<u->	unary minus
*/


MORE:
      ls.symptr (-la_put) = addr (ib (nc));
      ls.symlen (-la_put) = 0;
      ls.symbol (-la_put) = 0;
      ls.type (-la_put) = 0;
      ls.num (-la_put) = 0;
      ls.loc (-la_put) = 0;
      if (nc > te)			/* last char may not be a NL	       */
      then do;
         if (level = 0)
         then do;			/* no more input		       */
	  test_symbol = 0;
	  goto error;
         end;
         level = level - 1;
         IP = input.pt (level);
         lgnc = input.loc0 (level);
         nc = input.loc1 (level);
         te = input.loc2 (level);
         goto MORE;
      end;
      fc = nc;
      ret_val = val_mad (fixed (ib (nc), 9));
      nc = nc + 1;
      if (nc <= te)
      then ch2 = ic (nc);
      else ch2 = " ";
      i = verify (substr (is, nc - 1), azAZ09);
      char16 = substr (is, nc - 1, i - 1);
(subscriptrange): goto LS (ret_val);

LS (0):				/* Characters that are skipped. */
      if ic (fc) > " " then goto error;
      goto MORE;
LS (1):				/* <digit>. */
      k = nc - 1;
      ns_num = index ("0123456789", ic (nc - 1)) - 1;
      do nc = nc to te while (val_mad (fixed (ib (nc), 9)) = 1);
         ns_num = (10 * ns_num) + index ("0123456789", ic (nc)) - 1;
      end;
      ls.num (-la_put) = ns_num;
      ls.symlen (-la_put) = nc - k;
      return (1);

LS (2):				/* <string>. */
      j = te - nc + 1;
      ns_string = "";
      do while (j > 0);
         k = index (substr (is, nc, j), """");
         if k < 1 then j = 0;
         else do;
	  if k > 1 then ns_string = ns_string || substr (is, nc, (k - 1));
	  ls.symlen (-la_put) = k;
	  nc = nc + k;		/* The location of the char after ". */
	  if nc > te then return ((STRING_TYPE ()));
	  if ic (nc) = """"		/* "Internal" quote.	       */
	  then do;
	     ns_string = ns_string || """"; /* Catenate in one quote.      */
	     nc = nc + 1;
	     j = te - nc + 1;
	  end;
	  else return ((STRING_TYPE ()));
         end;
      end;
      msg = "Vmq) Missing "".";
      goto err_ret;

LS (3): return (3);			/* ] */

LS (4):				/* , */
LS (5):				/* { */
LS (7):				/* ( */
LS (10):				/* ; */
LS (19):				/* = */
unary_check:
      do nc = nc to te while (ic (nc) < "!");
      end;
      if (ic (nc) = "+") | (ic (nc) = "-")
      then unary = "1"b;
      return (ret_val);

LS (6): return (6);			/* } */
LS (8): return (8);			/* ) */

LS (9):				/* :. */
      if (ch2 = "=") then do; nc = nc + 1; ret_val = 11; end;
      goto unary_check;

LS (11):				/* p */
      if (char16 = "pn") then do; nc = nc + 1; return (60); end;
      ret_val = 61;			/* might be p[ */
      goto LS (24);


LS (12): return (12);		/* * */
LS (13): return (13);		/* / */

LS (14):				/* | */
      if ch2 = "|" then do; nc = nc + 1; return (65); end;
      return (14);

LS (15):				/* + */
LS (16):				/* - */
      if unary
      then do;
         unary = "0"b;
         ret_val = ret_val + 56;
      end;
      return (ret_val);

LS (17):				/* <. */
      if ch2 = "=" then do; nc = nc + 1; ret_val = 20; end;
      goto unary_check;

LS (18):				/* >. */
      if ch2 = "=" then do; nc = nc + 1; ret_val = 21; end;
      goto unary_check;

LS (21):				/* azAZ */
alpha:
      nc = nc + length (char16) - 1;
      ls.symlen (-la_put) = length (char16);
      nextab = val.avar;
      do avar_ptr = pointer (lval_ptr, nextab)
         repeat (pointer (lval_ptr, nextab))
         while (nextab ^= "0"b);
         if (avar.txt_r = "0"b)
         then cat_p = null ();
         else cat_p = pointer (lval_ptr, avar.txt_r);
         if (char16 = "abbreviations") & (avar.type = ABREV)
         then call ioa_ ("^8a  ^a", avar.name, catv.text);
         else if (char16 = avar.name)
         then do;
	  if (avar.type = ABREV)
	  then do;
	     call ns_alt (addr (catv.text), 1, catv.len);
	     goto MORE;
	  end;
	  ls.pt (-la_put) = avar_ptr;
	  return (66);		/* defined variable		       */
         end;
         nextab = avar.next;
      end;
      if (char16 = "abbreviations")
      then goto MORE;
      ls.pt (-la_put) = null ();
      return (66);			/* undefined var		       */

LS (22):				/* ^ */
      if ch2 = "=" then do; nc = nc + 1; goto unary_check; end;
      goto error;

LS (25):				/* K[ or Kt. */
      if (char16 = "Kt") then do; nc = nc + 1; return (26); end;
      if (char16 = "Ks") then do; nc = nc + 1; return (26); end;
      if (char16 = "Kl") then do; nc = nc + 1; return (53); end;
      if (char16 = "Kb") then do; nc = nc + 1; return (54); end;
      if ("0"b) then do;
LS (23):				/* a. */
         if (char16 = "ag") then do; nc = nc + 1; return (57); end;
      end;
LS (24):				/* k */
      do nc = nc to te while (ic (nc) < "!");
      end;
      if nc <= te then if ic (nc) = "["
	 then do;
	    nc = nc + 1;
	    goto unary_check;
	 end;
      goto alpha;

LS (27):				/* "b". */
      if (char16 = "be") then do; nc = nc + 1; return (27); end;
      if (char16 = "bn") then do; nc = nc + 1; return (28); end;
      goto alpha;

LS (28):				/* c */
      if (char16 = "cs") then do; nc = nc + 1; return (58); end;
      goto alpha;

dcl fxx		(14) char (03) int static init (
		"fl ", "fs ", "fak", "fka", "fi ", "fir", "fv ", "fvr",
		"ff ", "ffr", "fln", "fmx", "fmn", "frs");
dcl fvv		(14) fixed bin int static init (
		00029, 00030, 00041, 00042, 00044, 00045, 00046, 00047,
		00048, 00049, 00050, 00062, 00063, 00064);
LS (29):				/* "f". */
      do i = 1 to 14;
         if (char16 = fxx (i))
         then do;
	  k = fvv (i);
test_for_paren:
	  ii = nc + length (char16) - 1;
	  if (ic (ii) ^= "(")
	  then goto alpha;
	  nc = ii;
	  return (k);
         end;
      end;
      goto alpha;

LS (31):				/* "l". */
      if (char16 = "lb") then do; nc = nc + 1; return (31); end;
      if (char16 = "le") then do; nc = nc + 1; return (32); end;
      goto alpha;

LS (33):				/* "s". */
      if (char16 = "sb") then do; nc = nc + 1; return (33); end;
      if (char16 = "se") then do; nc = nc + 1; return (34); end;
      if (char16 = "sn") then do; nc = nc + 1; return (40); end;
      if (char16 = "sk") then do; nc = nc + 1; return (51); end;
      goto alpha;

LS (35):				/* "d". */
      if (char16 = "da") then do; nc = nc + 1; return (35); end;
      if (char16 = "dk") then do; nc = nc + 1; return (36); end;
      if (char16 = "dK") then do; nc = nc + 1; return (37); end;
      if (char16 = "dn") then do; nc = nc + 1; return (38); end;
      k = 67;
      goto test_for_paren;

LS (36):				/* "e". */
      if (char16 = "en") then do; nc = nc + 1; return (39); end;
      if (char16 = "em") then do; nc = nc + 1; return (43); end;
/**** if (char16 = "ex") then do; nc = nc + 1; return (56); end;	       */
      if (char16 = "emt") then do; k = 69; goto test_for_paren; end;
      if (char16 = "emc") then do; k = 70; goto test_for_paren; end;
      goto alpha;

LS (37):				/* J */
      if (char16 = "J") then return (52);
      goto alpha;

LS (38):				/* i */
      if (char16 = "if") then do; nc = nc + 1; return (55); end;
      goto alpha;

LS (39):				/* S */
      if (ch2 ^= "(") then goto alpha;
      ls.mask (-la_put) = "0"b;
      ls.symlen (-la_put) = index (substr (is, nc - 1, te - nc), ")");
      nc = nc + 1;
      do while ("1"b);

/*	A alphabetic	a->z_A->Z
   N numeric		0->9
   U upper case	A->Z
   L lower case	a->z
   M carriage motion	BSP HT NL VT FF SP
   O octal		0->7
   X hex		0->9a->fA->F
   g graphic		!->~
*/
         k = index ("ANULMOXGA)anulmoxga)", ic (nc)); /* the last A is for future expansion */
         if (k = 0)
         then goto error;
         if (k > 10)
         then k = k - 10;
         nc = nc + 1;
         if (k = 10)
         then return (59);
         substr (ls.mask (-la_put), k, 1) = "1"b;
      end;

/* NEVER gets here */

LS (40):				/* m */
      if (char16 = "mct") then do; k = 68; goto test_for_paren; end;
      goto alpha;


dcl azAZ09	char (63) int static init (
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz");
dcl (k, j)	fixed bin (21);
dcl val_mad	(0:511) fixed bin (8) unaligned static internal init (

/* "mad" array initialized to:
   '000'777	0
   09	1
   "	2
   ]	3
   ,	4
   {	5
   }	6
   (	7
   )	8
   :	9
   ;	10
   *	12
   /	13
   |	14
   +	15
   -	16
   <	17
   >	18
   =	19
   az	21
   AZ	21
   ^	22
   a	23
   k	24
   K	25
   b	27
   c	28
   f	29
   l	31
   p	11
   s	33
   d	35
   e	36
   J	37
   i	38
   S	39
   m	40

*/
		(34) 0, 2, (5) 0, 7, 8, 12, 15, 4, 16, 0, 13, (10) 1, 9,
		10, 17, 19, 18, (2) 0, (9) 21, 37, 25, (7) 21, 39, (7) 21,
		(2) 0, 3, 22, (2) 0, 23, 27, 28, 35, 36, 29, (2) 21, 38,
		21, 24, 31, 40, (2) 21, 11, 21, 21, 33, (7) 21, 5, 14, 6,
		(386) 0);
ns_alt: entry (ipt, ilc, iln);

dcl ipt		ptr,		/* pointer to command string */
    ilc		fixed bin (21),	/* beginning location */
    iln		fixed bin (21);	/* length */

      if (level = 5)
      then do;
         ain_l = input.loc1 (1);
         msg = "Vlv) Evaluation depth > 5.";
         goto err_ret;
      end;
      if (level >= 0)
      then do;
         input.pt (level) = IP;
         input.loc0 (level) = lgnc;
         input.loc1 (level) = nc;
         input.loc2 (level) = te;
      end;
      level = level + 1;
      input.pt (level), IP = ipt;
      input.loc0 (level), lgnc,
         input.loc1 (level), nc = ilc;
      input.loc2 (level), te = ilc + iln - 1;
      return;
   end scanner;
dcl level		fixed bin (21);
dcl 1 input	(0:5),
    2 pt ptr,
    2 loc0 fixed bin (21),
    2 loc1 fixed bin (21),
    2 loc2 fixed bin (21);



%include ted_eval_;
%include ted_eval_t_;
%page;
/* . . . cka . . . */
cka: proc (i) returns (fixed bin (21));
				/* Check "i" as a valid index for    */
				/*  the "a" array.		       */
      if (i < alb) | (i > aub)
      then do;
         msg = "Vsa) Subscript not in a[-200:200].";
         goto err_ret;
      end;
      return (i);
dcl i		fixed bin (21) parm;
   end cka;


/* . . . ckk . . . */
ckk: proc (i) returns (fixed bin (21));
				/* Check "i" as a valid index for    */
				/*  the "k" array.		       */
      if (i < klb) | (i > kub)
      then do;
         msg = "VSk) Subscript not in k[-200:200].";
         goto err_ret;
      end;
      return (i);
dcl i		fixed bin (21) parm;
   end ckk;


/* . . . ckK . . . */
ckK: proc (i) returns (fixed bin (21));
				/* Check "i" as a valid index for    */
				/*  the "K" array.		       */
      if (i < Klb) | (i > Kub)
      then do;
         msg = "VsK) Subscript not in K[-10:10].";
         goto err_ret;
      end;
      return (i);
dcl i		fixed bin (21) parm;
   end ckK;
%page;
/* . . . STRING_TYPE . . . */
STRING_TYPE: proc returns (fixed bin);

dcl hold_string	char (20);

      hold_string = ns_string;
				/* Evaluate the string. */
      if nc > te then return (2);	/* <string>. */
      goto typ (index ("xXoO", substr (is, nc, 1)));
typ (0): return (2);		/* <string>. */
typ (1):
typ (2):				/* <hexvalue>. */
      nc = nc + 1;			/* Skip the "x". */
      rn = 9;			/* location of the right hex field.  */
      ns_num = 0;
				/* Proliferate first bit left. */
      if length (ns_string) > 0
      then if substr (hold_string, 1, 1) > "7"
	 then unspec (ns_num) = (36)"1"b;
      do i = length (ns_string) to 1 by -1; /* Assign right to left. */
         j = index ("0123456789ABCDEFabcdef", substr (hold_string, i, 1));
         if j < 1
         then do;
	  msg = "Vbx) Bad hex digit, """;
	  msg = msg || ns_string;
	  msg = msg || """";
	  goto err_ret;
         end;
         if j > 16
         then j = j - 6;		/* Adjust for lower case letters. */
         addr (ns_num) -> hex (rn) = hexv (j);
         rn = rn - 1;
      end;
      ls.num (-la_put) = ns_num;
      return (1);			/* <integer> */
typ (3):
typ (4):				/* <octvalue>. */
      nc = nc + 1;			/* Skip the "o". */
      rn = 12;			/* location of the right oct field.  */
      ns_num = 0;
				/* Proliferate first bit left. */
      if length (ns_string) > 0
      then if substr (hold_string, 1, 1) > "3"
	 then unspec (ns_num) = (36)"1"b;
      do i = length (ns_string) to 1 by -1; /* Assign right to left. */
         j = index ("01234567", substr (hold_string, i, 1));
         if j < 1
         then do;
	  msg = "Vbo) Bad octal digit. """;
	  msg = msg || ns_string;
	  msg = msg || """";
	  goto err_ret;
         end;
         addr (ns_num) -> oct (rn) = octv (j);
         rn = rn - 1;
      end;
      ls.num (-la_put) = ns_num;
      return (1);			/* <integer> */

dcl (i, j,
    rn)		fixed bin (21),
    hex		(9) bit (4) based,
    hexv		(16) bit (4) static internal init (
		"0000"b, "0001"b, "0010"b, "0011"b,
		"0100"b, "0101"b, "0110"b, "0111"b,
		"1000"b, "1001"b, "1010"b, "1011"b,
		"1100"b, "1101"b, "1110"b, "1111"b),
    oct		(12) bit (3) based,
    octv		(8) bit (3) static internal init (
		"000"b, "001"b, "010"b, "011"b,
		"100"b, "101"b, "110"b, "111"b);
   end STRING_TYPE;
%page;
/* . . . Global declarations . . . */
dcl
    (ioa_$ioa_switch,
     ioa_$nnl)	entry options (variable),

/* Input files. */
    IP		ptr,
    te		fixed bin (24),
    is		char (te) aligned based (IP),
    1 CHAR_ARRAY	aligned based (IP),
      2 ic	(te) char (1) unaligned,
    1 BIT_ARRAY	aligned based (IP),
      2 ib	(te) bit (9) unaligned;

dcl (abs, addrel, char, collate9, divide, hbound, index, lbound, length, 
    ltrim, max, min, null, pointer, rel, reverse, search, string, substr,
    unspec, verify
    )		builtin;

/* Declaration of Automatic data. */
dcl fc		fixed bin (21);
dcl l		fixed bin (21);
dcl lgnc		fixed bin (21);
dcl lnl		fixed bin (21);
dcl nc		fixed bin (21);

%include tedcommon_;
%include tedbase;
%include tedbcb;
%include tedstk;
dcl tedget_segment_ entry (		/* get a segment to work in	       */
		ptr,		/* -> database		       */
		ptr,		/* -> gotten segment	 [OUT] */
		fixed bin,	/* sequence # of it         [IN/OUT] */
				/* if >0 upon entry, it will then    */
				/*  fill that entry in seg_p array   */
				/* otherwise it will take any one    */
		);


dcl tedcount_lines_ entry (		/* return # lines in string	       */
		ptr,		/* -> buffer in which to count       */
		fixed bin (21),	/* where string begins in segment    */
		fixed bin (21),	/* where string ends in segment      */
		fixed bin (21)	/* # lines		 [OUT] */
		);


dcl db_sw bit (1) int static init ("0"b);
dbn: entry; db_sw = "1"b; return;
dbf: entry; db_sw = "0"b; return;
   end tedeval_;

/*
   Change: R.J.C. Kissel to remove		
   references to gcos_cv_gebcd_ascii_ (which was	
   moved to sss and named bcd_to_ascii_).	
  					
   Change: Dave Ward	07/30/81 9.0 modules
   Change: Ron Barstad        11/15/82  add gcos_ids2_concur_
*/

Objectname:	bound_gcos_;

Addname:		gc,
		gcos,
		gcos_cc_endjob_,
		gcos_control_tables_,
		gcos_cv_ascii_gebcd_,
		gcos_cv_ascii_gebcd_check_,
		gcos_cv_printline_gebcd_,
		gcos_ext_stat_,
		gcos_ids2_concur_,
		gcos_mme_bort_;

Order:		PNOTICE_gcos,
		gcos,
		gcos_abort_,
		gcos_attach_file_,
		gcos_build_pathname_,
		gcos_cc_abort_,
		gcos_cc_activity_cards_,
		gcos_cc_data_,
		gcos_cc_directive_cards_,
		gcos_cc_endjob_,
		gcos_cc_file_cards_,
		gcos_cc_goto_,
		gcos_cc_ident_,
		gcos_cc_incode_,
		gcos_cc_limits_,
		gcos_cc_loader_cards_,
		gcos_cc_misc_cards_,
		gcos_cc_param_,
		gcos_cc_set_,
		gcos_cc_snumb_,
		gcos_cc_update_,
		gcos_check_for_mme_,
		gcos_cleanup_files_,
		gcos_close_file_,
		gcos_control_tables_,
		gcos_cv_ascii_gebcd_,
		gcos_error_,
		gcos_et_,
		gcos_ext_stat_,
		gcos_fault_processor_,
		gcos_fms_error_,
		gcos_gein_,
		gcos_gein_pass1_,
		gcos_get_bar_,
		gcos_get_cc_field_,
		gcos_gtss_update_,
		gcos_ids2_concur_,
		gcos_incode_,
		gcos_interpret_file_string_,
		gcos_mme_bort_,
		gcos_mme_call_,
		gcos_mme_chek_,
		gcos_mme_fadd_,
		gcos_mme_fcon_,
		gcos_mme_fini_,
		gcos_mme_frce_,
		gcos_mme_fsye_,
		gcos_mme_info_,
		gcos_mme_inos_,
		gcos_mme_laps_,
		gcos_mme_lbar_,
		gcos_mme_loop_,
		gcos_mme_more_,
		gcos_mme_mrel_,
		gcos_mme_prio_,
		gcos_mme_rels_,
		gcos_mme_rets_,
		gcos_mme_rout_,
		gcos_mme_save_,
		gcos_mme_sets_,
		gcos_mme_snap_,
		gcos_mme_snp1_,
		gcos_mme_syot_,
		gcos_mme_time_,
		gcos_mme_user_,
		gcos_mme_wake_,
		gcos_open_file_,
		gcos_print_call_,
		gcos_process_mme_,
		gcos_read_card_,
		gcos_restart_,
		gcos_restore_regs_,
		gcos_run_activity_,
		gcos_set_slave_,
		gcos_sysout_writer_,
		gcos_time_convert_,
		gcos_verify_access_,
		gcos_verify_tss_access_,
		gcos_write_,
		gcos_write_to_er_;

Global:		delete;

No_Table;

objectname:	gcos;
synonym:		gc;
global:		retain;

objectname:	gcos_attach_file_;
global:		retain;			/* retain debugging entries */

objectname:	gcos_cc_endjob_;		/* allow $ ENDJOB processor to be entered from command level
					after a fault */
global:		retain;

objectname:	gcos_close_file_;
global:		retain;			/* retain debugging entries */

objectname:	gcos_control_tables_;
global:		retain;			/* used by gcos_utility_ */

objectname:	gcos_cv_ascii_gebcd_;
synonym:		gcos_cv_ascii_gebcd_check_,
		gcos_cv_printline_gebcd_;
global:		retain;			/* used lots of places, for conversions */


objectname:	gcos_ext_stat_;
global:		retain;			/* to provide access, for debug routines */

objectname:	gcos_ids2_concur_;		/* to make available to gtss_mcfc_ */
global:		retain;

objectname:	gcos_mme_bort_;		/* allow MME GEBORT to be simulated from command level,
					after a fault */
global:		retain;

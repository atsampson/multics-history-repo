/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

comp_format_ctls_:
  proc (ctl_index);

/* PARAMETERS */

    dcl ctl_index	   fixed bin;

/* LOCAL STORAGE 
				   text alignment flags */
    dcl align_flags	   (6) bit (6) aligned static options (constant)
		   init ("000001"b, "000100"b, "100000"b, "001000"b,
		   "010000"b, "000010"b);
    dcl align_mode	   char (32) var;
    dcl col_depth_adj  fixed bin (31);
    dcl ercd	   fixed bin (35);	/* error code */
				/* debug message for exit */
    dcl exit_str	   char (128) var init ("");
    dcl fnxt	   fixed bin (21);	/* next variable field char */
    dcl head_used	   fixed bin (31);
    dcl hf_needed	   fixed bin (31);	/* extra header/footer space needed */
    dcl hscales	   (7) fixed bin (31) static options (constant)
		   init (7200, 6000, 72000, 2834.65, 12000, 1000, 0);
    dcl (i, j, k)	   fixed bin;	/* working index */
    dcl locolptr	   ptr;		/* for local reference */
    dcl 1 locol	   aligned like col based (locolptr);
    dcl maxcolusd	   fixed bin (31);	/* for setting multicolumn data */
    dcl min_val	   fixed bin (31);	/* minimum page length required */
    dcl net_line	   fixed bin (31);	/* net line for setting margins */
    dcl page_width	   fixed bin (31);	/* width for column specs */
    dcl save_index	   fixed (35);	/* to save ctl.index around calls */
    dcl tab_char	   char (1);	/* htab character */
    dcl tab_name	   char (32);	/* name of horizontal tab pattern */
    dcl unscaled	   (1) fixed bin (31) static options (constant) init (1);
				/* copy of control line variable field */
    dcl varfld	   char (1020) var;
    dcl vscales	   (7) fixed bin (31) static options (constant)
		   init (12000, 9000, 72000, 2834.65, 12000, 1000, 0);

    dcl comp_error_table_$inconsistent
		   ext fixed bin (35);
    dcl comp_error_table_$limitation
		   ext fixed bin (35);
    dcl comp_error_table_$missing_delimiter
		   ext fixed bin (35);
    dcl comp_error_table_$syntax_error
		   ext fixed bin (35);

    dcl (index, length, ltrim, max, min, size)
		   builtin;

    dcl ioa_$rsnnl	   entry options (variable);

    if shared.bug_mode
    then call ioa_ ("format_ctls: (^d) ""^a""", ctl_index, ctl_line);

    maxcolusd = 0;
    varfld = substr (ctl_line, ctl.index);

    if shared.table_mode
    then
      do;
        tblfmtptr = tbldata.fmt (tbldata.ndx).ptr;
        tblcolptr = tblfmt.colptr (tblfmt.ccol);
      end;

    goto ctl_ (ctl_index);

/* TEXT ALIGNMENT CONTROLS
   both      center    inside    left      outside   right
   ".alb"    ".alc"    ".ali"    ".all"    ".alo"    ".alr"  */
ctl_ (1):
ctl_ (2):
ctl_ (3):
ctl_ (4):
ctl_ (5):
ctl_ (6):
    if shared.blkptr ^= null ()	/* change mode in current block */
    then
      do;
        if ^shared.ftn_mode		/* not a footnote */
				/* or not first footnote line */
	   | (shared.ftn_mode & text.hdr.count > 0)
				/* clean up leftovers */
        then if length (text.input_line) > 0
	   then call comp_break_ (format_break, 0);

        if text.hdr.colno >= 0	/* if not a loose block */
	   | option.galley_opt	/* or in galley mode */
        then current_parms.quad = align_flags (ctl_index);
        text.parms.quad, text.input.quad = align_flags (ctl_index);
      end;

    else if ^current_parms.title_mode
    then current_parms.quad = align_flags (ctl_index);

    if shared.bug_mode
    then
      do;
        align_mode = "%AlignMode%";
        call comp_use_ref_ (align_mode, "0"b, "0"b, addr (ctl.info));
        call ioa_$rsnnl ("align=^a", exit_str, 0, align_mode);
      end;

    goto return_;

ctl_ (74):			/* ".fi" = fill_mode ->  DEFAULT */
    goto fin_ctl;

ctl_ (75):			/* ".fif" = fill-off */
    if shared.blkptr ^= null		/* is there an active block? */
    then
      do;				/* clean up any leftovers */
        if text.input_line ^= ""
        then call comp_break_ (format_break, 0);
        text.parms.fill_mode = "0"b;
        if text.hdr.colno >= 0	/* if block isnt loose */
        then			/* set shared parm */
	   current_parms.fill_mode = "0"b;

        if ctl.index > length (ctl_line)
        then text.hdr.nofill_count = -1;
        else text.hdr.nofill_count =
	        comp_read_$number (ctl_line, unscaled, ctl.index, ctl.index,
	        addr (ctl.info), 0);
      end;
    else current_parms.fill_mode = "0"b;/* set shared parm */

    if ctl.index > length (ctl_line)
    then text_header.nofill_count = -1;
    else text_header.nofill_count =
	    comp_read_$number (ctl_line, unscaled, ctl.index, ctl.index,
	    addr (ctl.info), 0);

    goto return_;

/* ".fin" = fill-on */
ctl_ (76):
fin_ctl:				/* fin_ctl_index */
    if ^option.nofill_opt		/* if fill mode is not disabled */
    then
      do;
        if shared.blkptr ^= null ()	/* if there is an active block */
        then if ^text.parms.fill_mode	/* if not already filling */
	   then
	     do;
	       if length (text.input.ptr -> txtstr) > 0
				/* clean up leftovers */
	       then call comp_break_ (format_break, 0);
	       text.parms.fill_mode = "1"b;
	       if text.hdr.colno >= 0 /* if block isnt loose */
	       then current_parms.fill_mode = "1"b;

	     end;
	   else ;			/**/
				/* set shared parm */
        else current_parms.fill_mode = "1"b;
      end;
    goto return_;

ctl_ (101):			/* ".htd" = horizontal-tabs-define */
    if varfld = ""			/* if nothing given */
    then
      do;				/* and there are some defined */
        if shared.htab_ptr ^= null ()
        then htab.count = 0;		/* cancel them all */
        goto return_;
      end;

    tab_name =			/* fetch the pattern name */
         comp_read_$name (varfld, 1, fnxt, addr (ctl.info));
    if tab_name = ""		/* if no name */
    then goto return_;

    varfld = ltrim (substr (varfld, fnxt));

    if shared.htab_ptr = null ()	/* allocate htab data */
    then
      do;
        shared.htab_ptr = allocate (const.local_area_ptr, size (htab));
        htab.count, htab.pats = 0;	/* initialize pattern count */
        htab.chars = "";		/* and clear chars */
				/* initialize the patterns */
        htab.pattern.name, htab.pattern.fill = "";
        htab.pattern.count, htab.pattern.stop = 0;
      end;

    do i = 1 to htab.count		/* see if this one exists */
         while (tab_name ^= htab.pattern (i).name);
    end;

    if i > hbound (htab.pattern, 1)	/* check pattern count limit */
    then
      do;
        call comp_report_$ctlstr (2, comp_error_table_$limitation,
	   addr (ctl.info), ctl_line,
	   "Only ^d horizontal tab patterns permitted.",
	   hbound (htab.pattern, 1));
        goto return_;
      end;

    if varfld = ""			/* if no column stops given */
    then
      do;
        if i > htab.count		/* if not known */
        then goto return_;

        else			/* cancel the pattern */
	do i = i to htab.count - 1;	/* and close up table */
	  htab.pattern (i) = htab.pattern (i + 1);
	end;

        htab.count = htab.count - 1;	/* reduce pattern count */
        goto return_;
      end;

    call set_htabs (i, ercd);		/* set the stops */
    if ercd = 0			/* record the pattern name */
    then htab.pattern (i).name = tab_name;

set_htabs:
  proc (itab, pmercd);		/* record stops */

    dcl itab	   fixed bin;	/* pattern index */
    dcl pmercd	   fixed bin (35);	/* error code */

    dcl jstop	   fixed bin;

    pmercd = 0;
    jstop, htab.pattern (itab).count = 0;

    do while (varfld ^= "" & jstop < hbound (htab.pattern.stop, 1));
				/* extract stop column */
      htab.pattern (itab).stop (jstop + 1) =
	 comp_read_$number (varfld, hscales, 1, fnxt, addr (ctl.info),
	 pmercd);
      if pmercd ^= 0
      then return;

      varfld = substr (varfld, fnxt);	/**/
				/* extract fill string, if any */
      htab.pattern (itab).fill (jstop + 1) = "";
      if index (varfld, ",") ^= 1
      then
        do;
	if index (varfld, """") = 1
	then
	  do;
	    htab.pattern (itab).fill (jstop + 1) =
	         comp_extr_str_ ("1"b, varfld, 1, fnxt, 0, addr (ctl.info));
	    varfld = substr (varfld, fnxt);
	  end;

	else
	  do;
	    k = search (varfld, ", ");
	    if k > 0
	    then
	      do;
	        htab.pattern (itab).fill (jstop + 1) =
		   substr (varfld, 1, k - 1);
	        varfld = substr (varfld, k);
	      end;
	    else
	      do;
	        htab.pattern (itab).fill (jstop + 1) = varfld;
	        varfld = "";
	      end;
	  end;
        end;			/**/
				/* check for a comma */
      if varfld ^= "" & index (varfld, ",") ^= 1
      then
        do;
	pmercd = comp_error_table_$missing_delimiter;
	call comp_report_$ctlstr (2, pmercd, addr (ctl.info), ctl_line,
	     "Missing comma after stop column ^d", jstop + 1);
	return;
        end;

      varfld = after (varfld, ",");	/* step over comma */

      if htab.pattern (itab).stop (jstop + 1) = 0
      then call comp_report_ (2, 0, "Invalid tab stop in column 0 ignored.",
	      addr (ctl.info), ctl_line);
      else jstop = jstop + 1;
    end;

    if varfld ^= ""			/* if more were given */
    then call comp_report_$ctlstr (2, comp_error_table_$limitation,
	    addr (ctl.info), ctl_line,
	    "More than ^d tab stops given. The excess will be ignored.",
	    hbound (htab.pattern.stop, 1));

    htab.pattern (itab).count = jstop;
    htab.count = max (itab, htab.count);

  end set_htabs;

    goto return_;

ctl_ (102):			/* ".htf" = horizontal-tabs-off */
    if shared.htab_ptr = null ()	/* if no tab patterns */
    then goto return_;

    if ctl.index > length (ctl_line)	/* if no tab chars given */
    then htab.chars = "";		/* clear the tab char string */

    else				/* for each given character */
      do ctl.index = ctl.index to length (ctl_line);
				/* find it in the tab char string */
        i = search (htab.chars, substr (ctl_line, ctl.index, 1));

        if i > 0			/* if its there */
        then			/* take it out of the string */
	do;
	  if i < maxlength (htab.chars)
	  then
	    do;
	      htab.chars =
		 substr (htab.chars, 1, i - 1)
		 || substr (htab.chars, i + 1);
				/* close up pattern index array */
	      do i = i by 1 while (htab.pats (i) ^= 0);
	        htab.pats (i) = htab.pats (i + 1);
	      end;
	    end;

	  else
	    do;
	      htab.chars = substr (htab.chars, 1, i - 1);
	      htab.pats (i) = 0;
	    end;
	end;

        else call comp_report_ (2, 0,
	        substr (ctl_line, ctl.index, 1)
	        || " is not an active tabbing character.", addr (ctl.info),
	        ctl_line);
      end;

    if htab.chars = ""		/* if all are off */
    then
      do;
        current_parms.htab_mode = "0"b; /* reset the flags */
        if shared.blkptr ^= null ()
        then text.parms.htab_mode = "0"b;
      end;

    goto return_;

ctl_ (103):			/* ".htn" = horizontal-tabs-on */
    if varfld = ""			/* if no char given */
         | index (varfld, " ") ^= 2
    then
      do;
        call comp_report_$ctlstr (2, comp_error_table_$syntax_error,
	   addr (ctl.info), ctl_line, "The tab character must be first.");
        goto return_;
      end;

    if shared.htab_ptr = null ()	/* allocate htab data */
    then
      do;
        shared.htab_ptr = allocate (const.local_area_ptr, size (htab));
        htab.count, htab.pats = 0;	/* initialize pattern count */
        htab.chars = "";		/* and clear chars */
      end;

    tab_char = substr (varfld, 1, 1);	/* record htab char */
				/* advance to pattern name */
    varfld = ltrim (substr (varfld, 2));

    if varfld = ""			/* if no name given */
    then
      do;
        call comp_report_$ctlstr (2, comp_error_table_$syntax_error,
	   addr (ctl.info), ctl_line,
	   "No tab pattern or pattern name given.");
        goto return_;
      end;			/**/
				/* if 1st char is numeric, its a pattern */
    if search (varfld, "0123456789") = 1
    then
      do;
        call set_htabs (0, ercd);
        if ercd ^= 0
        then goto return_;
        j = 0;
      end;

    else
      do;
        tab_name = comp_read_$name (varfld, 1, 1, addr (ctl.info));

        if htab.count > 0		/* find the pattern */
        then
	do j = 1 to htab.count while (tab_name ^= htab.name (j));
	end;
        else j = 0;

        if j = 0 | j > htab.count
        then
	do;
	  call comp_report_ (2, 0, "Tab pattern not defined.",
	       addr (ctl.info), ctl_line);
	  goto return_;
	end;
      end;			/**/
				/* find the tab char in the active list */
    i = index (htab.chars, tab_char);

    if i = 0			/* if not an active tab char */
    then
      do;
        htab.chars =		/* record new tab char */
	   htab.chars || tab_char;
        i = length (htab.chars);	/* and set the index */
      end;

    htab.pats (i) = j;		/* record pattern index */
    current_parms.htab_mode = "1"b;	/* set the mode flag */

    if shared.blkptr ^= null ()
    then
      do;
        if text.input_line ^= ""	/* first a format break */
        then call comp_break_ (format_break, 0);
        text.parms.htab_mode = "1"b;
      end;

    goto return_;

ctl_ (113):			/* ".in" = indent-left */
ctl_ (114):			/* ".inb" = indent-both */
ctl_ (115):			/* ".inl" = indent-left */
    save_index = ctl.index;		/* save index in case of .inb */

    if shared.blkptr ^= null		/* if there is an active block */
    then if text.input_line ^= ""	/* clean up any leftovers */
         then call comp_break_ (format_break, 0);

    if shared.ftn_mode
    then call comp_util_$set_bin (current_parms.left.indent,
	    "footnote left indent", footnote_parms.left.indent,
	    footnote_parms.left.indent, footnote_parms.measure, hscales,
	    comp_dvt.min_WS);
    else call comp_util_$set_bin (current_parms.left.indent, "left indent", 0,
	    0, current_parms.measure, hscales, comp_dvt.min_WS);

    if shared.blkptr ^= null		/* if there is an active block */
    then text.parms.left.indent = current_parms.left.indent;

    if shared.bug_mode
    then call ioa_$rsnnl ("col=^d mrg=^f/^f net=^f", exit_str, 0,
	    page.hdr.col_index, show (current_parms.left.indent, 12000),
	    show (current_parms.measure - current_parms.right.indent, 12000),
	    show (current_parms.measure - current_parms.left.indent
	    - current_parms.right.indent, 12000));

    if index (ctl_line, ".inb") ^= 1	/* if not indenting both margins */
    then goto return_;

    ctl.index = save_index;		/* restore index */
    goto inr_ctl;			/* set the other side */

ctl_ (116):			/* ".inr" = indent-right */
inr_ctl:
    call comp_util_$set_bin (current_parms.right.indent, "right indent", 0, 0,
         current_parms.measure, hscales, comp_dvt.min_WS);

    if shared.blkptr ^= null ()	/* if there is an active block */
    then
      do;
        if text.input_line ^= ""	/* any leftovers */
        then call comp_break_ (format_break, 0);
        text.parms.right.indent = current_parms.right.indent;
      end;

    if shared.bug_mode
    then call ioa_$rsnnl ("col=^d mrg=^f/^f net=^f", exit_str, 0,
	    page.hdr.col_index, show (current_parms.left.indent, 12000),
	    show (current_parms.measure + current_parms.left.indent, 12000),
	    show (current_parms.measure, 12000));

    goto return_;

ctl_ (120):			/* ".ls" = line-space */
    call comp_util_$set_bin (current_parms.linespace, "linespacing",
         option.linespace, 0, page.parms.length, vscales, comp_dvt.min_lead);

    ctl.linespace = current_parms.linespace;

    if shared.blkptr ^= null ()
    then text.parms.linespace, text.input.linespace = current_parms.linespace;

    goto return_;

ctl_ (121):			/* ".pd"  = page-define */
    goto pdl_ctl;

ctl_ (122):			/* ".pdc" = page-define-column */
pdc_ctl:
    if shared.blkptr ^= null		/* finish an active block */
    then call comp_break_ (block_break, 0);
				/* if we have anything */
    if page.hdr.used > 0		/* balance the page */
    then call comp_break_ (column_break, 0);

    if ctl.index >= length (ctl_line)	/* if no columns are given */
    then
      do;
        if page.parms.cols.count > 0
        then
	do;
/****	  col0.hdr.pspc = 0;	/* erase old head space */
	  do i = 1 to page.parms.cols.count;
	    maxcolusd = max (maxcolusd, page.column_ptr (i) -> col.hdr.used);
	  end;
	end;			/**/
				/* revert to col0 */
        page_parms.cols.count, page.parms.cols.count, page_header.col_index,
	   page.hdr.col_index = 0;
        shared.colptr = page.column_ptr (0);
        current_parms.measure = col0.parms.measure;

        if shared.bug_mode
        then call ioa_$rsnnl ("^a col=0 b^d u^f/^f(^f)^[ ftn=^d/^f^;^2s^]"
	        || " h^f pag=^a c^d u^f/^f h^f^[ pi=^d ^f^]", exit_str, 0,
	        exit_str, col.hdr.blkct, show (col.hdr.used, 12000),
	        show (col.hdr.net, 12000), show (col.depth_adj, 12000),
	        (col.hdr.ftn.ct > 0), col.hdr.ftn.ct,
	        show (col.hdr.ftn.usd, 12000), show (col.hdr.pspc, 12000),
	        page.hdr.pageno, page.hdr.col_count,
	        show (page.hdr.used, 12000), show (page.hdr.net, 12000),
	        show (page.hdr.hdspc, 12000), (shared.picture.count > 0),
	        shared.picture.count, show (shared.picture.space, 12000));
      end;

    else				/* do the given columns */
      do;
        col0.margin.right, col0.parms.measure = page.parms.measure;
/**** column 1 is given special treatment because, if its width
      is zero, its gutter as assigned to column left margin
      and the second column spec is taken for column 1. */
				/* if no column 1 yet */
        if page.column_ptr (1) = null ()
        then
	do;
	  page.column_ptr (1) = allocate (const.local_area_ptr, size (col));
	  col1.blkptr (*) = null ();
	  col1.hdr = colhdr;
	  col1.hdrptr, col1.ftrptr = null ();
	  col1.hdrusd, col1.ftrusd = 0;
	end;

        page_parms.cols.count = 1;	/* set col 1 parms */
        call comp_util_$set_bin (col1.parms.measure, " column 1 measure", 0, 0,
	   page_parms.measure, hscales, comp_dvt.min_WS);
        page_width = col1.parms.measure;/**/
				/* is there a depth value? */
        if substr (ctl_line, ctl.index, 1) = "("
        then
	do;
	  ctl.index = ctl.index + 1;	/* step over paren */
	  call comp_util_$set_bin (col_depth_adj, "initial column 1 depth",
	       page_parms.length, -page_parms.length, page_parms.length,
	       vscales, comp_dvt.min_lead);

	  ctl.index = ctl.index + 2;	/* step over paren and comma */
	end;
        else col_depth_adj = 0;
        col1.depth_adj = col_depth_adj;

        if page_width = 0		/* if its zero width */
        then
	do;			/* gutter becomes left margin */
	  call comp_util_$set_bin (col1.margin.left, "column 1 left margin",
	       0, 0, page_parms.measure, hscales, comp_dvt.min_WS);
	  call comp_util_$set_bin (col1.parms.measure, "column 1 measure", 0,
	       0, page_parms.measure, hscales, comp_dvt.min_WS);

	  if page_width + col1.parms.measure > page_parms.measure
	  then goto bad_col;

	  else page_width = col1.margin.left + col1.parms.measure;
	end;			/**/
				/* col 1 has width, so */
        else col1.margin.left = 0;	/* it has 0 margin */
        col1.margin.right = col1.margin.left + col1.parms.measure;

        col1.hdr.net = page.hdr.net - col0.hdr.used - col0.hdr.ftn.usd;
        col1.hdr.baldepth = page.hdr.baldepth;
        maxcolusd = col1.hdr.used;

/****        if col0.hdr.used = 0
/****        then col1.hdr.pspc = page.hdr.hdspc;
/****        else */
        col1.hdr.pspc = col0.hdr.pspc;

        if shared.bug_mode
        then call ioa_$rsnnl ("^a col=1 b^d u^f/^f(^f)^[ ftn=^d/^f^;^2s^]"
	        || " h^f pag=^a c^d u^f/^f h^f^[ pi=^d ^f^]", exit_str, 0,
	        exit_str, col1.hdr.blkct, show (col1.hdr.used, 12000),
	        show (col1.hdr.net, 12000), show (col1.depth_adj, 12000),
	        (col1.hdr.ftn.ct > 0), col1.hdr.ftn.ct,
	        show (col1.hdr.ftn.usd, 12000), show (col1.hdr.pspc, 12000),
	        page.hdr.pageno, page.hdr.col_count,
	        show (page.hdr.used, 12000), show (page.hdr.net, 12000),
	        show (page.hdr.hdspc, 12000), (shared.picture.count > 0),
	        shared.picture.count, show (shared.picture.space, 12000));
				/* set bal default */
        page_parms.cols.bal, page.parms.cols.bal = "1"b;
				/* now the rest of the columns */
        do while (ctl.index < length (ctl_line));
				/* if too many columns */
	if page_parms.cols.count = max_cols
	then
	  do;
	    call comp_report_ (2, 0,
	         "Only " || ltrim (char (max_cols))
	         || " text columns allowed.", addr (ctl.info), ctl_line);
	    goto return_;
	  end;			/**/
				/* go to next column */
	page_parms.cols.count = page_parms.cols.count + 1;

	locolptr = page.column_ptr (page_parms.cols.count);
	if locolptr = null ()	/* allocate the column */
	then
	  do;
	    page.column_ptr (page_parms.cols.count) =
	         allocate (const.local_area_ptr, size (col));

	    locolptr = page.column_ptr (page_parms.cols.count);
	    locol.hdr = colhdr;
	    locol.hdrptr, locol.ftrptr = null ();
	    locol.hdrusd, locol.ftrusd = 0;
	    locol.blkptr (*) = null ();
	  end;

	call comp_util_$set_bin (locol.gutter, "column gutter", 3 * 7200, 0,
	     page_parms.measure, hscales, comp_dvt.min_WS);

	if page_width + locol.gutter >= page_parms.measure
	then
	  do;
bad_col:
	    call comp_report_$ctlstr (2, comp_error_table_$inconsistent,
	         addr (ctl.info), ctl_line,
	         "Sum of column widths exceeds page width. All columns "
	         || "after column ^d will be ignored.",
	         page_parms.cols.count);
	    page.parms.cols = page_parms.cols;
	    page.hdr.col_count, page.parms.cols.count =
	         page_parms.cols.count;
	    goto return_;
	  end;

	else
	  do;
	    locol.margin.left, page_width = page_width + locol.gutter;
	  end;

	call comp_util_$set_bin (locol.parms.measure,
	     "column  " || ltrim (char (page_parms.cols.count))
	     || " measure", 0, 0, page_parms.measure, hscales,
	     comp_dvt.min_WS);

	if page_width + locol.parms.measure > page_parms.measure
	then goto bad_col;
	else page_width = page_width + locol.parms.measure;

	locol.margin.right = locol.margin.left + locol.parms.measure;
				/* if there a depth value? */
	if ctl.index <= length (ctl_line)
	then if substr (ctl_line, ctl.index, 1) = "("
	     then
	       do;		/* step over paren */
	         ctl.index = ctl.index + 1;
	         call comp_util_$set_bin (col_depth_adj,
		    "initial column depth", page_parms.length,
		    -page_parms.length, page_parms.length, vscales,
		    comp_dvt.min_lead);
				/* step over paren and comma */
	         ctl.index = ctl.index + 2;
	       end;
	     else col_depth_adj = 0;
	locol.depth_adj = col_depth_adj;

	locol.hdr.net =
	     page.hdr.net - col0.hdr.used - col0.hdr.ftn.usd - locol.ftrusd;

/****	if col0.hdr.used = 0
/****	then locol.hdr.pspc = page.hdr.hdspc;
/****	else */
	locol.hdr.pspc = col0.hdr.pspc;

	locol.hdr.baldepth = page.hdr.baldepth;
	maxcolusd = max (maxcolusd, locol.hdr.used);
        end;

        if ctl.index = length (ctl_line)
        then
	do;
	  if index (substr (ctl_line, ctl.index), "u") = 1
	  then page_parms.cols.bal, page.parms.cols.bal = "0"b;
	  else page_parms.cols.bal, page.parms.cols.bal = "1"b;
	end;

        page.parms.cols = page_parms.cols;
				/* go to column 1 */
        page_parms.cols.count, page.hdr.col_index = 1;
        shared.colptr = page.column_ptr (1);
        current_parms.measure = col1.parms.measure;
      end;

    if page.hdr.used > 0
    then page.hdr.col_count = max (page.hdr.col_count, page.parms.cols.count);
    else page.hdr.col_count = page.parms.cols.count;

    do i = 0 to page.hdr.col_count;
      locolptr = page.column_ptr (i);
      if i > 0
      then locol.hdr.balusd, locol.hdr.used = maxcolusd;

      if shared.bug_mode
      then
        do;
	col_depth_adj = locol.depth_adj;
	call ioa_ ("^5x(col=^d b^d d^f u^f/^f(^f) h^f "
	     || "mrg=^f/^f/^f^[ (f^d ^f)^]", i, locol.hdr.blkct,
	     show (locol.hdr.baldepth, 12000), show (locol.hdr.used, 12000),
	     show (locol.hdr.net, 12000), show (locol.depth_adj, 12000),
	     show (locol.hdr.pspc, 12000), show (locol.margin.left, 12000),
	     show (locol.margin.right, 12000),
	     show (locol.parms.measure, 12000), (locol.hdr.ftn.ct > 0),
	     locol.hdr.ftn.ct, show (locol.hdr.ftn.usd, 12000));
        end;
    end;

    goto return_;

ctl_ (123):			/* ".pdl" = page-define-length */
pdl_ctl:
    if shared.blkptr ^= null ()	/* clean up current block */
    then call comp_break_ (block_break, 0);

    hf_needed = 0;			/* length is first, find min val */
    if shared.ophdrptr ^= null ()
    then hf_needed = max (hf_needed, shared.ophdrptr -> hfcblk.hdr.used);
    if shared.ephdrptr ^= null ()
    then hf_needed = max (hf_needed, shared.ephdrptr -> hfcblk.hdr.used);
    if shared.opftrptr ^= null ()
    then hf_needed = max (hf_needed, shared.opftrptr -> hfcblk.hdr.used);
    if shared.epftrptr ^= null ()
    then hf_needed = max (hf_needed, shared.epftrptr -> hfcblk.hdr.used);
    min_val =
         page.parms.margin.top + page.parms.margin.header + hf_needed
         + page.parms.margin.footer + page.parms.margin.bottom + 12000;

    call comp_util_$set_bin (page.parms.length, "page length", 792000, min_val,
         comp_dvt.pdl_max, vscales, comp_dvt.min_lead);
    page_parms.length = page.parms.length;

    if ^option.galley_opt
    then call comp_util_$set_net_page ("0"b);

    if index (ctl_line, ".pdl") = 1	/* if length only */
    then
      do;
        if shared.bug_mode
        then call ioa_$rsnnl ("page length = ^f", exit_str, 0,
	        show (page.parms.length, 12000));
        goto return_;
      end;

    else goto pdw_ctl;		/* otherwise, go for width */

ctl_ (124):			/* ".pdw" = page-define-width */
pdw_ctl:
    call comp_util_$set_bin (page_parms.measure, "page width", 468000, 1,
         comp_dvt.pdw_max, hscales, comp_dvt.min_WS);
    page.parms.measure, col0.parms.measure, col0.margin.right,
         text_parms.measure, footnote_parms.measure = page_parms.measure;

    if shared.blkptr ^= null ()
    then text.parms.measure = text_parms.measure;

    if shared.bug_mode
    then call ioa_$rsnnl ("pag,col0 measure ^f", exit_str, 0,
	    show (page_parms.measure, 12000));

    if index (ctl_line, ".pdw") = 1
    then goto return_;		/* if width only */
    else goto pdc_ctl;		/* otherwise, go for columns */

ctl_ (162):			/* ".un"  = undent */
ctl_ (163):			/* ".unb" = undent-both */
    goto unl_ctl;

ctl_ (164):			/* ".unh" = undent-hanging */
unh_ctl:
    if shared.blkptr = null
    then
      do;
        if ^page.hdr.headed & ^option.galley_opt
        then call comp_head_page_ (head_used);
        call comp_util_$getblk (page.hdr.col_index, shared.blkptr, "tx",
	   addr (current_parms), "0"b);
      end;
    else if text.input_line ^= ""
    then call comp_break_ (format_break, 0);

    text.input.hanging = "1"b;
    goto join_unl;

ctl_ (165):			/* ".unl" = undent-left */
unl_ctl:
    if shared.blkptr = null
    then
      do;
        if ^page.hdr.headed & ^option.galley_opt
        then call comp_head_page_ (head_used);
        call comp_util_$getblk (page.hdr.col_index, shared.blkptr, "tx",
	   addr (current_parms), "0"b);
      end;
    else if text.input_line ^= ""
    then call comp_break_ (format_break, 0);

join_unl:
    net_line =
         current_parms.measure - current_parms.right.indent
         - current_parms.left.indent;
    call comp_util_$set_bin (text.parms.left.undent, "left margin undent",
         text.parms.left.indent, -net_line, text.parms.left.indent, hscales,
         comp_dvt.min_WS);

    if index (ctl_line, ".unb") = 1
    then text.parms.right.undent = text.parms.left.undent;

    if shared.bug_mode
    then call ioa_$rsnnl ("col=^d mrg=^f/^f/^f", exit_str, 0,
	    page.hdr.col_index,
	    show (text.parms.left.indent - text.parms.left.undent, 12000),
	    show (text.parms.measure - text.parms.right.indent
	    + text.parms.right.undent, 12000),
	    show (text.parms.measure - text.parms.left.indent
	    + text.parms.left.undent - text.parms.right.indent
	    + text.parms.right.undent, 12000));

    goto return_;

ctl_ (166):			/* ".unn" = undent-nobreak - OBSOLETED BY .unh */
    goto unh_ctl;

ctl_ (167):			/* ".unr" = undent-right */
    if shared.blkptr = null
    then call comp_util_$getblk (page.hdr.col_index, shared.blkptr, "tx",
	    addr (current_parms), "0"b);

    net_line =
         page_parms.measure - current_parms.right.indent
         - current_parms.left.indent;
    call comp_util_$set_bin (text.parms.right.undent, "right margin undent",
         text.parms.right.indent, -net_line, text.parms.right.indent, hscales,
         comp_dvt.min_WS);

    if shared.bug_mode
    then call ioa_$rsnnl ("col=^d mrg=^f/^f/^f", exit_str, 0,
	    page.hdr.col_index,
	    show (text.parms.left.indent - text.parms.left.undent, 12000),
	    show (text.parms.measure - text.parms.right.indent
	    + text.parms.right.undent, 12000),
	    show (text.parms.measure - text.parms.left.indent
	    + text.parms.left.undent - text.parms.right.indent
	    + text.parms.right.undent, 12000));

    goto return_;

return_:
    if shared.bug_mode
    then call ioa_ ("^5x(format_ctls:^[ ^a^])", (exit_str ^= ""), exit_str);

    return;
%page;
show:
  proc (datum, scale) returns (fixed dec (11, 3));
    dcl datum	   fixed bin (31);
    dcl scale	   fixed bin (31);

    return (round (dec (round (divide (datum, scale, 31, 11), 10), 11, 4), 3));
  end show;
%page;
%include comp_brktypes;
%include comp_column;
    dcl 1 col1	   aligned like col based (page.column_ptr (1));
%include comp_dvt;
%include comp_entries;
%include comp_fntstk;
%include comp_footnotes;
%include comp_htab;
%include comp_option;
%include comp_page;
%include comp_shared;
%include comp_table;
%include comp_text;
%include compstat;
%include translator_temp_alloc;

  end comp_format_ctls_;

&ext device= x9700&;
&ext machines= &.(Xerox 9700)&;
&ext devclass = bitmap&;&+
&ext notes =
/* This writer emits text in quarter-line increments. */
&;&+

&ext dcls=
    dcl comment_ptr	   ptr;		/* the DJDE string */
    dcl 1 comment	   aligned based (comment_ptr),
	2 len	   fixed bin (21),
	2 str	   char (0 refer (comment.len)) unaligned;
    dcl FNL	   char (2)	/* full newline */
		   static options (constant) initial ("1
");
    dcl hot_chars	   char (9) static options (constant)
		   init ("");
    dcl inf_media      bit (36);	/* inferior fonts */
    dcl MAX_STR	   fixed bin static options (constant) init (130);
    dcl PENDOWN	   char (1) static options (constant) init ("_");
    dcl PENUP	   char (1) static options (constant) init ("");
    dcl QNL	   char (2)	/* quarter newline */
		   static options (constant) initial ("2
");
    dcl sup_media      bit (36);	/* superior fonts */
&;&+

&ext file_init=
      max_revlead = -12000;		/* back up only 1 line */
&;&+

&ext display=&+
    goto rtn (index (hot_chars, ch));

rtn (1):				/* \020 = EM- */
    ct = verify (dtext, ch);	/* how many? */
    if ct = 0			/* all the rest */
    then ct = length (dtext);
    else ct = ct - 1;

    if ct > 1			/* if more than one */
    then call ioa_$rsnnl ("<EM-*^d>", dstr, 0, ct);
    else dstr = "<EM->";

    rtn_str = rtn_str || dstr;
    goto rtn_end;

rtn (3):				/* \022 = HUGE */
    ct = verify (dtext, ch);	/* how many? */
    if ct = 0			/* all the rest */
    then ct = length (dtext);
    else ct = ct - 1;

    if ct > 1			/* if more than one */
    then call ioa_$rsnnl ("<HUGE*^d>", dstr, 0, ct);
    else dstr = "<HUGE>";

    rtn_str = rtn_str || dstr;
    goto rtn_end;

rtn (4):				/* \023 = EM */
    ct = verify (dtext, ch);	/* how many? */
    if ct = 0			/* all the rest */
    then ct = length (dtext);
    else ct = ct - 1;

    if ct > 1			/* if more than one */
    then call ioa_$rsnnl ("<EM*^d>", dstr, 0, ct);
    else dstr = "<EM>";

    rtn_str = rtn_str || dstr;
    goto rtn_end;

rtn (5):				/* 21 (\024) THICK */
				/* how many? */
      ct = verify (dtext, ch);
      if ct = 0			/* all the rest */
      then ct = length (dtext);
      else ct = ct - 1;

      if ct > 1			/* if more than one */
      then call ioa_$rsnnl ("<THK*^d>", dstr, 0, ct);
      else dstr = "<THK>";

      rtn_str = rtn_str || dstr;
      goto rtn_end;

rtn (6):				/* 22 (\025) MEDIUM */
				/* how many? */
      ct = verify (dtext, ch);
      if ct = 0			/* all the rest */
      then ct = length (dtext);
      else ct = ct - 1;

      if ct > 1			/* if more than one */
      then call ioa_$rsnnl ("<MED*^d>", dstr, 0, ct);
      else dstr = "<MED>";

      rtn_str = rtn_str || dstr;
      goto rtn_end;

rtn (7):				/* 23 (\026) THIN */
      ct = verify (dtext, ch);
      if ct = 0
      then ct = length (dtext);
      else ct = ct - 1;

      if ct > 1
      then call ioa_$rsnnl ("<THN*^d>", dstr, 0, ct);
      else dstr = "<THN>";

      rtn_str = rtn_str || dstr;
      goto rtn_end;

rtn (8):				/* 24 (\027) HAIR */
      ct = verify (dtext, ch);
      if ct = 0			/* all the rest */
      then ct = length (dtext);
      else ct = ct - 1;

      if ct > 1			/* if more than one */
      then call ioa_$rsnnl ("<HAIR*^d>", dstr, 0, ct);
      else dstr = "<HAIR>";

      rtn_str = rtn_str || dstr;
      goto rtn_end;

rtn (9):				/* 25 (\030) STROKE */
      ct = verify (dtext, ch);
      if ct = 0			/* all the rest */
      then ct = length (dtext);
      else ct = ct - 1;

      if ct > 1			/* if more than one */
      then call ioa_$rsnnl ("<STRK*^d>", dstr, 0, ct);
      else dstr = "<STRK>";

      rtn_str = rtn_str || dstr;
      goto rtn_end;

rtn (0):				/* 0 not hot */
rtn (2):				/* \021 = interfers with DC1 ctls */
      ct = 0;
rtn_end:
&;&+

&ext image_init=
      if first_page
      then
        do;			/* emit DJDE */
	window_level, max_level = 0;
	tstr_ptr = addr (window (0));
	tstr.str_ptr = allocate (window_area_ptr, 1024); 
	medselstr = "";
	comment_ptr = pointer (const.devptr, comp_dvt.comment_r);
	call put_str ((comment.str), 0);
	call put_;
	tstr_line = "";		/* clean up */
	tstr.open = "0"b;
	first_page = "0"b;
        end;

    if comp_dvt.pdl_max > 792000	/* Center 11" frob on longer page */
    then if page.length <= comp_dvt.pdl_max
         then			/* This is an idiosyncracy in the */
	 do;			/* way that the 9700 handles */
				/* vertical space; it thinks that a */
				/* page is 825 pts long when its */
				/* actually only 792 pts. Thus, we */
				/* try to center when the real page */
	   if page.length > 792000	/* length is between 792 and 825 */
	   then Yinit =
		   -round (divide (comp_dvt.pdl_max - page.length, 2 * Ypixel, 31, 1), 0);
	   else Yinit = -round (divide (comp_dvt.pdl_max - 792000, 2 * Ypixel, 31, 1), 0);
	 end;
&;&+

&ext line_init=
            sup_media, inf_media = "0"b;/* clear font control bits */
&;&+

&ext process_text=
        if tstr_line = ""
        then call put_str ((medselstr), 0);
&;&+

&ext line_finish=
	if tstr.devfnt > 0		/* if device font has been set */
	then
	  do;			/* show this one complete */
	    substr (fonts_done, tstr.font, 1) = "1"b;

	    if tstr_line = medselstr	/* empty line? */
	    then
	      do;
	        tstr_line = "";
	        tstr.devfnt = 0;
	      end;		/**/
				/* is it a superior font? */
	    if substr (sup_media, tstr.font, 1)
	    then call move_tstr (1);	/**/
				/* is it a inferior font? */
	    else if substr (inf_media, tstr.font, 1)
	    then call move_tstr (-1);
	  end;			/**/
				/* look for another */
	need_font = index (fonts_needed && ^fonts_done, "1"b);

	if need_font > 0		/* if another is needed */
	then
	  do;
	    if tstr.font = 0
	    then
	      do;
	        call set_font (need_font, media_size);
	        call set_media (font_in, font_media (font_in));
	        goto rescan_line;
	      end;

	    if font_media (tstr.font) ^= font_media (need_font)
	    then
	      do;			/* trim trailing media select */
	        if length (tstr_line) > 1
	        then if index (substr (tstr_line, length (tstr_line) - 1),
		        CR || medselstr) = 1
	             then tstr_line = rtrim (tstr_line, CR || medselstr);
				/* set font right away */
	        call set_font (need_font, media_size);
	        call set_media (font_in, font_media (font_in));
	        goto rescan_line;
	      end;
	  end;			/**/
				/* trim trailing media select */
	if length (tstr_line) > 1
	then if index (substr (tstr_line, length (tstr_line) - 1),
	          CR || medselstr) = 1
	     then tstr_line = rtrim (tstr_line, CR || medselstr);
&;&+

&ext plot= &.
    dcl ch1	     char (1);	/* a spacing char */
    dcl cu	     fixed bin;	/* ch1 width in strokes */

    if ^PLOT_OP			/* if a SHIFT is wanted */
    then
      do;
        if xmove ^= 0                   /* any X movement? */
        then
          do;
	  if xmove < 0
	  then
	    do;
	      if new_ypos = old_ypos	/* on the same line? */
	      && Xpos ^= 0
	      then pltstr = pltstr || CR || medselstr;
	      pltwidth = pltwidth - Xpos;
	      xmove = new_xpos;
	    end;

	  if tstr.devfnt ^= need_devfnt
	  then
	    do;
	      call set_media (font_in, need_devfnt);
	      xmove = new_xpos;
	    end;

	  if tstr_line = medselstr
	  then tstr.white = "1"b;

	  do ch1 = HUGE, EM, EN, THICK, MEDIUM, THIN, DEVIT, STROKE
	    while (xmove > 0);

	    if fnttbl.replptr (rank (ch1)) ^= null
	    then
	      do;
	        cu = fnttbl.units (rank (ch1));
	        penctl = fnttbl.replptr (rank (ch1)) -> replstr;

	        xii = divide (xmove, cu, 17, 0);
	        if xii > 0
	        then
		do;
		  pltstr = pltstr || copy (penctl, xii);
		  pltwidth = pltwidth + xii * cu;
		  xmove = xmove - xii * cu;
		end;
	      end;
	  end;
	end;
      end;			/**/
				/* else a VECTOR is wanted	       */
    else
      do;
        if ymove ^= 0		/* no vertical vectors allowed       */
        then call comp_report_$exact ("Vertical vectors not allowed " ||
	     "for &device&. device.", lineinfoptr);

        if xmove < 0		/* no rev horiz vectors allowed      */
        then call comp_report_$exact ("Reverse horizontal vectors not " ||
	     "allowed for &device&. device.", lineinfoptr);

        else if xmove > 0	/* forward horzontal vector	       */
        then
	do;
	      ch1 = substr (penctl, 1, 1);
	      char_ndx = rank (ch1);
	      repl_str_ptr = fnttbl.replptr (char_ndx);
	      cu = fnttbl.units (char_ndx);

	      if cu > 0
	      then
	        do;
		penctl = substr (replstr, 1, repl_str.len);

		xii = divide (xmove, cu, 17, 0);
		pltstr = pltstr || copy (penctl, xii);
		pltwidth = pltwidth + xii * cu;
	        end;
	   end;
	end;
&;&+

&ext set_font=&+
    if index (medsel (need_devfnt), "U") = 2
    then substr (sup_media, font_in, 1) = "1"b;
    else if index (medsel (need_devfnt), "D") = 2
    then substr (inf_media, font_in, 1) = "1"b;
    font_media (font_in) = need_devfnt;
&;

&ext set_media=&+
	medselstr = substr (medsel (new_devfnt), 1, 1);
&;&+

&ext put=&+
				/* for full/partial lines */
      do level_skip = 0 to line_window_size - 1
        while (^window (level + level_skip + 1).open);
      end;			/**/
				/* need a FNL? */
      if level_skip >= line_window_size - 1 | level = max_level
      then
        do;
          if first_page		/* is this the DJDE line? */
          then tstr_line = tstr_line || NL;

          else			/* not DJDE; thus, main body */
	  do;
	    if tstr_line ^= "" && index (reverse (tstr_line), NL) ^= 1
	    then tstr_line = tstr_line || CR;
	    tstr_line = tstr_line || FNL;
	  end;

	level_skip = 3;
        end;			/**/
				/* need a QNL */
      else
        do;
          if tstr_line ^= "" && index (reverse (tstr_line), NL) ^= 1
          then tstr_line = tstr_line || CR;
          tstr_line = tstr_line || QNL;
	level_skip = 0;
        end;
&;&+

&ext foot_proc=
      footref (1) = "";
      footref (3) = "";
&;&+

&comp_dev_writer()

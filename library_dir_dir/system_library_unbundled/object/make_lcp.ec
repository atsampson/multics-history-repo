&  **************************************************************
&  *                                                            *
&  * Copyright, (C) Massachusetts Institute of Technology, 1982 *
&  *                                                            *
&  **************************************************************
&  This ec makes the saved environment for the lisp compiler.  It starts with the
&  lisp_nostartup environment (see lisp_standard_environment_.ec), and loads
&  lcp_init_, lcp_semant_, and lcp_cg_, in that order.  It takes up to five
&  arguments.  The first two are required, and will be prompted for if not given.
&  The first is the compiler version printed by the compiler when it is invoked.
&  The second is the directory where lcp will be installed; it is used for the
&  historian autoload property.  The others are directory pathnames for the three
&  files it loads.  If these arguments are nonblank, they will be used in the
&  (load ...) forms.  The saved environment is compiler.sv.lisp, created in the
&  working directory.
&
&command_line off
&if [and [exists argument &1] [exists argument &2]]
&then &goto OK
&if [exists argument &2]
&then ec &r0 [response "Compiler version?" -non_null] &rf2
&else ec &r0 [response "Compiler version?" -non_null] [response "Directory to be installed in?" -non_null] &rf3
&quit



&label OK
discard_output -osw error_output "tmr (lcp_cg_ lcp_semant_ lcp_init_)"
&
&input_line off
&attach
lisp lisp_nostartup

(ioc w)

(gctwa nil)	; don't collect truly worthless atoms.

&if [exists argument &3]
&then (load "&q3>lcp_init_")
&else (load "lcp_init_")

(cond ((getl 'uread '(fsubr fexpr)))
      ((get 'uread 'autoload) (load (get 'uread 'autoload))))

(use compiler-obarray)

(setq compiler-revision &r1)

(defprop historian "&q2>lcp_historian_" autoload)

&if [exists argument &4]
&then (load "&q4>lcp_semant_")
&else (load "lcp_semant_")

&if [exists argument &5]
&then (load "&q5>lcp_cg_")
&else (load "lcp_cg_")

(*rset nil)
(record-compiler-history)

(use working-obarray)

(*rset nil)
(gctwa)
(gc)
(sstatus uuolinks t)
(save compiler)
&print LCP Version &1 saved.
&quit

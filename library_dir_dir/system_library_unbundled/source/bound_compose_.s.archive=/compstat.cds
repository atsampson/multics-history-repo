/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/*   static data for compose */

/* format: style2,ind3,ll79,dclind4,idind15,comcol41,linecom */

compstat:
   proc;

      dcl 1 compstat     aligned static,
	  2 compconst  like const;	/* constant data structure */

%include compstat;

      dcl com_err_	     entry options (variable),
	create_data_segment_
		     entry (ptr, fixed (35)),
	ercode	     fixed (35);	/* system error code */

      dcl 1 cdsargs	     aligned like cds_args;

%include cds_args;

/* assign true constants */
      compstat.version = const_version;
      compstat.art_symbols = "[]{}()|=o/X*mct^v<>\-HhSs~""'";

/*      compstat.comp_version = "9.15c";/* FirstPass, PageCount BIFs */
/*      compstat.comp_version = "9.16d";/* .fnt -reset */
/*      compstat.comp_version = "9.17l";/* marg adjs */
/*      compstat.comp_version = "9.18g";/* full page window */
/*      compstat.comp_version = "9.19e";/* compstat V6 */
/*      compstat.comp_version = "9.20a";/* MR11 performance */
/*      compstat.comp_version = "9.21";	/* translator_temp */
      compstat.comp_version = "10.0e";	/* extensible blocks */
      compstat.max_seg_chars = 4 * sys_info$max_seg_size;

/* set up cds arg structure */
      cdsargs.p (*) = addr (compstat);
      cdsargs.len (1) = 0;
      cdsargs.len (2) = size (compstat);
      cdsargs.struct_name (1) = "";
      cdsargs.struct_name (2) = "compstat";
      cdsargs.seg_name = "compstat";
      cdsargs.num_exclude_names = 0;
      cdsargs.exclude_array_ptr = null ();
      cdsargs.defs_in_link = "0"b;
      cdsargs.separate_static = "0"b;
      cdsargs.have_text = "0"b;
      cdsargs.have_static = "1"b;

      call create_data_segment_ (addr (cdsargs), ercode);
      if ercode ^= 0
      then call com_err_ (ercode, "create_stat");

   end;


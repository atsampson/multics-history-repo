/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose subroutine to process input files */

/* This routine is recursive since the controls processor must call it
   to process inserted files. */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

comp_:
  proc;

/* LOCAL STORAGE */

    dcl ascii_width	   fixed bin;	/* width of ctl line in chars */
    dcl blank_count	   fixed bin init (0);
    dcl BREAK	   bit (1) static options (constant) init ("1"b);
    dcl break_type	   fixed bin;
    dcl CBARS	   bit (1) static options (constant) init ("1"b);
				/* count of blanks inserted */
    dcl char_index	   (1020) fixed bin (9)
				/* for width measurement */
		   unsigned unaligned based (char_index_ptr);
    dcl char_index_ptr ptr;
    dcl col_space	   fixed bin (31);	/* to advance table columns */
    dcl EMPTY	   bit (1) static options (constant) init ("1"b);
    dcl endinput	   bit (1);	/* local copy of shared flag */
    dcl EPILOGUE	   fixed bin static options (constant) init (4);
    dcl ercd	   fixed bin (35);	/* error code */
    dcl fill_count	   fixed bin;	/* tab fill count */
    dcl head_used	   fixed bin (31);	/* space taken by page header */
    dcl htab_shift	   char (7) based (DCxx_p);
				/* ctl string for htabbing */
    dcl 1 htab_space   like dclong_val; /* for inserting htab WS */
    dcl (i, j)	   fixed bin;	/* working index and string index */
    dcl (ii, jj, k)	   fixed bin;	/* working index */
				/* for htab measuring */
    dcl 1 meas1	   aligned like text_entry.cur;
    dcl 1 meas2	   aligned like text_entry.cur;
    dcl strndx	   fixed bin;	/* working line scan index */
    dcl TEXT	   bit (1) static options (constant) init ("1"b);
    dcl text_added	   bit (1) aligned; /* text added to output buffer */
    dcl text_flag	   bit (1);	/* current block is in-line text */
    dcl TRIM	   bit (1) static options (constant) init ("1"b);
    dcl txtwidth	   fixed bin (31) init (0);
				/* measured text width */

/* EXTERNAL STORAGE */

    dcl (addrel, before, bin, copy, divide, index, length, max, min, mod, null,
        rtrim, search, substr)
		   builtin;
    dcl (comp_abort, end_output)
		   condition;

    dcl iox_$put_chars entry (ptr, ptr, fixed (24), fixed (35));

    if shared.bug_mode
    then call ioa_ ("comp_: (^d ^d ^a pass=^d)", call_stack.index,
	    insert_data.index, shared.input_filename, shared.pass_counter);

    htab_space.mark = DC1;
    htab_space.type = type_slx;	/* setup for htabbing */
    htab_space.leng = 4;
    DCxx_p = addr (htab_space);
    call_box_ptr = call_stack.ptr (call_stack.index);
				/* set ctl line overlay pointer */
    char_index_ptr = addrel (ctl.ptr, 1);

    call_box.lineno = 0;		/* clear line counter for this file */
    if call_stack.index = 0		/* set source file lineno */
    then call_box.lineno0 = 0;
    else call_box.lineno0 = call_box0.lineno;

    endinput = shared.end_input;	/* copy shared flag for recursion */
    shared.end_input = "0"b;		/* and reset it */
    on end_output goto end_output_;	/* end_output signal chain ends here */

read:
    if shared.end_input		/* did somebody signal? */
    then goto end_input_;
    if shared.end_output
    then goto end_output_;		/**/
				/* read an input line */
    call comp_read_$line (call_stack.ptr (call_stack.index), ctl_line, "0"b);
    ctl.info = call_box.info;

    if shared.end_input		/* input EOF? */
    then goto end_input_;

    if shared.literal_mode		/* a literal block? */
    then
      do;
        if shared.lit_count = 0	/* if thats all, reset the flag */
        then shared.literal_mode = "0"b;
        else			/* count lines */
	   shared.lit_count = shared.lit_count - 1;
      end;

    ctl.DVctl = "0"b;		/* reset device ctl flag */
    ctl.font = ctl.cur.font;		/* propagate any font changes */

    if index (ctl_line, "	") ^= 0	/* if any HTs in the line */
    then
      do;
        ascii_width = 0;		/* set up loop counters */
        i, j = 1;			/* and control indices */
        do while (j > 0);		/* as long as HTs are found */
	j = index (substr (ctl_line, i), "	");
				/* look for an HT */

	if j > 0			/* if one was found */
	then
	  do;

	    if j > 1		/* measure preceding text */
	    then
	      do;
	        do k = i to i + j - 2;
		if char_index (k) >= 32 & char_index (k) <= 126
		then ascii_width = ascii_width + 1;
		else if char_index (k) = 8
		then ascii_width = ascii_width - 1;
	        end;
	        ii = i + j - 1;	/* position of HT in line */
	      end;
	    else ii = i;		/* HT is the next character */

	    blank_count =		/* blanks to next Multics tab */
	         10 - mod (ascii_width, 10);
	    ctl_line = substr (ctl_line, 1, ii - 1) ||
				/* insert them */
	         copy (" ", blank_count) || substr (ctl_line, ii + 1);
	    i = ii + blank_count;	/* adjust counters */
	    ascii_width = ascii_width + blank_count;
	  end;
        end;
      end;

    if shared.table_mode
    then
      do;				/* record current table column */
        tblfmtptr = tbldata.fmt (tbldata.ndx).ptr;
        tblcolndx = tblfmt.ccol;
        tblcolptr = tblfmt.colptr (tblcolndx);
        if tblcolndx = 0
        then break_type = block_break;
        else break_type = format_break;
      end;

    text_added = "1"b;		/* preset text flag */

    if length (ctl_line) = 0		/* special handling for null lines */
    then
      do;
null_line:
        if shared.blkptr ^= null	/* if there is an active block */
        then
	do;
	  if text.parms.title_mode	/* a title block? */
	  then
	    do;			/* count lines */
	      text.hdr.eqn_line_count = text.hdr.eqn_line_count - 1;
				/* if thats all, reset flag */
	      if text.hdr.eqn_line_count = 0
	      then text.parms.title_mode = "0"b;
	    end;			/**/
				/* is there a header pending? */
	  if text.parms.hdrptr ^= null & ^shared.inserting_hfc
	  then call comp_title_block_ (text.parms.hdrptr);
	end;

        call comp_space_ (current_parms.linespace, shared.blkptr, TEXT, ^TRIM,
	   CBARS, "0"b);
        if shared.table_mode
        then call comp_break_ (break_type, -1);
        else if ^text.parms.art
        then call comp_break_ (block_break, 0);

        goto read;
      end;			/**/
				/* indented controls? then */
				/* find first nonblank */
    if shared.indctl.stk (shared.indctl.ndx)
    then ctl.index = verify (ctl_line, " ");
    else ctl.index = 1;		/* else start a 1 */

/* control line? */
    if index (substr (ctl_line, ctl.index), ".") = 1
         & index (substr (ctl_line, ctl.index), ". ") ^= 1
         & substr (ctl_line, ctl.index) ^= "."
         & index (substr (ctl_line, ctl.index), ".. ") ^= 1
         & substr (ctl_line, ctl.index) ^= ".."
         & index (substr (ctl_line, ctl.index), "...") ^= 1
    then
      do;
        if ^shared.literal_mode	/* if not in literal mode */
	   | (shared.literal_mode & shared.lit_count < 0
				/* or a non-cntng literal */
	   & (ctl_line = ".bel"	/* and end literal */
	   | ctl_line = ".be"))	/* or end all */
        then
	do;
tbl_:
	  if shared.table_mode	/* table mode? */
	  then if tblfmt.context	/* and format in context mode */
	       then
	         do;		/**/
				/* if there is a column index */
		 if index ("1234567890", substr (ctl_line, 2, 1)) ^= 0
		 then
		   do;
		     ctl.index = ctl.index + 1;
				/* bad column? */
		     if bin (substr (ctl_line, ctl.index, 1))
			> tblfmt.ncols
		     then
		       do;
		         call comp_report_ (2, 0,
			    "Column undefined for this format.",
			    addr (ctl.info), ctl_line);
		         goto read;
		       end;	/**/
				/* changing? */
		     if substr (ctl_line, ctl.index, 1) = "0"
			& tblfmt.ccol ^= 10
			| substr (ctl_line, ctl.index, 1) ^= "0"
			& bin (substr (ctl_line, ctl.index, 1))
			^= tblfmt.ccol
		     then call comp_tbl_ctls_ (tac_ctl_index);
				/* assure context mode */
		     tblfmt.context = "1"b;
				/* strip column off input line */
		     if length (ctl_line) > 2
		     then ctl_line = substr (ctl_line, 3);
		     else ctl_line = "";
				/* if changing columns */
		     if tblfmt.ccol ^= tblcolndx
		     then
		       do;	/* leaving column 0? */
		         if tblcolndx = 0
		         then
			 do i = 1 to tblfmt.ncols;
			   tblfmt.colptr (i) -> tblcol.depth =
			        tblcol0.depth;
			 end;	/**/
				/* set to new column */
		         tblcolndx = tblfmt.ccol;
		         tblcolptr = tblfmt.colptr (tblcolndx);
		         ctl.font, ctl.cur.font =
			    tblcol.parms.fntstk
			    .entry (tblcol.parms.fntstk.index);

		         if shared.blkptr ^= null ()
		         then
			 do;
			   text.input.font, text.input.cur.font,
			        ctl.font, ctl.cur.font =
			        tblcol.parms.fntstk
			        .entry (tblcol.parms.fntstk.index);
			   text.input.quad, ctl.quad = tblcol.parms.quad;
			 end;
		       end;	/**/
				/* for a null line */
		     if ctl_line = ""
		     then goto null_line;
		     else goto text_;
		   end;		/**/
				/* a real control line */
		 else if substr (ctl_line, 1, 3) ^= ".ur"
		 then
		   do;		/* clean up */
		     if shared.blkptr ^= null ()
		     then if text.input_line ^= ""
			then call comp_break_ (format_break, 0);
		   end;
	         end;		/**/
				/* call control processor */
	  call comp_ctls_ (text_added);

	  if text_added & shared.table_mode & substr (ctl_line, 1, 1) = "."
	       & index ("1234567890", substr (ctl_line, 2, 1)) ^= 0
	  then if tbldata.fmt (tbldata.ndx).ptr -> tblfmt.context
	       then goto tbl_;
	end;
      end;

    if shared.table_mode & text_added	/* text line in table mode? */
    then if tblfmt.context		/* and format in context mode? */
         then
	 do;
	   tblcolndx = tblfmt.ccol;
	   if tblcolndx ^= 0	/* going back to column 0? */
	   then
	     do;			/* clean up */
	       if shared.blkptr ^= null ()
	       then if text.input_line ^= ""
		  then call comp_break_ (format_break, 0);
				/* switch to column 0 */
	       tblcolndx, tblfmt.ccol = 0;
	       tblcolptr = tblfmt.colptr (0);
	       current_parms = tblcol.parms;

	       if shared.blkptr ^= null ()
	       then
	         do;
		 text.parms = current_parms;
		 text.input.quad, ctl.quad = current_parms.quad;
	         end;		/**/
				/* advance short columns */
	       do i = 0 to tblfmt.ncols;
	         tblfmt.colptr (i) -> tblcol.depth = tblfmt.maxdepth;
	       end;
	     end;
	 end;

/* text line */
text_:
    if text_added			/* if there's text to be added */
    then
      do;
        if shared.blkptr = null ()	/* get a text block if one is needed */
        then
	do;			/**/
				/* head page if needed */
	  if ^option.galley_opt & ^page.hdr.headed & page.hdr.col_index >= 0
	  then call comp_head_page_ (head_used);

	  call comp_util_$getblk (page.hdr.col_index, shared.blkptr, "tx",
	       addr (current_parms), ^EMPTY);
	end;

        if (text.blktype = "oh" | text.blktype = "eh" | text.blktype = "of"
	   | text.blktype = "ef" | text.blktype = "tf"
	   | text.blktype = "th" | "0"b)
				/* NAMED BLOCKS REPLACE "0"b */
        then text_flag = "0"b;
        else text_flag = "1"b;	/**/
				/* is there a header pending? */
        if text.parms.hdrptr ^= null & ^shared.inserting_hfc
        then if ^text.parms.title_mode
	   then call comp_title_block_ (text.parms.hdrptr);

        text.input.lmarg = text.parms.left.indent - text.parms.left.undent;
        text.input.rmarg =
	   text.parms.measure - text.parms.right.indent
	   + text.parms.right.undent;
        text.input.net = text.input.rmarg - text.input.lmarg;

        if shared.table_mode & ^text.parms.footnote
        then
	do;
	  text.input.lmarg = text.input.lmarg + tblcol.margin.left;
	  text.input.rmarg = text.input.rmarg + tblcol.margin.left;
	end;

        if ctl_line = ""		/* a null line */
        then goto null_line;		/**/
				/* if a filled block with leading */
        if text.parms.fill_mode	/* white space & there are leftovers */
        then if index (" ", substr (ctl_line, 1, 1)) ^= 0
	        & length (text.input_line) > 0
	   then
	     do;
	       call comp_break_ (format_break, 0);
	       if text.input.oflo & ^text.parms.keep & text.hdr.colno >= 0
		  & ^shared.table_mode
	       then call comp_break_ (need_break, -2);

	       if shared.end_output
	       then goto return_;
	     end;			/**/
				/* any active htabs? */
        if shared.htab_ptr ^= null ()
        then if htab.chars ^= ""
	   then call do_htabs;

        if ctl_line = ""		/* if its empty after all that */
        then goto null_line;

/* title block */
        if text.parms.title_mode
        then
	do;			/* count lines */
	  text.hdr.eqn_line_count = text.hdr.eqn_line_count - 1;
				/* if thats all, reset flag */
	  if text.hdr.eqn_line_count = 0
	  then text.parms.title_mode = "0"b;
				/* a <title> line? */
	  if index (ctl_line, shared.ttl_delim) = 1
	  then
	    do;			/* clean up leftovers */
	      if length (text.input_line) > 0
	      then call comp_break_ (format_break, 0);

	      text.input_line = ctl_line;
	      text.input.info = ctl.info;

/****	      if text.hdr.colno >= 0
/****	      then */
	      call comp_hft_ctls_$title (shared.blkptr, addr (text.input),
		 text.input_line, text.parms.linespace);
/****	      else call comp_util_$add_text (shared.blkptr, "0"b, "0"b, "0"b,
/****		 (text.input.quad ^= quadl), ^text.input.art, "0"b,*/
/****		      "0"b,	/* text.input.oflo, */
/****		      addr (text.input));*/

	      text.input.art = text.input.art | text.parms.art;
	      if text.input.art	/* if an artwork line */
	      then
	        do;
		text.hdr.art_count = text.hdr.art_count - 1;
		if text.hdr.art_count = 0
		then current_parms.art, text.parms.art = "0"b;
	        end;
	    end;

	  else goto plain;		/* free line in a formatted block */
	end;

/* column aligned table? */
        else if shared.table_mode & tblcol.align.posn > 0
        then
	do;			/* find the string */
	  strndx = index (ctl_line, tblcol.align.str);

	  if strndx > 0		/* if its there */
	  then
	    do;			/* measure preceding text */
	      unspec (meas1) = "0"b;
	      call comp_measure_ (substr (ctl_line, 1, strndx - 1),
		 addr (text.input.font), "0"b, text.input.art,
		 text.input.quad, 0, addr (meas1), addr (meas2),
		 addr (ctl.info));	/* add to left margin undent */
	      text.parms.left.undent =
		 text.parms.left.undent + meas1.width + meas1.avg;
	      text.input.lmarg =
		 text.input.lmarg + text.parms.left.indent
		 - text.parms.left.undent;
	      text.input.net = text.input.rmarg - text.input.lmarg;
	      text.input.quad = quadl;
	    end;
	  goto plain;
	end;

/* plain text */
        else
	do;
plain:
	  text.input.art = text.input.art | text.parms.art;
	  if text.input.art		/* if an artwork line */
	  then
	    do;
	      text.hdr.art_count = text.hdr.art_count - 1;
	      if text.hdr.art_count = 0
	      then current_parms.art, text.parms.art = "0"b;
	    end;			/**/
				/* if not building a formatted block */
	  if ^text.parms.title_mode	/* insert pending text heading */
	       & text.parms.hdrptr ^= null () & ^shared.inserting_hfc
	  then if text.parms.hdrptr -> hfcblk.hdr.count > 0
	       then call comp_title_block_ (text.parms.hdrptr);

	  if ^ctl.DVctl
	  then ctl.linespace = text.parms.linespace;
	  else
	    do;
	      text_flag = "0"b;
	      if ctl_line ^= wait_signal
	      then ctl.linespace = 0;
	    end;

/* if filling */
	  if text.parms.fill_mode & length (ctl_line) > 0
	       & ^text.parms.htab_mode
	  then
	    do;
	      call comp_fill_;
	      if shared.end_output
	      then goto return_;
	    end;

/* not filling */
	  else
	    do;
	      if (text.input.quad & just) | text.parms.htab_mode
	      then text.input.quad = quadl;

	      if ctl.DVctl
	      then text.input.linespace = 0;

	      if text.input.hanging
	      then
	        do;
		unspec (meas1) = "0"b;
		call comp_measure_ (ctl_line, addr (text.input.font), "0"b,
		     text.input.art, text.input.quad, 0, addr (meas1),
		     addr (meas2), addr (ctl.info));
		if meas1.width + meas1.avg <= text.parms.left.undent
		then text.input.linespace = 0;
		else text.input.linespace = text.parms.linespace;
	        end;

	      text.input_line = ctl_line;
	      text.input.info = ctl.info;
	      text.input.cbar = text.parms.cbar;
	      text.parms.cbar.del = "0"b;

	      call comp_util_$add_text (shared.blkptr,
		 (text.input.quad ^= quadl), ^text.input.art, "0"b,
		 text.input.oflo, addr (text.input));
	      text.input_line = "";

	      if text.input.oflo & text.hdr.colno >= 0
		 & ^(shared.table_mode | text.parms.keep | text.parms.art)
	      then call comp_break_ (need_break, -2);

	      if shared.end_output
	      then goto end_output_;

	      if shared.blkptr ^= null/* is there still an active block? */
	      then
	        do;
		text.input_line = "";
				/* erase */
				/* undents are used */
		text.parms.left.undent, text.parms.right.undent = 0;
		text.input.hanging, text.input.und_prot, ctl.hanging =
		     "0"b;
		text.input.linespace, ctl.linespace = text.parms.linespace;

		text.hdr.nofill_count = text.hdr.nofill_count - 1;
		if text.hdr.nofill_count = 0
		then call comp_format_ctls_ (fin_ctl_index);
	        end;
	    end;
	end;
      end;
    goto read;
%page;
end_input_:
    if shared.bug_mode
    then call ioa_ ("end_input: (^d ^d ^a)", call_stack.index,
	    insert_data.index, shared.input_filename);

    if call_stack.index > 0
    then
      do;
        shared.end_input = endinput;	/* restore the shared flag */
        goto return_;
      end;
%page;
end_output_:
    if option.db_line_end = -1	/* debugging end_output? */
    then shared.bug_mode = "1"b;

    if shared.bug_mode
    then call ioa_ ("end_output: (^a,^d)", shared.source_filename, ctl.lineno);

    if shared.if_nest.ndx > 0		/* open if nest? */
    then
      do;
        call comp_report_$ctlstr (2, 0,
	   addr (shared.if_nest (shared.if_nest.ndx).info),
	   shared.if_nest (shared.if_nest.ndx).line,
	   "Unterminated conditional execution (if) group.");
        shared.if_nest.ndx = 0;
      end;

    ctl_line = "";			/* erase a possible control line */
    if option.galley_opt		/* force the flag in galley */
    then shared.end_output = "1"b;

    if shared.blkptr ^= null ()	/* if there is a block */
    then
      do;
        if text.parms.title_mode	/* unterminated special block */
        then
	do;			/**/
				/* clean it up */
	  call comp_break_ (format_break, 0);

	  if text.blktype = "tx"
	  then
	    do;
	      call comp_report_$ctlstr (2, 0, addr (ctl.info), ctl_line,
		 "Unterminated equation block.");
	    end;
	  else
	    do;
	      const.current_parms_ptr = text.hdr.parms_ptr;
	      shared.blkptr = text.hdr.blkptr;
	    end;
	end;

        if shared.ftn_mode		/* unclosed footnote */
        then
	do;
	  ctl_line = ".bef";
	  call comp_block_ctls_ (bef_ctl_index);
	end;

        if shared.blkptr ^= null
        then
	do;
	  if shared.table_mode	/* exit table mode */
	  then
	    do;
	      ctl.index = 5;
	      ctl_line = ".taf";
	      call comp_tbl_ctls_ (taf_ctl_index);
	    end;

	  else if text.blktype = "pi" /* unterminated picture? */
	  then call comp_block_ctls_ (bep_ctl_index);

	  else			/* reset mode switches */
	    do;			/* and finish the block */
	      text.parms.keep, text.parms.art = "0"b;
	      text.input.lmarg =
		 text.parms.left.indent - text.parms.left.undent;
	      text.input.rmarg =
		 text.parms.measure - text.parms.right.indent
		 + text.parms.right.undent;
	      text.input.net = text.input.rmarg - text.input.lmarg;

	      if shared.table_mode & ^text.parms.footnote
	      then
	        do;
		text.input.lmarg = text.input.lmarg + tblcol.margin.left;
		text.input.rmarg = text.input.rmarg + tblcol.margin.left;
	        end;

	      call comp_break_ (block_break, 0);
	    end;
	end;
      end;

    if current_parms.cbar.del		/* orphan delete mark? */
    then
      do;
        call comp_space_ (current_parms.linespace, shared.blkptr, "1"b, "1"b,
	   "1"b, "0"b);
        call comp_break_ (block_break, 0);
      end;

    if shared.picture.count > 0	/* put any pictures */
    then call comp_util_$pictures (shared.blkptr);
				/* are footnotes held? */
    if shared.ftnblk_data_ptr ^= null () & shared.ftn_reset = "hold"
    then if ftnblk_data.highndx > 0
         then
	 do;
	   shared.purge_ftns = "1"b;
	   ctl_line = ".ift";
	   call comp_ctls_ ("0"b);
	 end;			/**/
				/* any leftovers? */
    if page.hdr.used + col0.hdr.ftn.ct ^= 0 | shared.blkptr ^= null ()
    then call comp_break_ (page_break, 0);

    if shared.pass_counter <= 1 & ^option.check_opt & page.image_ptr ^= null
    then
      do;
        page_record_ptr = addr (page_image.text_ptr -> record.page_record);
        page_record.leng = 0;
        call comp_dvt.outproc (EPILOGUE, 0);

        if page_record.leng > 0
        then
	do;
	  call iox_$put_chars ((shared.compout_ptr), addr (page_record.text),
	       page_record.leng, ercd);
	  if ercd ^= 0
	  then
	    do;
	      call comp_report_ (2, ercd, "Writing epilogue.",
		 addr (ctl.info), "");
	      signal comp_abort;
	      return;
	    end;
	end;
      end;

return_:
    if shared.bug_mode
         & (shared.input_filename = option.db_file
         | option.db_file = "ALLFILES")
    then call ioa_ ("^5x(comp_: ^a)", shared.input_filename);
%page;
do_htabs:
  proc;
    txtwidth =			/* set loop counters */
         text.parms.left.indent - text.parms.left.undent;
    i, j = 1;			/* set line scan controls */

    if length (ctl_line) > 0		/* adjust tabs */
    then
      do while (j > 0);
        j = search (substr (ctl_line, i), htab.chars);
        if j > 0			/* if a tab char was found */
        then
	do;
	  if j > 1		/* measure the preceding text */
	  then
	    do;
	      unspec (meas1) = "0"b;
	      call comp_measure_ (substr (ctl_line, i, j - 1),
		 addr (text.input.font), "0"b, text.input.art,
		 text.input.quad, 0, addr (meas1), addr (meas2),
		 addr (ctl.info));
	      txtwidth = txtwidth + meas1.width + meas1.avg;
	      ii = i + j - 1;
	    end;
	  else ii = i;		/* tab char is next, no new text */
				/* which tab character? */
	  jj = index (htab.chars, substr (ctl_line, ii, 1));
	  jj = htab.pats (jj);	/* pattern index for that character */
				/* find the stop column */
	  do k = 1 to htab.pattern (jj).count
	       while (txtwidth
	       >= htab.pattern (jj).stop (k) - shared.EN_width);
	  end;

	  if k <= htab.pattern (jj).count
				/* if within given stops */
	  then
	    do;
	      htab_space.v1 =	/* space needed */
		 htab.pattern (jj).stop (k) - txtwidth - shared.EN_width;

	      if htab_space.v1 > 0	/* if any to be inserted */
	      then
	        do;
		if htab.pattern (jj).fill (k) = ""
				/* if no given fill string */
		then
		  do;
		    ctl_line =
		         substr (ctl_line, 1, ii - 1) || htab_shift
		         || substr (ctl_line, ii + 1);
		    ii = ii + 7;
		  end;

		else
		  do;		/* construct the fill string */
		    unspec (meas1) = "0"b;
		    call comp_measure_ ((htab.pattern (jj).fill (k)),
		         addr (text.input.font), "0"b, "0"b, "0"b, 0,
		         addr (meas1), addr (meas2), addr (ctl.info));
		    fill_count =	/* number of fill strings needed */
		         divide (htab_space.v1, meas1.width + meas1.avg,
		         17, 0);
		    htab_space.v1 = htab_space.v1 -
				/* extra space */
		         fill_count * (meas1.width + meas1.avg);
		    if htab_space.v1 > 0
		    then
		      do;
		        ctl_line =
			   substr (ctl_line, 1, ii - 1) || htab_shift
			   || substr (ctl_line, ii);
		        ii = ii + 7;
		      end;
		    ctl_line = substr (ctl_line, 1, ii - 1) ||
				/* insert fill string */
		         copy (htab.pattern (jj).fill (k), fill_count)
		         || substr (ctl_line, ii + 1);
		    ii = ii
		         + fill_count
		         * length (htab.pattern (jj).fill (k));
		  end;

		i = ii;		/* adjust counters */
		txtwidth = htab.pattern (jj).stop (k) - shared.EN_width;
	        end;

	      else		/* htab char is next, just remove it */
		 ctl_line =
		      substr (ctl_line, 1, ii - 1)
		      || substr (ctl_line, ii + 1);
	    end;

	  else i = ii + 1;		/* not within given stops, step over it */
	end;
      end;
  end do_htabs;
%page;
%include comp_brktypes;
%include comp_ctl_index;
%include comp_text;
%include comp_column;
%include comp_DCdata;
%include comp_dvid;
%include comp_dvt;
%include comp_entries;
%include comp_fntstk;
%include comp_footnotes;
%include comp_htab;
%include comp_insert;
%include comp_metacodes;
%include comp_option;
%include comp_output;
%include comp_page;
%include comp_shared;
%include comp_table;
%include compstat;

  end comp_;

/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* format: style2,ind3,ll80,dclind4,idind16,comcol41,linecom */

expand_device_writer:
xdw:
   proc;
      me = "xdw";
      ME = "XDW";
      suffix = ".xdw";
      goto start;

macro:
   entry;
      me = "macro";
      ME = "MACRO";
      suffix = ".macro";

      dcl version	      char (6) int static init ("1.2b");
      dcl me	      char (32) var;
      dcl ME	      char (8) var;

start:
      code = 0;
      segname = "";
      in_p, of_p = null ();
      in_l, of_l = 0;
      callp = null ();
      in_sw, of_sw, pr_sw, long_sw = "0"b;

      argno = 0;
      argct = 0;
      do while (code = 0);
         argno = argno + 1;
         call cu_$arg_ptr (argno, arg_p, arg_l, code);
         if (code = 0)
         then
	  do;
	     if (substr (arg, 1, 1) = "-")
	     then
	        do;
		 if (arg = "-pr") | (arg = "-print")
		 then pr_sw = "1"b;
		 if (arg = "-npr") | (arg = "-no_print")
		 then pr_sw = "0"b;
		 else if (arg = "-call")
		 then
		    do;
		       argno = argno + 1;
		       call cu_$arg_ptr (argno, arg_p, arg_l, code);
		       if (code ^= 0)
		       then
			do;
			   call com_err_ (code, me, "-call value");
			   return;
			end;
		       callp = arg_p;
		       calll = arg_l;
		    end;
		 else if (arg = "-long") | (arg = "-lg")
		 then long_sw = "1"b;
		 else if (arg = "-brief") | (arg = "-bf")
		 then long_sw = "0"b;
		 else if (arg = "-instr") | (arg = "-input_string")
		 then
		    do;
		       if in_sw
		       then
			do;
			   call com_err_ (0, me,
			        "Multiple input_strings supplied.");
			   return;
			end;
		       if (in_p ^= null ())
		       then goto not_both;
		       argno = argno + 1;
		       call cu_$arg_ptr (argno, arg_p, arg_l, code);
		       if (code ^= 0)
		       then
			do;
			   call com_err_ (0, me, "Value for -in keyword.")
			        ;
			   return;
			end;
		       in_sw = "1"b;
		       in_p = arg_p;
		       in_l = arg_l;
		    end;
		 else if (arg = "-ag") | (arg = "-arguments")
		 then
		    do;
		       do i = 1 to 25 while (code = 0);
			argno = argno + 1;
			call cu_$arg_ptr (argno, arg_p, arg_l, code);
			if (code = 0)
			then
			   do;
			      argct = argct + 1;
			      argl.p (argct) = arg_p;
			      argl.l (argct) = arg_l;
			   end;
		       end;
		    end;
		 else if (arg = "-of") | (arg = "-output_file")
		 then
		    do;
		       of_sw = "1"b;
		       argno = argno + 1;
		       call cu_$arg_ptr (argno, arg_p, arg_l, code);
		       if (code ^= 0)
		       then
			do;
			   call com_err_ (0, me, "Value for -of keyword.")
			        ;
			   return;
			end;
		       of_p = arg_p;
		       of_l = arg_l;
		    end;
		 else
		    do;
		       call com_err_ (error_table_$badopt, me, "^a", arg);
		       return;
		    end;
	        end;
	     else
	        do;
		 if (segname ^= "")
		 then
		    do;
		       call com_err_ (0, me,
			  "Multiple source names supplied.");
		       return;
		    end;
		 if (substr (arg, 1, 1) = "&")
		 then
		    do;
		       call com_err_ (0, me,
			  "Must now use -input_string to specify string to be expanded."
			  );
		       return;
		    end;
		 if (in_p ^= null ())
		 then
		    do;
not_both:
		       call com_err_ (0, me,
			  "Cannot supply both string and segment as input."
			  );
		       return;
		    end;
		 call expand_pathname_ (arg, dname, ename, code);
		 if (code ^= 0)
		 then
		    do;
		       call com_err_ (code, me, "^a", arg);
		       return;
		    end;
		 ename = before (ename, suffix);
		 segname = rtrim (ename);
		 call hcs_$initiate_count (dname, segname || suffix, "",
		      in_l, 0, in_p, code);
		 if (in_p = null ())
		 then
		    do;
		       call com_err_ (code, me, "^a>^a^a", dname, ename,
			  suffix);
		       return;
		    end;
		 in_l = divide (in_l, 9, 24, 0);
		 code = 0;
	        end;
	  end;

         else if argno = 1
         then
	  do;
	     call com_err_ (code, ME,
		"^/Usage is: expand_device_writer {path} {-control_args}");
	     return;
	  end;
      end;

      if (segname = "") & (in_p = null ()) | (argno = 1)
      then
         do;
	  if (suffix = ".macro")
	  then call com_err_ (0, me,
		  "(^a) Proper usage is: ^a {path} {-control_args}",
		  version, me);
	  return;
         end;

      if ^of_sw
      then if in_sw
	 then pr_sw = "1"b;
	 else dname = get_wdir_ ();
      call get_temp_segments_ ((me), ptra, code);
      out_ptr = ptra (1);
      if (code ^= 0)
      then
         do;
	  call com_err_ (code, me);
	  return;
         end;

      out_len = 0;
      call ioa_ ("^a ^a", ME, version);
      on condition (cleanup)
         begin;
	  call xdw_$free (long_sw);
	  call release_temp_segments_ ((me), ptra, code);
         end;
      call xdw_$expand (me, segname, "", out_ptr, out_len, addr (argl), (argct),
	 msg, in_p, in_l, code);
      call xdw_$free (long_sw);
      if (code ^= 0)
      then
         do;
	  if (code = 1) | (code = -1)
	  then icode = 0;
	  else icode = code;
	  call com_err_ (icode, me, "^a", msg);
         end;
      if pr_sw
      then
         do;
	  call iox_$put_chars (iox_$user_output, out_ptr, out_len, code);
         end;
      else if (code = 0)
      then
         do;
	  if (of_p ^= null ())
	  then
	     do;
	        arg_p = of_p;
	        arg_l = of_l;
	        call expand_pathname_ (arg, dname, ename, code);
	        if (code ^= 0)
	        then
		 do;
		    call com_err_ (0, me, "^a", arg);
		    goto done;
		 end;
	     end;
	  call hcs_$make_seg (dname, ename, "", 01010b, out_ptr, code);
	  if (out_ptr = null ())
	  then call com_err_ (code, me, "^a>^a", dname, ename);
	  else
	     do;
	        substr (out_ptr -> str, 1, out_len) =
		   substr (ptra (1) -> str, 1, out_len);
	        call hcs_$set_bc_seg (out_ptr, out_len * 9, code);
	        call hcs_$terminate_noname (out_ptr, code);
	        if (code ^= 0)
	        then call com_err_ (code, me);
	        if (code = 0) & (callp ^= null ())
	        then call cu_$cp (callp, calll, 0);
	     end;
         end;
done:
      call release_temp_segments_ ((me), ptra, code);


      dcl 1 argl	      (25),
	  2 p	      ptr,
	  2 l	      fixed bin (24);
      dcl argct	      fixed bin (24);

      dcl in_p	      ptr;
      dcl in_l	      fixed bin (24);
      dcl of_p	      ptr;
      dcl of_sw	      bit (1);
      dcl in_sw	      bit (1);
      dcl of_l	      fixed bin (24);
      dcl get_wdir_	      entry returns (char (168));
      dcl icode	      fixed bin (35);
      dcl dname	      char (168);
      dcl suffix	      char (32) var;
      dcl ename	      char (32);
      dcl expand_pathname_
		      entry (char (*), char (*), char (*), fixed bin (35));
      dcl arg	      char (arg_l) based (arg_p);
      dcl arg_l	      fixed bin;
      dcl arg_p	      ptr;
      dcl argno	      fixed bin;
      dcl calll	      fixed bin (24);
      dcl callp	      ptr;
      dcl cleanup	      condition;
      dcl code	      fixed bin (35);
      dcl com_err_	      entry options (variable);
      dcl cu_$arg_ptr     entry (fixed bin, ptr, fixed bin, fixed bin (35));
      dcl cu_$cp	      entry (ptr, fixed bin (24), fixed bin (35));
      dcl error_table_$badopt
		      fixed bin (35) ext static;
      dcl get_temp_segments_
		      entry (char (*), (*) ptr, fixed bin (35));
      dcl i	      fixed bin;
      dcl ioa_	      entry options (variable);
      dcl iox_$put_chars  entry (ptr, ptr, fixed bin (24), fixed bin (35));
      dcl iox_$user_output
		      ptr ext static;
      dcl long_sw	      bit (1);
      dcl segname	      char (32) var;
      dcl xdw_$expand     entry (char (32) var, char (32) var, char (32) var,
		      ptr, fixed bin (24), ptr, fixed bin, char (1000) var,
		      ptr, fixed bin (24), fixed bin (35));
      dcl xdw_$free	      entry (bit (1));
      dcl msg	      char (1000) var;
      dcl out_len	      fixed bin (24);
      dcl out_ptr	      ptr;
      dcl pr_sw	      bit (1);
      dcl hcs_$initiate_count
		      entry (char (*), char (*), char (*), fixed bin (24),
		      fixed bin (2), ptr, fixed bin (35));
      dcl hcs_$make_seg   entry (char (*), char (*), char (*), fixed bin (5),
		      ptr, fixed bin (35));
      dcl hcs_$set_bc_seg entry (ptr, fixed bin (24), fixed bin (35));
      dcl hcs_$terminate_noname
		      entry (ptr, fixed bin (35));
      dcl ptra	      (1) ptr;
      dcl release_temp_segments_
		      entry (char (*), (*) ptr, fixed bin (35));
      dcl str	      char (262144) based;
   end expand_device_writer;

/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_alter_perform.pl1 Reformatted code to new Cobol standard.
                                                   END HISTORY COMMENTS */


/* Modified on 01/14/77 by ORN to signal command_abort_ rather than cobol_compiler_error */
/* Modified since Version 2.0 */

/*{*/
/* format: style3 */
cobol_alter_perform:
     proc (space_ptr, space_max);			/*
The procedure cobol_alter_perform is called by cobol_gen_driver only if
there are alter/perform records in variable common.  It constructs
alter_list, perform_list, and seg_init_list, utilizing data taken
from these records, and sets seg_init_flag to one if there are
any perform records or any alterable GO's in the fixed portion of
the program.  If neither of these conditions is met, seg_init_flag
is set to 0.  The first of the alter_perform records is located
by the variable perf_alter_info contained in fixed_common and/or
the variable size_perform_info, also in fixed_common.  The re-
cords located by the perf_alter_info "pointer" contain informa-
tion about perform and alter statements in the source program and
those located by the size_perform_info "pointer" contain informa-
tion on performable procedures created by ddalloc for computing
the "size" of data declared with "depending on" clauses.  Alter_
list, perform_list, and seg_init_list are described below in the
Data Section.


U__s_a_g_e:_

     declare cobol_alter_perform entry (ptr, fixed bin);
     call cobol_alter_perform(space_ptr, space_max);
						   */

dcl	space_ptr		ptr,
	space_max		fixed bin;

/*
space_ptr is a pointer to the next free location in the segment
	in which alter_list, perform_list, and seg_init_list
	are to be or have been located. (Input/Output)

space_max is the maximum number of words available in the
	segment pointed to by space_ptr. (Input)

D__a_t_a:_

     % include cobol_;
	Items in cobol_ include file used (u) and/or set (s) by
	cobol_alter_perform:

	     cobol_ptr (u)
	     com_ptr (u)
	     alter_list_ptr (s)
	     cobol_data_wd_off (u/s)
	     next_tag (u/s)
	     perform_list_ptr (s)
	     seg_init_list_ptr (s)
	     seg_init_flag (s)

     % include fixed_common;
	Items in fixed_common include file used (u) and/or(s) by
	cobol_alter_perform:

	     perf_alter_info (u)
	     size_perform_info (u)
						   */

%include cobol_alter_list;
%include cobol_perform_list;
%include cobol_seg_init_list;

dcl	alt_per_recd_ptr	ptr;

dcl	1 alt_per_recd	(100) aligned based (alt_per_recd_ptr),
	  2 filler	fixed bin,
	  2 record_len	fixed bin aligned,
	  2 proc_no	fixed bin aligned,
	  2 code		fixed bin aligned,
	  2 next_entry	fixed bin aligned,
	  2 extra		(2) fixed bin aligned,
	  2 priority	fixed bin aligned,
	  2 extra1	fixed bin aligned;

dcl	wds_left		fixed bin,
	index		fixed bin,
	jndex		fixed bin,
	make_even		fixed bin,
	no_fix_gos	fixed bin,
	num_alt_recds	fixed bin,
	num_recds		fixed bin,
	temp_min		fixed bin,
	temp_num		fixed bin,
	temp_pri		fixed bin;

dcl	1 seg_ovfl_error	aligned static,
	  2 my_name	char (32) init ("cobol_alter_perform"),
	  2 message_len	fixed bin init (36),
	  2 seg_name	char (12),
	  2 message	char (24) init ("Segment length exceeded!");

/*
where:

alt_per_recd_ptr is a pointer to the first alter/perform record
	       in variable common.

record_len       is the length, in characters, of the record.
	       The length of all alter/perform records is, in
	       fact, fixed and is 32 characters.

proc_no	       is the tag number of the procedure to be altered
	       or of the procedure at the end of a perform
	       range.

code	       is 1 if the information in the record pertains 
	       to an ALTER statement, 0 if it pertains to an
	       explicit PERFORM statement, and 2 if it pertains
	       to a PERFORM statement created by PD_Syntax to
	       implement a SORT statement.

next_entry       is a locator of the next alter_perform record in
	       variable common.  It contains the character
	       string "0000" if the record is the last record.

extra	       is unused by MCOBOL.

priority	       is the COBOL segment number assocoated with the
	       procedure identified by proc_no.

extra1	       is unused by MCOBOL.

wds_left	       is the number of free words remaining in the
	       segment pointed to by space_ptr.

index	       is a do loop index.

jndex	       is a do loop index.

make_even	       is a variable used to adjust space_ptr such that
	       seg_init_list begins on an even word boundary.
no_fix_gos       is the number of alterable GO's in fixed COBOL
	       segments.

num_alt_recds    is the number of ALTER records in the chain of
	       alter_perform records located in variable common.
num_recds	       is the total number of records in the chain of
	       alter/perform records located in variable common.

temp_min	       is used to hold the current minimun value of
	       procedure number in sorting alter_list and
	       perform_list on the basis of prodecure name.

temp_num	       is the index in alter_list or perform_list of
	       temp_min.

temp_pri	       is the COBOL segment number associated with
	       temp_min or is used to hold the priority number
	       of the member of alter_list being examined
	       during the construction of seg_init_list.

						   */
/*
P__r_o_c_e_d_u_r_e_s_C__a_l_l_e_d:_
						   */
dcl	cobol_read_rand	entry (fixed bin, char (5), ptr),
	signal_		entry (char (*), ptr, ptr);

/*
B__u_i_l_t-__i_n_F__u_n_c_t_i_o_n_s_U__s_e_d:_
						   */
dcl	addr		builtin,
	addrel		builtin,
	binary		builtin,
	null		builtin,
	rel		builtin,
	substr		builtin,
	unspec		builtin;

/*}*/


%include cobol_;
%include cobol_fixed_common;
%include cobol_ext_;


/*************************************/
start:
	cobol_$seg_init_flag = 0;
	wds_left = space_max - binary (rel (space_ptr), 17);

/*    		PROCESS PERFORM INFORMATION		   */

	cobol_$perform_list_ptr = space_ptr;
	perform_list.n = 0;
	num_alt_recds = 0;
	num_recds = 0;

	if fixed_common.size_perform_info ^= "00000"
	then do;
		call cobol_read_rand (1, fixed_common.size_perform_info, alt_per_recd_ptr);
		alt_per_recd_ptr = addrel (alt_per_recd_ptr, -2);

		do index = 1 by 1;
		     if wds_left >= 5
		     then do;
			     perform_list.n = perform_list.n + 1;
			     perform_list.perf.proc_num (perform_list.n) = alt_per_recd.proc_no (index);
			     perform_list.perf.priority (perform_list.n) = alt_per_recd.priority (index);
			     wds_left = wds_left - 5;
			end;

		     else goto signal_altr_prform_ovfl;

		     if unspec (alt_per_recd.next_entry (index)) = (4)"000110000"b
		     then goto eof_size_perform;

		end;

eof_size_perform:
	     end;
	if fixed_common.perf_alter_info ^= "00000"
	then do;
		call cobol_read_rand (1, fixed_common.perf_alter_info, alt_per_recd_ptr);
		alt_per_recd_ptr = addrel (alt_per_recd_ptr, -2);

/*  PUT PERFORMS, IF ANY, IN perform_list  */

		do index = 1 by 1;
		     if alt_per_recd.code (index) ^= 1
		     then if wds_left >= 5
			then do;

				perform_list.n = perform_list.n + 1;
				perform_list.perf.proc_num (perform_list.n) = alt_per_recd.proc_no (index);
				perform_list.perf.priority (perform_list.n) = alt_per_recd.priority (index);
				wds_left = wds_left - 5;

			     end;
			else goto signal_altr_prform_ovfl;
		     else num_alt_recds = num_alt_recds + 1;
						/* ORN */

		     if unspec (alt_per_recd.next_entry (index)) = (4)"000110000"b
		     then do;

			     num_recds = index;
			     goto end_of_list;

			end;
		end;
end_of_list:
	     end;

	if perform_list.n ^= 0
	then /*  SORT perform_list ON proc_num  */
	     do;

		cobol_$seg_init_flag = 1;
		if perform_list.n > 262144 - cobol_$cobol_data_wd_off
		then goto signal_data_ovfl;

		do index = 1 to perform_list.n;

		     temp_min = perform_list.perf.proc_num (index);
		     temp_num = index;

		     do jndex = index + 1 to perform_list.n;

			if perform_list.perf.proc_num (jndex) < temp_min
			then do;

				temp_min = perform_list.perf.proc_num (jndex);
				temp_num = jndex;

			     end;
		     end;

		     if temp_num ^= index
		     then do;

			     temp_pri = perform_list.perf.priority (temp_num);
			     perform_list.perf.proc_num (temp_num) = perform_list.perf.proc_num (index);
			     perform_list.perf.priority (temp_num) = perform_list.perf.priority (index);
			     perform_list.perf.proc_num (index) = temp_min;
			     perform_list.perf.priority (index) = temp_pri;

			end;
		     perform_list.perf.target_a_segno (index) = 2;
		     perform_list.perf.target_a_offset (index) =
			binary (unspec (cobol_$cobol_data_wd_off) || "00"b, 17);
		     cobol_$cobol_data_wd_off = cobol_$cobol_data_wd_off + 1;
		     perform_list.perf.int_tag_no (index) = cobol_$next_tag;
		     cobol_$next_tag = cobol_$next_tag + 1;

		end;
		cobol_$seg_init_flag = 1;
		if wds_left < 1
		then goto signal_altr_prform_ovfl;

		wds_left = wds_left - 1;
		space_ptr = addrel (space_ptr, binary (unspec (perform_list.n) || "00"b, 17) + perform_list.n + 1);

	     end;
	else perform_list_ptr = null;

/*       PROCESSING OF PERFORM INFORMATION COMPLETE          */


/*  	           PROCESS ALTER INFORNATION   	             */
	if num_alt_recds ^= 0
	then do;

		cobol_$alter_list_ptr = space_ptr;
		alter_list.n = 0;
		no_fix_gos = 0;
		do index = 1 to num_recds;

		     if alt_per_recd.code (index) = 1
		     then if wds_left >= 4
			then do;

				alter_list.n = alter_list.n + 1;
				alter_list.goto.proc_num (alter_list.n) = alt_per_recd.proc_no (index);
				alter_list.goto.priority (alter_list.n) = alt_per_recd.priority (index);
				if alter_list.goto.priority (alter_list.n) < 50
				then no_fix_gos = no_fix_gos + 1;

				wds_left = wds_left - 4;

			     end;
			else goto signal_altr_prform_ovfl;

		end;
		if alter_list.n > 262144 - cobol_$cobol_data_wd_off
		then goto signal_data_ovfl;

/*  SORT alter_list ON proc_num  */

		do index = 1 to alter_list.n;

		     temp_min = alter_list.goto.proc_num (index);
		     temp_num = index;

		     do jndex = index + 1 to alter_list.n;

			if alter_list.goto.proc_num (jndex) < temp_min
			then do;

				temp_min = alter_list.goto.proc_num (jndex);
				temp_num = jndex;

			     end;
		     end;

		     if temp_num ^= index
		     then do;

			     temp_pri = alter_list.goto.priority (temp_num);
			     alter_list.goto.proc_num (temp_num) = alter_list.goto.proc_num (index);
			     alter_list.goto.priority (temp_num) = alter_list.goto.priority (index);
			     alter_list.goto.proc_num (index) = temp_min;
			     alter_list.goto.priority (index) = temp_pri;

			end;
		     alter_list.goto.target_a_segno (index) = 2;
		     alter_list.goto.target_a_offset (index) =
			binary (unspec (cobol_$cobol_data_wd_off) || "00"b, 17);
		     cobol_$cobol_data_wd_off = cobol_$cobol_data_wd_off + 1;

		end;				/*  CONSTRUCT seg_init_list  */

		wds_left = wds_left - binary (unspec (alter_list.n) || "0"b, 17) - alter_list.n - 1;
		if wds_left < 8
		then goto signal_altr_prform_ovfl;

		if substr (rel (space_ptr), 18, 1) = "1"b
						/*  odd  */
		then make_even = 1;

		else make_even = 2;

		space_ptr = addrel (space_ptr, binary (unspec (alter_list.n) || "00"b, 17) + make_even);
		cobol_$seg_init_list_ptr = space_ptr;
		if no_fix_gos ^= 0
		then do;

			cobol_$seg_init_flag = 1;
			seg_init_list.n = 1;
			seg_init_list.seg.priority (1) = 0;
			seg_init_list.seg.int_tag_no (1) = 0;
			seg_init_list.seg.no_gos (1) = no_fix_gos;
			seg_init_list.seg.next_init_no (1) = 0;
			wds_left = wds_left - make_even - 6;

		     end;
		else seg_init_list.n = 0;

		if no_fix_gos ^= alter_list.n
		then do index = 1 to alter_list.n;

			temp_pri = alter_list.goto.priority (index);
			if temp_pri > 49
			then do;
				do jndex = 1 to seg_init_list.n;

				     if temp_pri = seg_init_list.seg.priority (jndex)
				     then do;

					     seg_init_list.seg.no_gos (jndex) =
						seg_init_list.seg.no_gos (jndex) + 1;
					     goto counted;

					end;
				end;
				if wds_left < 6
				then goto signal_altr_prform_ovfl;

				seg_init_list.n = seg_init_list.n + 1;
				seg_init_list.seg.priority (seg_init_list.n) = temp_pri;
				seg_init_list.seg.int_tag_no (seg_init_list.n) = cobol_$next_tag;
				cobol_$next_tag = cobol_$next_tag + 1;
				seg_init_list.seg.no_gos (seg_init_list.n) = 1;
				seg_init_list.seg.next_init_no (seg_init_list.n) = 0;
				wds_left = wds_left - 6;

			     end;
counted:
		     end;

		space_ptr = addrel (space_ptr, 6 * seg_init_list.n + 2);

		do index = 1 to seg_init_list.n;

		     seg_init_list.seg.init_ptr (index) = space_ptr;
		     space_ptr =
			addrel (space_ptr,
			binary (unspec (seg_init_list.seg.no_gos (index)) || "0"b, 17)
			+ seg_init_list.seg.no_gos (index));

		end;
	     end;
	else alter_list_ptr = null;

/*  	PROCESSING OF ALTER INFORMATION COMPLETE  	   */
exit:
	return;


/*************************************/
/*  		     ERROR LOOPS       		   */

signal_altr_prform_ovfl:
	seg_ovfl_error.seg_name = "ALTR/PRFORM ";
	call signal_ ("command_abort_", null, addr (seg_ovfl_error));
	return;

signal_data_ovfl:
	seg_ovfl_error.seg_name = "COBOL  data ";
	call signal_ ("command_abort_", null, addr (seg_ovfl_error));
	return;

     end cobol_alter_perform;

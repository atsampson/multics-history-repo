/* Begin include file setjmp.h */
/*	@(#)setjmp.h	1.3	*/


#define _JBLEN 3
typedef int *jmp_buf[_JBLEN];

extern int setjmp();
void longjmp();

/* End include file setjmp.h */

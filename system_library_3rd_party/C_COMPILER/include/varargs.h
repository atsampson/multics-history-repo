/* Begin include file varargs.h */


/* HISTORY COMMENTS:
  1) change(88-08-30,DGHowe), approve(88-08-30,MCR7971),
     audit(88-09-09,RWaters):
     Make compatible with System V version of varargs.h. Remove previously
     required indirection. Allow for compilation with -def
     Multics_Obsolete_Varargs. NOTE: This is an incompatible change.
                                                   END HISTORY COMMENTS */

/* Modification  */
/* Implemented March 9, 1987 DGHowe                                   */

/* The include file varargs.h has been altered to support the System
   V definitions of the macros usually included in this file.  This
   will imply that programs using the older version of the macros
   which required an extra level of indirection have to be
   recompiled using -def Multics_Obsolete_Varargs. or be altered to
   remove the usages of get_arg and the level of indirection.
*/

#ifndef Multics_Obsolete_Varargs

/* new MR12.2 version of the varargs macros. */

#ifndef NULL
#define NULL (void *) 0
#endif

typedef int **va_list;
va_list c_arg_ptr_addr();

#define va_dcl int va_alist;

int new_last_arg();

#define va_start(ap) (ap = c_arg_ptr_addr(&va_alist))
#define va_arg(ap, type) ((new_last_arg(ap) | ap == NULL) ? ((ap=NULL),((type) 0)) : **(type **) ((ap)++))
#define va_end(ap) (ap = NULL)

#else

/* MR12.1 version of varargs.h 
   two new functions are included here to obtain the last argument passed
   to a routine (usually the return value except for void functions) and
   a function get_arg(x) which will return a pointer to the specified 
   argument. Both functions return types va_list.

   Example.

   va_list get_arg(),last_arg();
   va_list arg, last;

   arg = get_arg (2);  will get second arg 
   last = last_arg();  will return the last arg 

*/

typedef char **va_list;
#define va_dcl int va_alist;

va_list get_arg(), last_arg();

#define va_end(list) (list)
#define va_start(list) (list=get_arg(1))
#define va_arg(list, mode) ((mode *)((list==last_arg()) ? (void*)0:*((list = list + 1), (list - 1))))

#endif

/* End include file varargs.h */

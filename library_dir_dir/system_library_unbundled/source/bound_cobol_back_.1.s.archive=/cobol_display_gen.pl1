/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_display_gen.pl1 Reformatted code to new Cobol standard.
                                                   END HISTORY COMMENTS */


/* Modified on 5/18/76 by George Mercuri for changes to error interface. */
/* Modified on 5/10/76 by George Mercuri for changes to error handling. */
/* Modified on 5/5/76 by George Mercuri for error handling techniques. */
/* Modified on 4/28/76 by G. Mercuri change code for new cobol_operators_. */
/* format: style3 */
cobol_display_gen:
     proc (mp_ptr);

dcl	stoff		fixed bin init (0);
dcl	mp_ptr		ptr;
dcl	1 mp		based (mp_ptr),
	  2 n		fixed bin,
	  2 pt		(0 refer (mp.n)) ptr;

dcl	1 args		static,
	  2 entryno	fixed bin init (6),		/* put_chars */
	  2 arglist_off	fixed bin,
	  2 stacktemp_off	fixed bin init (44),
	  2 n		fixed bin init (4),
	  2 arg1,
	    3 pt		ptr init (null ()),		/* not meaningful */
	    3 type	fixed bin init (4),		/* entry variable */
	    3 seg		fixed bin,		/* set to lp|off of link to iox_$user_output or $error_output*/
	    3 off		fixed bin init (0),
	    3 value	bit (18) unaligned,		/* not meaningful */
	    3 indirect	bit (1) unaligned init ("0"b),
	    3 overlay	bit (1) unaligned init ("0"b),
	    3 repeat_nogen	bit (1) unaligned init ("0"b),
	  2 arg2,
	    3 pt		ptr,			/* set to pt to type 9 token or varying literal string */
	    3 type	fixed bin init (5),		/* variable */
	    3 seg		fixed bin init (0),
	    3 off		fixed bin init (42),	/* allocate pointer to data at sp|42 */
	    3 value	bit (18) unaligned,		/* set to immed value when type = 2 */
	    3 indirect	bit (1) unaligned init ("1"b),/* pass a ptr to the data rather than the data */
	    3 overlay	bit (1) unaligned init ("0"b),
	    3 repeat_nogen	bit (1) unaligned init ("0"b),
	  2 arg3,
	    3 pt		ptr init (null ()),		/* not meaningful */
	    3 type	fixed bin init (3),
	    3 seg		fixed bin init (80),	/* allocate length at sp|80 */
	    3 off		fixed bin,		/* not meaningful */
	    3 value	bit (18) unaligned,		/* not meaningful */
	    3 indirect	bit (1) unaligned init ("0"b),
	    3 overlay	bit (1) unaligned init ("0"b),
	    3 repeat_nogen	bit (1) unaligned init ("0"b),
	  2 arg4,
	    3 pt		ptr init (null ()),		/* not meaningful */
	    3 type	fixed bin init (3),
	    3 seg		fixed bin init (40),	/* allocate multics code at sp|40 */
	    3 off		fixed bin,		/* not meaningful */
	    3 value	bit (18) unaligned,		/* not meaningful */
	    3 indirect	bit (1) unaligned init ("0"b),
	    3 overlay	bit (1) unaligned init ("0"b),
	    3 repeat_nogen	bit (1) unaligned init ("0"b);

dcl	1 s		auto,
	  2 n		fixed bin,
	  2 tag		fixed bin,
	  2 rtp		ptr,
	  2 ptp		ptr,
	  2 str		(2:257),
	    3 stp		ptr,
	    3 dtp		ptr;

dcl	1 nl_token	static,
	  2 size		fixed bin init (25),
	  2 line		fixed bin init (0),
	  2 column	fixed bin init (0),
	  2 type		fixed bin init (3),
	  2 lit_type	bit (1) init ("0"b),
	  2 all_lit	bit (1) init ("0"b),
	  2 filler1	bit (6) init (""b),
	  2 lit_size	fixed bin init (1),
	  2 string	char (1) init ("
");

dcl	1 rtype9		static,
	  2 header	(4) fixed bin init (112, 0, 0, 9),
	  2 repl_ptr	(2) ptr init ((2) null ()),
	  2 fill1		bit (108) init (""b),
	  2 file_key_info,
	    3 fb1		(3) fixed bin init (0, 0, 0),
	    3 size	fixed bin init (0),
	    3 fb2		(2) fixed bin init (0, 0),
	    3 flags1	bit (36) init ("010000100100000000010000000100000000"b),
	    3 flags2	bit (36) init (""b),
	    3 seg		fixed bin init (1000),
	    3 off		fixed bin,
	  2 fill2		(7) fixed bin init (0, 0, 0, 0, 0, 0, 0);


dcl	stream		char (20);

dcl	i		fixed bin;
dcl	tflen		fixed bin;
dcl	tvlen		fixed bin;
dcl	errorno		fixed bin;
dcl	lineno		fixed bin;
dcl	tagno		fixed bin;
dcl	tagno1		fixed bin;

dcl	argptr		ptr static;
dcl	dn_ptr		ptr;

dcl	cobol_call_op	entry (fixed bin, fixed bin);
dcl	cobol_reg_manager$after_op
			entry (fixed bin);
dcl	cobol_make_tagref	entry (fixed bin, fixed bin, ptr);
dcl	cobol_define_tag	entry (fixed bin);
dcl	cobol_ioop_util	entry (fixed bin);
dcl	cobol_string	entry (ptr);
dcl	cobol_io_util$move_direct
			entry (bit (3) aligned, fixed bin, fixed bin, fixed bin, bit (18) aligned);
dcl	cobol_alloc$stack	entry (fixed bin, fixed bin, fixed bin);
dcl	cobol_emit	entry (ptr, ptr, fixed bin);


/*************************************/
start:
	tagno = cobol_$next_tag;
	cobol_$next_tag = cobol_$next_tag + 1;
	call cobol_define_tag (tagno);
	rtype9.size = 0;				/* signal of no limit to cobol_string */
	if mp.pt (mp.n) -> end_stmt.a = "000"b
	then do;					/* iox_$error_output */
		errorno = 4;
	     end;
	else do;					/* iox_$user_output */
		errorno = 3;
	     end;
	lineno = mp.pt (1) -> reserved_word.line;

	s.n = mp.n - 1;
	s.tag = 0;
	s.rtp = addr (rtype9);
	s.ptp = null ();

	tflen, tvlen = 0;
	do i = 2 to mp.n - 1;
	     dn_ptr = mp.pt (i);
	     if data_name.type = 9
	     then do;
		     if data_name.variable_length
		     then tvlen = tvlen + data_name.item_length;
		     else tflen = tflen + data_name.item_length;
		end;
	     else if data_name.type = 2
	     then tflen = tflen + dn_ptr -> numeric_lit.places;
	     else if data_name.type = 3
	     then tflen = tflen + dn_ptr -> alphanum_lit.lit_size;
	     else tflen = tflen + 1;			/* figurative constant (type 1) */
	     s.stp (i) = dn_ptr;
	     s.dtp (i) = null ();
	end;
	s.stp (mp.n) = addr (nl_token);
	s.dtp (mp.n) = null ();
	tflen = tflen + 1;
	call cobol_io_util$move_direct ("110"b, 320, 4, 1, substr (unspec (tflen), 19, 18));

	call cobol_alloc$stack (tflen + tvlen, 2, stoff); /* for arg list + combined max item length */
	rtype9.off = stoff * 4;			/* next available location (will be reserved later) */
	call cobol_string (addr (s));

	arg2.pt = addr (rtype9);
	call cobol_ioop_util (stoff);
	if errorno = 3
	then call cobol_call_op (26, tagno);		/*5/18/76*/
	else call cobol_call_op (28, tagno);		/*5/18/76*/
	call cobol_reg_manager$after_op (4095 + errorno);

	return;


/*************************************/

/*****	Declaration for builtin function	*****/

dcl	(substr, mod, binary, fixed, addr, addrel, rel, length, string, unspec, null, index)
			builtin;

/*****	End of declaration for builtin function	*****/

%include cobol_type1;
%include cobol_type2;
%include cobol_type3;
%include cobol_type9;
%include cobol_type19;
%include cobol_;

     end cobol_display_gen;

/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

/*++
INCLUDE ERROR						        \
BEGIN
   /   / PUSH (BEGIN)
	  [call push ("BEGIN")]				       /\

\" if "dcl" or "MediaChars" appear first, take them.
   / dcl :
       /	  [if db_start = "dcl" then db_sw, dt_sw = "1"b;
	   if db_sw
	   then call ioa_ ("^[^/^]===Declare", dt_sw)]
         LEX(2)					            / dcl \
   / MediaChars :
       / LEX(2) pop
	  [mediachars_p = area_free_p]
         PUSH (done_MediaChars)
	  [call push ("done_MediaChars")]		     / MediaChars \
				\" any of these need a MediaChars table
				\" (which may be empty), so define
				\" a dummy table.
   / Media /					  / no_MediaChars \
   / View /					  / no_MediaChars \
   / Def /					  / no_MediaChars \
   / Font /					  / no_MediaChars \
   / Size /					  / no_MediaChars \
   / Device /					  / no_MediaChars \
   / <no-token> /					  / no_MediaChars \
				\" anything else here must be a 
				\" global device value. they dont need
				\" MediaChars.
   /   /						  / global_device \

no_MediaChars
   /   / pop
	  [call ERROR (missing_MediaChars)]
	  [mediachars_p = area_free_p;
	   mediachars.count = 1;	\" supply a dummy one
	   mediachars.name (1) = "<mediachar>";
	   mediachars.out_r (1) = "0"b]			       /\
				\" finish the MediaChars table
done_MediaChars
  / /	        [area_free_p = addr_inc (mediachars_p, size (mediachars))] /\

Media
     /	/	[if (db_start = "media") then db_sw, dt_sw = "1"b]       /\
     /	/	[media_p = area_free_p;
		 media.count = 0]				       /\
       / View	/	/ no_Media \
       / Def	/	/ no_Media \
       / Font	/	/ no_Media \
       / Size	/	/ no_Media \
       / Device	/	/ no_Media \
       / <no-token>	/	/ no_Media \
Media_
     /	/PUSH (Media_)[call push ("Media_")]			       /\
     / Media :
	/LEX(2)					        / Mwidths \
     / View	/pop				     / done_Media \
       / Def	/pop	/ done_Media \
       / Font	/pop	/ done_Media \
       / Size	/pop	/ done_Media \
       / Device	/pop	/ done_Media \
     /	/					  / global_device \
no_Media
     /	/[call ERROR (missing_Media)]
		[media.count = 1;	\" supply a dummy one
		 media.name (1) = "<media>";
		 media.rel_units (1) = 0;
		 media.width (1, 1) = 0]			       /\
done_Media
     /	/	[area_free_p = addr_inc (media_p, size (media))]	       /\

start_View
     /	/	[if (db_start = "view") then db_sw, dt_sw = "1"b]	       /\
     /	/	[view_p = area_free_p;
		 view.count = 0]				       /\
       / Def	/	/ no_View \
       / Font	/	/ no_View \
       / Size	/	/ no_View \
       / Device	/	/ no_View \
       / <no-token>	/	/ no_View \
View_
     /	/PUSH (View_)[call push ("View_")]			       /\
     / View :
	/LEX(2)	 				       / Viewrest \
       / Def	/pop	/ done_View \
       / Font	/pop	/ done_View \
       / Size	/pop	/ done_View \
       / Device	/pop	/ done_View \
     /	/					  / global_device \
no_View
     /	/[call ERROR (missing_View)]
		[view.count = 1;	\" supply a dummy one
		 view.name (1) = "<view>";
		 view.media (1) = 1]			       /\
done_View
     /	/	[area_free_p = addr_inc (view_p, size (view))]           /\

start_Def
     /	/	[if (db_start = "def") then db_sw, dt_sw = "1"b]	       /\
     /	/	[Def_p = area_free_p;
		 Def.count = 0]				       /\
       / Font	/	/ no_Def \
       / Size	/	/ no_Def \
       / Device	/	/ no_Def \
       / <no-token> /	/ no_Def \
Def_
     /	/PUSH (Def_)[call push ("Def_")]			       /\
     / Def :
	/LEX(2)					        / Defrest \
     / Font	/pop				       / done_Def \
       / Size	/pop       / done_Def \
       / Device	/pop       / done_Def \
     /	/					  / global_device \
no_Def
     /	/	[Def.count = 1;
		 Def.name (1) = "<Def>";
		 Def.pt (1) = null()]			       /\
done_Def
     /	/	[area_free_p = addr_inc (Def_p, size (Def))]	       /\

start_Font
     /	/	[if (db_start = "font") then db_sw, dt_sw = "1"b]	       /\
       / Size	/	/ no_Font \
       / Device	/	/ no_Font \
       / <no-token>	/	/ no_Font \
Font_
     /	/PUSH (Font_)[call push ("Font_")]			       /\
     / Font :
	/LEX(2)					       / Fontrest \
     / Size	/pop				      / done_Font \
       / Device	/pop	/ done_Font \
     /	/					  / global_device \
no_Font
     /	/[call ERROR (missing_Font)]			     / start_Size \
done_Font
     /	/	[area_free_p = addr_inc (oput_p, size (oput))]	       /\

start_Size
     /	/	[if (db_start = "size") then db_sw, dt_sw = "1"b]	       /\
     /	/	[size_list_p = area_free_p;
		 size_list.count = 0;
		 area_free_p, sizel_p = addr (size_list.start)]	       /\
       / Device	/	/ no_Size \
       / <no-token>	/	/ no_Size \
Size
     /	/PUSH (Size)[call push ("Size")]			       /\
     / Size :
	/LEX(2)					       / Sizerest \
     / Device
	/pop					      / done_Size \
     /	/					  / global_device \
no_Size
     /	/[call ERROR (missing_Size)]
		[size_list.count = 1;	\" supply a dummy one
		 size_list.name (1) = "<size>";
		 size_list.pt (1) = addr(size_list.start)]       / Device \
done_Size
     /	/	[if Sizes = 0
		 then if size_list.count > 0
		 then Sizes = 1;

		 tp = Ptoken;
		 do fnt_p = fntl_p (1) repeat (fnt.next)
		    while (fnt_p ^= null ());
		    Ptoken, Pthis_token = fnt.node;
		    if (font.min_wsp = -1)
		    then call ERROR (no_wordspace_val);
		 end;
		 Ptoken, Pthis_token = tp]		                 /\

Device
     /	/	[if (db_start = "dev") then db_sw, dt_sw = "1"b]	       /\
     /	/ PUSH (Device)[call push ("Device")]			       /\
     / Device :
	/	[Device_Pthis_token = Pthis_token]
	  LEX(2)
		[if db_sw then
		 call ioa_ ("===Device ^a", token_value)]    / Devicerest \
     / <no-token>
	/	[if const.devptr = null ()
		 then call ERROR (no_Device)]		         / RETURN \
     /	/  					  / global_device \

stack_pop
     /	/[if tr_sw
	  then call ioa_(" STACK_POP(^a,^i)",Stack(STACK_DEPTH),STACK_DEPTH)]
						     / STACK_POP \
     /	/[;
push: proc (name);
dcl name		char (*);

      Stack (STACK_DEPTH) = name;
      if tr_sw then call ioa_ (" PUSH(^a,^i)", name,STACK_DEPTH);
   end push;

pop: proc;
      if tr_sw then call ioa_ (" POP(^a,^i)", Stack (STACK_DEPTH),STACK_DEPTH);
      STACK_DEPTH = max (STACK_DEPTH - 1, 0);
   end pop;	]					       /\

\" define local named symbols for various strings
dcl
   / <ident> ,     
      /	  [dclname = token_value]
         LEX(2)
         PUSH (dcl_1)[call push ("dcl_1")]		       / output_0 \
dcl_1
     / ; /        [dcl_p = area_free_p;
	         dcl_.leng = length (part_str (1));
	         dcl_.dcl_v = part_str (1);
	         dcl_.dcl_name = dclname;
	         area_free_p = addr (dcl_.dummy);
	         if dt_sw
	         then call ioa_ ("^p^-dcl ^8a ""^a""", dcl_p, dcl_name,
	           dcl_v);
	         call link (dcl_l_p, dcl_p)]
	 LEX(1)					      / stack_pop \
     /	/[call ERROR (syntax_dcl)] NEXT_STMT		      / stack_pop \

MediaChars
     / <ident2>
	/	[media1, media2=token_value]		        / Media_3 \
     / <input>
	/	[media1, media2="[" || Input || "]"]
	 LEX(1)					        / Media_1 \
Media_err
     /	/[call ERROR (syntax_MediaChars)] 			       /\
Media_skip	\" scan forward looking for a "," or ";"
     / ,	/LEX(1)					     / MediaChars \
     / ;	/					        / Media_9 \
     / <any-token>
	/LEX(1)					     / Media_skip \

Media_1
     / :	/LEX(1)					        / Media_2 \
     /	/LEX(-1)					        / Media_3 \
Media_2
     / <input>
	/	[media2="[" || Input || "]"]		        / Media_3 \
     /	/					      / Media_err \
Media_3
     /	/	[held_Pthis_token = Pthis_token] \" for error msgs
	 LEX(1) PUSH (Media_4)[call push ("Media_4")]	       / output_0 \
Media_4
     /	/	[hold_Pthis_token = Pthis_token;
		 Ptoken, Pthis_token = held_Pthis_token;
					 \"in case any ERRORS
		 the_string = part_str (1);

		 if (media1 ^= media2)
		 then do;
		    if (substr (media1, 1, 1) ^= "[")
		    | (substr (media2, 1, 1) ^= "[")
		    then call ERROR (inv_MediaChar_range);
		    else if (media1 > media2)
		    then call ERROR (inv_Multics_char_range);
		 end;

		 do while (media1 <= media2);
		    do i = 1 to mediachars.count;
		       if (mediachars.name (i) = media1)
		       then do;
			call ERROR (dup_MediaChars);
			i = mediachars.count;
		       end;
		    end;

		    i = index (the_string, o777);
		    if (i > 0)
		    then do;
		       if (substr (media1, 1, 1) ^= "[")
		       then call ERROR (inv_MediaChar_SELF_ref);
		       substr (the_string, i, 1) = substr (media1, 2, 1);
		    end;
		    mediachars.count = mediachars.count + 1;
		    mediachars.name (mediachars.count) = media1;
		    mediachars.out_r (mediachars.count)
		       = rel (find_str (1));
		    if (i > 0)
		    then substr (the_string, i, 1) = o777;
		    substr (media1, 2, 1)
		       = byte (rank (substr (media1, 2, 1)) + 1);
		   	\" media has form "[x]" when in a range
		 end;
		 Ptoken, Pthis_token = hold_Pthis_token]	       /\
     / ,	/LEX(1)					     / MediaChars \
Media_9
     / ;	/ LEX(1)					      / stack_pop \
     /	/					      / Media_err \

Mwidths
     /	/	[mediact = 0;
		 mediabase = media.count]			       /\
Mwidth_1
     / <valid_Media_name>
	/	[mediact = mediact + 1;
		 media.count = media.count + 1;
		 media.name (media.count) = token_value;
		 media.rel_units (media.count) = Strokes;
		 media.width (media.count, *) = nulwidth]
	 LEX(1)					       / Mwidth_2 \
Mwidth_err
     /	/[call ERROR (syntax_Media_sec)] NEXT_STMT	       / Mwidth_3 \

Mwidth_2
     / ,	/LEX(1)					       / Mwidth_1 \
     / ;	/LEX(1)					       / Mwidth_3 \
     /	/					     / Mwidth_err \
Mwidth_3
     / strokes :
	/LEX(2)	[media_i = 1]			      / Mwidth_s1 \
     /	/					       / Mwidth_4 \
Mwidth_s1
     / <num>
	/	[if (media_i > mediact)
		 then call ERROR (too_many_stroke_values);
		 media.rel_units (media_i+mediabase) = token.Nvalue]
	 LEX(1)						       /\
     / ,	/LEX(1)	[media_i = media_i + 1]		      / Mwidth_s1 \
     / ;	/LEX(1)					       / Mwidth_3 \
     /	/					     / Mwidth_err \
Mwidth_4
     / <charname>
	/	[charid=token.Nvalue;
		 media1 = media2;		\" charname sets media2
		 media_i = 1;
		 mediawidth = nulwidth]
						       / Mwidth_A \
     /	/					      / stack_pop \
Mwidth_A
     / <input_>
	/LEX(1)					       / Mwidth_B \
     /	/LEX(1)					       / Mwidth_6 \
Mwidth_B
     / :	/LEX(1)					       / Mwidth_C \
     /	/					       / Mwidth_6 \
Mwidth_C
     / <charname>
	/					       / Mwidth_D \
     /	/					     / Mwidth_err \
Mwidth_D
     / <input_>
	/LEX(1)					       / Mwidth_6 \
     /	/					     / Mwidth_err \
Mwidth_6
     / <num>
	/					       / Mwidth_7 \
     / <negnum>
	/					       / Mwidth_7 \
     / =	/					       / Mwidth_8 \
     /	/					       / Mwidth_9 \
Mwidth_7
     /	/	[mediawidth = token.Nvalue]			       /\
Mwidth_8
     /	/LEX(1)	[media_ = media1;
		 charid_ = charid;
		 if (mediawidth = nulwidth)
		 then call ERROR (no_prior_width);
		 else if (media_ > media2)
		 then call ERROR (inv_Media_range);
		 else do while (media_ <= media2);
		    if (media_i > mediact)
		    then do;
		       call ERROR (too_many_widths);
		       media_ = "~" || rtrim (media2);
		    end;
		    else do;
		       media.width (media_i + mediabase, charid_)
			= mediawidth;
		       if (media_ < media2)
		       then do;
			substr (media_, 2, 1) =
			   byte (rank (substr (media_, 2, 1))+1);
			charid_ = 0;
			do i = 1 to mediachars.count
			   while (charid_ = 0);
			   if (mediachars.name (i) = media_)
			   then charid_ = i;
			end;
			if (charid_ = 0)
			then do;
			   call ERROR (inv_Media_range);
			   media_ = "~" || rtrim (media2);
			end;
		       end;
		       else media_ = "~" || rtrim (media2);   \" force it HI
		    end;
		 end]					       /\
Mwidth_9
     / ;	/LEX(1)					       / Mwidth_3 \
     / ,	/	[media_i = media_i + 1]
	 LEX(1)					       / Mwidth_6 \
     /	/					     / Mwidth_err \

Viewrest
     / <ident>
	/	[viewname=token_value]
	 LEX(1)					         / View_1 \
View_err
     /	/[call ERROR (syntax_View)] NEXT_STMT		      / stack_pop \

View_1
     / <medianame>
	/	[view.count = view.count + 1;
		 view.name (view.count) = viewname;
		 view.media (view.count) = token.Nvalue]
	 LEX(1)					         / View_2 \
     /	/					       / View_err \
View_2
     / ,	/LEX(1)					       / Viewrest \
     / ;	/LEX(1)					      / stack_pop \
     /	/					       / View_err \

Defrest
     / <ident> ;
	/	[Def.count = Def.count + 1;
		 Def.name (Def.count) = token_value]
	 LEX(2)	[Def.pt (Def.count) = Pthis_token;
		 vals_ct = 0]			          / Def_1 \
\" the token pointer is saved here so that at ref time parsing can be
\" temporarily diverted back here.

     /	/[call ERROR (no_name_Def)] NEXT_STMT			       /\


\" This keeps parsing until either a DEF, FONT, or INVALID STATEMENT occurs.
\" Nothing is done with the results of the parse other than invalid statements
\" are deleted so they will not cause further errors.
Def_1
     / Def /					      / stack_pop \
     / Font /					      / stack_pop \
       / Size	/	/ stack_pop \
       / Device	/	/ stack_pop \
       / <no-token>	/	/ stack_pop \
     /	/	[this_view = -1]
	 PUSH (Def_2)[call push ("Def_2")]		      / font_char \
Def_2
     / ;	/LEX(1)						/ Def_1 \
     /	/DELETE_STMT				          / Def_1 \
		\" font_char has already said why it is bad.
		\" Deleting statement is so error won't happen again
		\" during reparse at ref time.

font_char
     /	/	[vals_ct = 0]				       /\
fch_1
     / <all_input>
	/LEX(1)	[vals_ct = vals_ct + 1;
		 vals (vals_ct) = rank (Input)]		/ fch_2 \
     / art
	/LEX(1)						/ fch_0 \
     /	/[call ERROR (inv_Mul_char_spec)]		 	/ fch_e \
fch_0
     / <part>
	/LEX(1)	[vals_ct = vals_ct + 1;
		 vals (vals_ct) = rank (Input)]		/ fch_5 \
     /	/[call ERROR (inv_artwork_spec)]		 	/ fch_e \
fch_2
     / :	/LEX(1)						/ fch_3 \
     /	/						/ fch_5 \
fch_3
     / <all_input>
	/LEX(1)	[i = rank (Input);
		if (vals (vals_ct) > i)
		then do;
		   call ERROR (inv_Multics_char_range);
		   call LEX (-2);
	\" ******* back up to the ":" to force error exit at fch_4
		end;
		else do;
		   j = vals (vals_ct);
		   do while (j < i);
		      j = j + 1;
		      vals_ct = vals_ct + 1;
		      vals (vals_ct) = j;
		   end;
		end]	 				/ fch_4 \
     /	/[call ERROR (syntax_after_colon)]			/ fch_e \
fch_4
     / :	\" ******* this catches error forced above
        	/						/ fch_e \
fch_5
     / ,	/LEX(1)						/ fch_1 \
     / <is_viewname>
	/	[if (this_view ^= -1)
		then this_view = token.Nvalue]
	 LEX(1)						       /\

	\" MC_STRING is an alternate entry point to this routine.
mc_string
     /	/	[mediawidth, self_ct = 0;
		 the_string = ""]				       /\
     / <quoted-string> /					/ fch_6 \
     / (	/LEX(1)						/ fch_A \
     / <num> (	/					/ fch_6 \
     / SELF	/					/ fch_6 \
     / <charname>	/					/ fch_6 \
     /	/[call ERROR (not_charname)]				       /\
fch_6
     /	/	[part_nest = 0]
	 PUSH (fch_7)[call push ("fch_7")]			/ fch_l \
fch_7
     /	/	[the_string = part_str (1);
		testwidth = nulwidth;
		mediawidth = part_width (1)]		       /\
     / =	/LEX(1)						/ fch_9 \
fch_8
     / ;	/					      / stack_pop \
		\" normal return is with ";" token current
     / ,	/					      / stack_pop \
		\" but don't complain about a "," either.
     /	/[call ERROR (not_charname)] LEX (1)		          / fch_6 \
fch_9
     / <negnum>
	/	   [testwidth = token.Nvalue]
	 LEX(1)						/ fch_8 \
     / <num>
	/	   [testwidth = token.Nvalue]
	 LEX(1)						/ fch_8 \
     /	/[call ERROR (no_test_width)]				/ fch_8 \

fch_A
     /	/	[part_nest = 0]
	 PUSH (fch_B)[call push ("fch_B")]			/ fch_l \
fch_B
     / ) =
	/LEX (2)	[the_string = part_str (1);
		testwidth = nulwidth]			/ fch_C \
     /	/[call ERROR (paren_equal_expected)]			/ fch_8 \
fch_C
     / <negnum>
	/	   [mediawidth = token.Nvalue]
	 LEX(1)						/ fch_8 \
     / <num>
	/	   [mediawidth = token.Nvalue]
	 LEX(1)						/ fch_8 \
     /	/[call ERROR (missing_width)]				/ fch_8 \

fch_e
     / ;	/LEX(-1)					      / stack_pop \
		\" error return can't be with token ";" current
     /	/					      / stack_pop \
fch_l
     /	/	[part_nest = part_nest + 1;
		 part_str (part_nest) = "";
		 part_width (part_nest) = 0]			       /\
fch_M
     / <num> (
	/	[part_repl (part_nest) = token.Nvalue]
	 LEX(2) PUSH (fch_el)[call push ("fch_el")]		/ fch_l \
     / SELF
	/LEX(1)	[part_str (part_nest) = part_str (part_nest) || o777]
							/ fch_M \
     / <charname>
	/PUSH(fch_M)[call push("fch_M")]			/ fch_K \
     / <quoted-string>
	/LEX(1)	[list_ndx = 1]				/ fch_L \
     /	/	[part_nest = part_nest - 1]		      / stack_pop \
fch_L
     /	/LEX(-1)						       /\
     / <charlist>	\" peel them off one char at a time
	/PUSH(fch_L)[call push("fch_L")]			/ fch_K \
     /	/LEX(1)						/ fch_M \
fch_K
     /	/	[str_p = ptr (next_str_p, mediachars.out_r (token.Nvalue));
		 part_str (part_nest) = part_str (part_nest) || bstr.str;
		 if this_view > 0
		 then do;
		    if (media.rel_units (view.media (this_view))
		       ^= font.rel_units)
		    then call ERROR (bad_stroke_value);
		    mw = media.width (view.media (this_view),
		       token.Nvalue);
		    if mw = nulwidth
		    then call ERROR_ (no_width_specified,
		       view.name (this_view),
		       show_name (mediachars.name (token.Nvalue)));
		    part_width (part_nest) = part_width (part_nest) + mw;
		 end]
	 LEX(1)					      / stack_pop \

fch_el
     / )	/LEX(1)	[part_str (part_nest) = part_str (part_nest)
		    || copy (part_str(part_nest+1), part_repl (part_nest));
		 part_width (part_nest) = part_width (part_nest)
		    + part_width (part_nest+1) * part_repl (part_nest)]
							/ fch_M \
     /	/[call ERROR (unbal_parens)]		 	      / stack_pop \

Fontrest
     / <ident>
	/	[fnt_p = find_font ("1"b);
		 if fnt.pt ^= null ()
		 then call ERROR (dup_fontname);
		 font_ptr, fnt.pt = area_free_p;
		 area_free_p = addr_inc (area_free_p, size (font));
		 uni_p = area_free_p;
		 area_free_p = addr_inc (area_free_p, size (uni));
		 call link (unil_p, uni_p);
		 uni.seqno, uni.refno, uni_ct = uni_ct + 1;
		 units_ptr, uni.ref_p = area_free_p;
		 area_free_p = addr_inc (area_free_p, size (units));
		 opu_p = area_free_p;
		 area_free_p = addr_inc (area_free_p, size (opu));
		 call link (opul_p, opu_p);
		 opu.refno, opu.seqno = uni_ct;
		 oput_p, opu.ref_p = area_free_p;
		 font.units_r = rel (uni_p);
		 font.oput_r = rel (opu_p);
		 font.rel_units = -1;
		 font.footsep = Footsep;
		 font.min_wsp = MinWordsp;
		 font.avg_wsp = AvgWordsp;
		 font.max_wsp = MaxWordsp;
		 units (*) = 0;
		 oput.data_ct = 0;
		 default_view = 1]
	 LEX(1)					         / Font_1 \
     /	/[call ERROR (not_valid_Font_name)] NEXT_STMT	         / Font_3 \

Font_1
     / <is_viewname>
	/	[default_view = token.Nvalue;
		 font.rel_units
		    = media.rel_units (view.media (default_view))]
	 LEX(1)					         / Font_2 \
     /	/[call ERROR (not_viewname)] NEXT_STMT		         / Font_3 \

Font_2
     / ;	/					         / Font_3 \
     /	/[call ERROR (syntax_Font)]				       /\
Font_3
     /	/	[if Wordspace_p = null()
		 then goto RD_NEXT_REDUCTION;
		 hold_Pthis_token = Pthis_token;
		 Ptoken, Pthis_token = Wordspace_p]
	 PUSH(Font_4)[call push("Font_4")]		      / wordspace \
     /	/					         / Font_5 \
Font_4
     / ;	/	[Ptoken, Pthis_token = hold_Pthis_token]         / Font_8 \
     /    /[call ERROR (syntax_Wordspace)]
		[Ptoken, Pthis_token = hold_Pthis_token]         / Font_5 \

font_err
     /	/[call ERROR (syntax_Font_sec)] NEXT_STMT	      / stack_pop \

Font_5
     /	/PUSH (endFont)[call push("endFont")]			       /\
Font_6
     / <all_input>
	/	[this_view = default_view]
	 PUSH (Font_8)[call push ("Font_8")]		      / font_char \
     / art
	/	[this_view = default_view]
	 PUSH (Font_8)[call push ("Font_8")]		      / font_char \
     / Def	\" This is here for purposes of ref closure
	/					      / stack_pop \
     / footrefseparator :
	/LEX(2)					     / footrefsep \
     / wordspace :
	/LEX(2) PUSH(Font_9)[call push("Font_9")]	      / wordspace \
     / ref :
	/LEX(2)						  / ref \
     / Font	/				      / stack_pop \
     / Size	/				      / stack_pop \
       / Device	/	/ stack_pop \
       / <no-token>	/	/ stack_pop \
     / 	/PUSH (Font_6)[call push ("Font_6")]		  / global_device \

Font_8
     / ;	/	[self_ct = 0;
		 j = index (the_string, o777);
		 if (j = 0)
		 then the_string_r = rel (find_str (2));
		 else do;
		    do while (j <= length(the_string));
		       self_ct = self_ct + 1;
		       self_i (self_ct) = j;
		       j = j + 1;
		       if (j < length (the_string))
		       then do;
			i = index (substr (the_string, j), o777);
			if (i = 0)
			then j = length (the_string)+1;
			else j = j + i - 1;
		       end;
		    end;
		 end;
		 media1 = "[ ]";
		 jj= 0;
		 do i = 1 to vals_ct;
		    ii = vals (i);
		    if (self_ct > 0)
		    then do;
		       substr (media1, 2, 1) = byte (ii);
		       do j = 1 to self_ct;
			substr (the_string, self_i (j), 1) = byte (ii);
		       end;
		       the_string_r = rel (find_str (2));
		       charid = 0;
		       do jj = 1 to mediachars.count
			while (charid = 0);
			if (mediachars.name (jj) = media1)
			then charid = jj;
		       end;
		       if (charid = 0)	\" a MediaChar must be
		       then do;		\" defined with this name
			call ERROR (inv_Font_SELF_ref);
			jj = 0;
		       end;
		       else do;
			jj = media.width (view.media (this_view),
			   charid);
			if jj = nulwidth
			then call ERROR_ (no_width_specified,
			   view.name (this_view),
			   show_name (mediachars.name (charid)));
		       end;
		    end;
		    units (ii) = mediawidth + jj * self_ct;
		    oput.data_ct = max (oput.data_ct, ii);
		    oput.which (ii) = this_view;
		    oput.what_r (ii) = the_string_r;
		    if (testwidth ^= nulwidth)
		    then if (units (ii) ^= testwidth)
		    then call ERROR_ (bad_width_value,
		       ltrim (char (units (ii))),
		       ltrim (char (testwidth)));
		 end]
	 LEX(1)					         / Font_6 \
		\" the LEX(1) needs to be after so error msg will display
		\"  proper statement.
     /	/NEXT_STMT				         / Font_6 \
		\" font_char already told why in error


footrefsep
     / <all_input> ;
	/LEX(2)	[font.footsep = Input]		         / Font_6 \
Font_9
     / ;	/LEX(1)					         / Font_6 \
     /    /[call ERROR (syntax_wordspace)] NEXT_STMT LEX(-1)         / Font_6 \


wordspace
     / <num> ,
	/	[font.min_wsp = token.Nvalue]
	 LEX(2)					        / font_s2 \
     /	/					        / font_se \
font_s2
     / <num> ,
	/	[font.avg_wsp = token.Nvalue]
	 LEX(2)					        / font_s3 \
     /	/					        / font_se \
font_s3
     / <num> ,
	/	[font.max_wsp = token.Nvalue]
	 LEX(2)					        / font_s4 \
     /	/					        / font_se \
font_s4
     / <charname> ;
	/	[this_view = default_view;
		 vals_ct = 1;
		 vals (1) = 32]			      / mc_string \
	\" Consistency between AvgWordsp and mediawidth will be checked later.
font_se
    / ;	/ LEX(-1)					      / stack_pop \
	\" make sure NOT pointing to ";" token when return

    /	/					      / stack_pop \

endFont
    /	/	[tp = unil_p (2);	\" see if units is like a prior one
		 done = "0"b;
		 do uni_p = unil_p (1) repeat (uni.next)
		    while ((uni_p ^= unil_p (2)) & ^done);
		    if (uni.refno = uni.seqno)
		    then do;	\" check only "real" ones
		       if (unspec (uni.ref_p -> units)
		          = unspec (tp -> uni.ref_p -> units))
		       then do;
		          tp -> uni.refno = uni.seqno; \"its a duplicate
		          done = "1"b;
		       end;
		    end;
		 end;
		 tp = opul_p (2);	\" see if oput is like a prior one
		 done = "0"b;
		 do opu_p = opul_p (1) repeat (opu.next)
		    while ((opu_p ^= opul_p (2)) & ^done);
		    if (opu.refno = opu.seqno)
		    then do;	\" check only "real" ones
		       if (unspec (opu.ref_p -> oput)
			= unspec (tp -> opu.ref_p -> oput))
		       then do;
		          tp -> opu.refno = opu.seqno; \"its a duplicate
		          done = "1"b;
		       end;
		    end;
		 end]				      / stack_pop \


\" ----------------------------------------------------------------------------
\" This routine reparses the source following the named Def and then continues
\" following the ref statement.
ref
     / <is_Defname>
	/	[i = token.Nvalue]
	 LEX(1)						/ ref__ \
     /	/[call ERROR (not_Defname)] NEXT_STMT		         / Font_6 \
ref__
     / ;	/						/ ref_0 \
     /	/[call ERROR (missing_semicolon)]			       /\
ref_0
     /	/	[hold_Pthis_token = Pthis_token;
		 Ptoken, Pthis_token = Def.pt (i)]
	 PUSH (ref_1)[call push ("ref_1")]		         / Font_6 \
		\" divert parsing back to the Def source
ref_1
     /	/	[Ptoken, Pthis_token = hold_Pthis_token]
	 NEXT_STMT				         / Font_6 \
		\" have reached end of Def,
		\" continue parsing where we left off
\" ----------------------------------------------------------------------
Sizerest
     / <ident>
	/	[size_list.count = size_list.count + 1;
		 size_list.name (size_list.count) = token_value;
		 size_list.pt (size_list.count) = sizel_p;
		 sizel.val_ct = 0]
	 LEX(1)					        / point_1 \
     /	/[call ERROR (no_Size_name)] NEXT_STMT		      / stack_pop \

point_1
     / , <num>
	/LEX(1)	[sizel.val_ct = sizel.val_ct + 1;
		 sizel.val (sizel.val_ct) = scale_unit (1000)]
	 LEX(1)					        / point_1 \
     /	/	[area_free_p, sizel_p = addr_inc (sizel_p, size (sizel))]/\
     / ;	/LEX(1)					      / stack_pop \
     /	/[call ERROR (syntax_Size)] NEXT_STMT		      / stack_pop \
\"
global_device
     / Units : <unitkey> ;
	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Units ^a",
			dt_sw, token_value);
		 Hscale = hscales (token.Nvalue);
	           Vscale = vscales (token.Nvalue)]
	 LEX(2)					     / stack_pop \
     / Artproc : <ident>
	/LEX(2)	[ArtProc = token_value]
	 LEX(1)					        / Artproc \
     / Attach : <quoted-string> ;
	/LEX(2)	[the_string = token_value;
		 Atd_r = rel (find_str (2))]
	 LEX(2)					      / stack_pop \
     / Cleanup :
	/LEX(2)	[part_nest = 0]
	 PUSH (Cleanup)[call push ("Cleanup")]		      / mc_string \
     / Font :
	/					      / stack_pop \
     / Comment : <quoted-string> ;
	/LEX(2)	 [the_string = token_value;
		  Com_r = rel (find_str (2))]
	 LEX(2)					      / stack_pop \
     / DefaultMargs : <num> , <num> , <num> , <num> ;
	/LEX(2)	[DefVmt = scale_unit (Vscale)]
	 LEX(2)	[DefVmh = scale_unit (Vscale)]
	 LEX(2)	[DefVmf = scale_unit (Vscale)]
	 LEX(2)	[DefVmb = scale_unit (Vscale)]
     	 LEX(2)					      / stack_pop \
     / DevClass : <quoted-string> ;
     	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===DevClass", dt_sw);
		 DevClass = token_value]
	 LEX(2)					      / stack_pop \
     / DevName : <quoted-string> ;
     	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===DevName", dt_sw);
		 DevName = token_value]
	 LEX(2)					      / stack_pop \
     / Endpage : <all_input> ;
	/LEX(2)	[EndPage = unspec (Input)]
	 LEX(2)					      / stack_pop \
     / Footproc :
	/	[if db_sw
		 then call ioa_ ("^[^/^]===Footproc", dt_sw)]
	 LEX(2)				 	       / Footproc \
     / Footrefseparator :
	/	[if db_sw
		 then call ioa_ ("^[^/^]===Footrefseparator", dt_sw)]
	 LEX(2)					     / Footrefsep \
     / Justify : <switch> ;
     	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Justify", dt_sw);
		 Justify = (token.Nvalue > 0)]
	 LEX(2)					     / stack_pop \
     / Interleave : <switch> ;
     	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Interleave", dt_sw);
		 Interleave = (token.Nvalue > 0)]
	 LEX(2)					     / stack_pop \
     / Letterspace : <num> ;
	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Letterspace", dt_sw);
		 Letterspace = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / MaxFiles : <limit> ;
	/LEX(2)	[MaxFiles = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / MaxPages : <limit> ;
	/LEX(2)	[MaxPages = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / MaxPageLength : <limit> ;
          /LEX(2)	[MaxPageLength = scale_unit (Vscale)]
	 LEX(2)					      / stack_pop \
     / MaxPageWidth : <num> ;
	/LEX(2)	[if db_sw
		 then call ioa_ ("^[^/^]===MaxPageWidth", dt_sw);
		 MaxPageWidth = scale_unit (Hscale)]
	 LEX(2)				                / stack_pop \
     / MinBotMarg : <num> ;
     	/LEX(2)	[MinVmb = scale_unit (Vscale)]
	 LEX(2)					      / stack_pop \
     / MinLead : <num> ;
          /LEX(2)	[if db_sw then call ioa_ ("^[^/^]===MinLead", dt_sw);
		 MinLead = scale_unit (Vscale)]
	 LEX(2)					      / stack_pop \
     / MinSpace : <num> ;
          /LEX(2)	[if db_sw then call ioa_ ("^[^/^]===MinSpace", dt_sw);
		 MinSpace = scale_unit (Hscale)]
	 LEX(2)					      / stack_pop \
     / MinTopMarg : <num> ;
     	/LEX(2)	[MinVmt = scale_unit (Vscale)]
	 LEX(2)					      / stack_pop \
     / Outproc : <ident>
     	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Outproc", dt_sw);
		 OutProc, DisplayProc = token_value]
	 LEX(1)					        / Outproc \
     / Strokes : <num> ;
	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Strokes", dt_sw);
		 Strokes = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / Wordspace :
	/LEX(2)	[if db_sw then call ioa_ ("===Wordspace");
		 Wordspace_p = Pthis_token] NEXT_STMT         / stack_pop \
		\" just remember where this is for later use.
     / Sizes : <sizename> ;
     	/LEX(2)	[Sizes = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / Stream : <switch> ;
	/LEX(2)	[if db_sw then call ioa_ ("^[^/^]===Stream", dt_sw);
		 Openmode = 5 - 3 * token.Nvalue]
	 LEX(2)					      / stack_pop \
     / TapeRec : <limit> ;
     	/LEX(2)	[TapeRec = token.Nvalue]
	 LEX(2)					      / stack_pop \
     / <no-token> /[call ERROR (end_of_source)]		         / RETURN \
     / dcl
	/					   / out_of_place \
     / MediaChars
	/					   / out_of_place \
     / Media
	/					   / out_of_place \
     / View
	/					   / out_of_place \
     / Def
	/					   / out_of_place \
     / Font
	/					   / out_of_place \
     / Size
	/					   / out_of_place \
     / Device
	/					   / out_of_place \
     /  	/[call ERROR (inv_statement)] NEXT_STMT		      / stack_pop \
out_of_place
     /	/[call ERROR (stmt_out_of_place)] NEXT_STMT	      / stack_pop \

Artproc
     / $ <ident> ;
	/LEX(1)	[ArtEntry = token_value]
	 LEX(2)					      / stack_pop \
     /	/	[ArtEntry = ArtProc]
	 LEX(1)					      / stack_pop \

Footrefsep
     / <all_input> ;
	/	[Footsep = Input]
	 LEX(2)					      / stack_pop \
     /	/[call ERROR (syntax_Footrefsep)] NEXT_STMT	      / stack_pop \

Footproc
     / <ident>
	/	[FootProc = token_value]
	 LEX(1)					         / Foot_1 \
     / ,	/LEX(1)					         / Foot_2 \
     /	/[call ERROR (syntax_Footproc)] NEXT_STMT	      / stack_pop \

Foot_1
     / $ <ident>
	/LEX(1)	[FootEntry = token_value]
	 LEX(1)					        / Foot_2  \
     /	/	[FootEntry = FootProc]			       /\

Foot_2
     / , <fam_mem>
	/LEX(2)	[FootFamily = font_fam;
		 FootMember = font_mem]		                 /\
     / ;	/LEX(1)			                          / stack_pop \
     /	/[call ERROR (syntax_Footproc)] NEXT_STMT	      / stack_pop \

Outproc
     / $ <ident> ;
	/LEX(1)	[OutEntry = token_value]
	 LEX(2)					      / stack_pop \
     / ;	/	[OutEntry = OutProc]
	 LEX(1)			                          / stack_pop \
     /	/[call ERROR (syntax_Outproc)] NEXT_STMT	      / stack_pop \

Cleanup
     / ;	/LEX(1)	[ Clean_r = rel (find_str (2))]	      / stack_pop \
     /	/[call ERROR (syntax_Cleanup)] NEXT_STMT	      / stack_pop \

output_0
     /	/	[iii, parenct, part_nest = 0]			       /\
output_1
     /	/	[part_nest = part_nest + 1;
		 part_repl (part_nest) = iii;
		 part_str (part_nest) = ""]		                 /\

output_2
     / <octal>
	/	[part_str (part_nest) = part_str (part_nest) || Input]
	 LEX(1)					       / output_2 \
     / <quoted-string>
	/	[part_str (part_nest) = part_str (part_nest) || token_value]
	 LEX(1)					       / output_2 \
     / SELF
	/	[part_str (part_nest) = part_str (part_nest) || o777]
	 LEX(1)					       / output_2 \
     / <num> (
	/	[iii = token.Nvalue;
		 parenct = parenct + 1]
	 LEX(2) PUSH (output_3)[call push ("output_3")]	       / output_1 \
     / <dcl_ed>
	/	[part_str(part_nest) = part_str(part_nest)||bstr.str]
	 LEX(1)					       / output_2 \
     /	/					      / stack_pop \

output_3
     / )	/LEX(1)	[part_str (part_nest-1) = part_str (part_nest-1)
		    || copy (part_str (part_nest), part_repl (part_nest));
		 part_nest = part_nest - 1;
		 parenct = parenct - 1]		       / output_2 \
     / ;	/[call ERROR (unbal_parens)]			      / stack_pop \
     /	/					      / stack_pop \

Devicerest
     /	/	[comp_dvid_ct = comp_dvid_ct+1;
		 comp_dvid_new="1"b;
		 like_table = -1]				       /\
Device_0
     / <valid_Device_name>
	/	[if (dvid_ct = 0)
		 then dvid_ct = dvid_ct + 1;   \" add Device name
	           dvid_p = area_free_p;
		 area_free_p = addr (dvid.dummy);
		 call link (dvidl_p, dvid_p);
		 dvid.ndx = comp_dvid_ct;
		 dvid.real = comp_dvid_new;
		 dvid.refname = token_value;
		 dvid.devname = DevName;
		 dvid.dvt_ndx = dvt_ct + 1;
		 comp_dvid_new = "0"b]
	 LEX(1)					       / Device_1 \

table_e
     /	/[call ERROR (syntax_Device)] NEXT_STMT		      / stack_pop \

Device_1
     / ,	/LEX(1)					       / Device_0 \
     / like 
          /LEX(1)					     / like_table \
     / ;	/LEX(1) PUSH (startDevice)[call push ("startDevice")]    / Device_I \
     /	/					        / table_e \

like_table
     / <table_name> ;
          /	[like_table = token.Nvalue]
	 LEX(2)					    / like_table2 \
     /	/					        / table_e \

like_table3
     /	/	[do dvid_p = dvidl_p (1) repeat (dvid.next)
		    while (dvid_p ^= null ());
		    if dvid.dvt_ndx = dvt_ct + 1
		    then dvid.dvt_ndx = like_table;
		 end]				      / stack_pop \
like_table2
     / Device
	/					    / like_table3 \
     / <no-token>
          /					    / like_table3 \
     /    / PUSH (copy_table)[call push ("copy_table")]	       / Device_I \

Device_I
     /	/	[dvt_p = area_free_p;
		 area_free_p = addr (dvt.dummy);
		 call link (dvtl_p, dvt_p);
		 dvt.ndx, dvt_ct = dvt_ct + 1;
		 dvt.med_sel = area_free_p;
		 med_sel_tab.count = font_count;
		 area_free_p
		    = addr_inc (area_free_p, size (med_sel_tab));
		 med_sel_tab.ref_r (*) = "0"b;

		 dvt.prent, prent_p = area_free_p;
		 area_free_p = addr (prent.dummy);

		 dvt.ref, const.devptr = area_free_p;
		 dvt_ct = dvt_ct + 1]		      / stack_pop \

copy_table
     /	/	[tp = null ();
		 do dvt_p = dvtl_p (1) repeat (dvt.next)
		    while (dvt_p ^= null () & tp = null ());
		    if dvt.ndx = like_table
		    then tp = dvt_p;
		 end;

		 dvt_p = tp;
		 med_sel_tab = dvt.med_sel -> med_sel_tab;
		 prent = dvt.prent -> prent;
		 comp_dvt.family_ct = dvt.ref -> comp_dvt.family_ct;
		 comp_dvt = dvt.ref -> comp_dvt]	       / Device_2 \

startDevice
     /	/	[prent.outproc = OutProc || "$" || OutEntry;
		 prent.artproc = ArtProc || "$" || ArtEntry;
		 prent.footproc = FootProc || "$" || FootEntry;
		 initfamily, initmember = "";
		 footfamily = FootFamily;
		 footmember = FootMember;
		 hscale = Hscale;
		 vscale = Vscale;
		 comp_dvt.devclass = DevClass;
		 comp_dvt.min_WS = MinSpace;
		 comp_dvt.min_lead = MinLead;
		 comp_dvt.vmt_min = MinVmt;
		 comp_dvt.vmb_min = MinVmb;
		 comp_dvt.def_vmt = DefVmt;
		 comp_dvt.def_vmh = DefVmh;
		 comp_dvt.def_vmf = DefVmf;
		 comp_dvt.def_vmb = DefVmb;
		 comp_dvt.pdw_max = MaxPageWidth;
		 comp_dvt.pdl_max = MaxPageLength;
		 comp_dvt.upshift = 0;
		 comp_dvt.init_ps = 0;
		 comp_dvt.lettersp = Letterspace;
		 comp_dvt.max_pages = MaxPages;
		 comp_dvt.max_files = MaxFiles;
		 comp_dvt.init_family = "";
		 comp_dvt.init_member = "";
		 comp_dvt.atd_r = Atd_r;
		 comp_dvt.dvc_r = ""b;
		 comp_dvt.comment_r = Com_r;
		 comp_dvt.cleanup_r = Clean_r;
		 comp_dvt.medsel_table_r = ""b;
		 comp_dvt.foot_family = "";
		 comp_dvt.foot_member = "";

		 comp_dvt.sws.interleave = Interleave;
		 comp_dvt.sws.justifying = Justify;
		 comp_dvt.sws.mbz = "0"b;
		 comp_dvt.sws.endpage = EndPage;
		 comp_dvt.open_mode = Openmode;
		 comp_dvt.recleng = TapeRec;
		 comp_dvt.family_ct = 0]			       /\

Device_2
     / units : <unitkey> ;
	/LEX(2)	[hscale = hscales (token.Nvalue);
	           vscale = vscales (token.Nvalue)]
	 LEX(2)					       / Device_2 \
     / artproc : <ident>
	/LEX(2)	[prent.artproc = token_value]
	 LEX(1)					        / artproc \
     / attach : <quoted-string> ;
	/LEX(2)	[the_string = token_value;
		 comp_dvt.atd_r = rel (find_str (2))]
	 LEX(2)					       / Device_2 \
     / cleanup :
	/LEX(2) PUSH (cleanup)[call push ("cleanup")]	      / mc_string \
     / comment : <quoted-string> ;
	/LEX(2)	[the_string = token_value;
		 comp_dvt.comment_r = rel (find_str (2));
		 if length (token_value) > length (the_string)
		 then call ERROR (comment_gt_8000)]
	 LEX(2)					       / Device_2 \

     / defaultmargs : <num> , <num> , <num> , <num> ;
	/LEX(2)	[comp_dvt.def_vmt = scale_unit (vscale)]
	 LEX(2)	[comp_dvt.def_vmh = scale_unit (vscale)]
	 LEX(2)	[comp_dvt.def_vmf = scale_unit (vscale)]
	 LEX(2)	[comp_dvt.def_vmb = scale_unit (vscale)]
	 LEX(2)					       / Device_2 \
     / devclass : <quoted-string> ;
	/LEX(2)	[comp_dvt.devclass = token_value]
	 LEX(2)					       / Device_2 \
     / devname : <quoted-string> ;
	/LEX(2)	[do dvid_p = dvidl_p (1) repeat (dvid.next)
		    while (dvid_p ^= null ());
		    if dvid.dvt_ndx = dvt_ct
		    then dvid.devname = token_value;
		 end]
	 LEX(2)					       / Device_2 \
\"     / dvc : <ident> ,
\"	/LEX(2)	[dvcname = token_value]
\"	 LEX(2)	[dvcproc, the_string = ""]		       / dvc_1    \
     / endpage : <all_input> ;
	/LEX(2)	[comp_dvt.endpage = unspec (Input)]
	 LEX(2)					       / Device_2 \
     / family :
	/	[bach_sw = "0"b]
	 LEX(2) PUSH(family)[call push("family")]	     / add_family \
     / footproc :
	/LEX(2)					       / footproc \
     / init :
	/LEX(2)					        / init_f0 \
     / interleave : <switch> ;
	/LEX(2)	[comp_dvt.interleave = (token.Nvalue > 0)]     / Device_2 \
     / justify : <switch> ;
	/LEX(2)	[comp_dvt.justifying = (token.Nvalue > 0)]     / Device_2 \
     / letterspace : <num> ;
	/LEX(2)	[comp_dvt.lettersp = token.Nvalue]
	 LEX(2)					       / Device_2 \
     / maxfiles : <limit> ;
	/LEX(2)	[comp_dvt.max_files = token.Nvalue]
	 LEX(2)					       / Device_2 \
     / maxpages : <limit> ;
	/LEX(2)	[comp_dvt.max_pages = token.Nvalue]
	 LEX(2)					       / Device_2 \
     / maxpagelength : <limit> ;
	/LEX(2)	[comp_dvt.pdl_max = scale_unit (vscale)]
	 LEX(2)					       / Device_2 \
     / maxpagewidth : <num> ;
	/LEX(2)	[comp_dvt.pdw_max = scale_unit (hscale)]
	 LEX(2)					       / Device_2 \
     / minbotmarg : <num> ;
	/LEX(2)	[comp_dvt.vmb_min = scale_unit (vscale)]
	 LEX(2)					       / Device_2 \
     / minlead : <num> ;
	/LEX(2)	[comp_dvt.min_lead = scale_unit (vscale)]
	 LEX(2)					       / Device_2 \
     / minspace : <num> ;
	/LEX(2)	[comp_dvt.min_WS = scale_unit (hscale)]
	 LEX(2)					       / Device_2 \
     / mintopmarg : <num> ;
	/LEX(2)	[comp_dvt.vmt_min = scale_unit (vscale)]
	 LEX(2)					       / Device_2 \
     / outproc : <ident>
	/LEX(2)	[prent.outproc = token_value]
	 LEX(1)					        / outproc \
     / stream : <switch> ;
	/LEX(2)	[comp_dvt.open_mode = 5 - 3 * token.Nvalue]
	 LEX(2)					       / Device_2 \
     / taperec : <limit> ;
	/LEX(2)	[comp_dvt.recleng = token.Nvalue]
	 LEX(2)					       / Device_2 \
     / bachelor :
	/	[bach_sw = "1"b]
	 LEX(2) PUSH(bachelor)[call push("bachelor")]	     / add_family \
     / viewselect :
	/LEX(2)					     / viewselect \
     / Device
	/					      / endDevice \
     / <no-token>
	/					      / endDevice \
     /	/ PUSH (Device_2)[call push ("Device_2")]	  / global_device \

endDevice
     /	/	[tp = Pthis_token;
		 Ptoken, Pthis_token = Device_Pthis_token;

		 done = "0"b;
		 do dvid_p = dvidl_p (1) repeat (dvid.next)
		    while ((dvid_p ^= null ()) & ^done);
		    if (dvid.dvt_ndx = dvt_ct)
		    then if (dvid.devname = "")
		    then do;
		       call ERROR (no_devname);
		       done = "1"b;
		    end;
	           end;
		 dvid_p = dvidl_p (2);
		 if (comp_dvt.family_ct = 0)
		 then call ERROR (no_fonts_selected);
		 if (initfamily = "")
		 then call ERROR (no_init_font);
		 if (footfamily = "")
		 then do;
		    footfamily = initfamily;
		    footmember = initmember;
		 end;

		 views_selected = 0;
		 do i = 1 to view.count;
		    if (med_sel_tab.ref_r (i) ^= "0"b)
		    then views_selected = views_selected + 1;
		 end;

		 do i = 1 to comp_dvt.family_ct;
		    mem_p = ptr (area1_p, comp_dvt.member_r (i));
		    member_ptr = mem.ref_p;
		    do ii = 1 to member.count;
		       if initfamily = comp_dvt.family (i).name
		       & initmember = member.name (ii)
		       then do;
			comp_dvt.init_fam = i;
			comp_dvt.init_family = initfamily;
			comp_dvt.init_mem = ii;
			comp_dvt.init_member = initmember;
		       end;
		       if footfamily = comp_dvt.family (i).name
		       & footmember = member.name (ii)
		       then do;
			comp_dvt.foot_fam = i;
			comp_dvt.foot_family = footfamily;
			comp_dvt.foot_mem = ii;
			comp_dvt.foot_member = footmember;
		       end;

		       if views_selected < view.count
		       then do;
			fnt_p = ptr (area2_p, member.font_r (ii));
			font_ptr = fnt.pt;
			uni_p = ptr (fnt.pt, font.units_r);
			units_ptr = uni.ref_p;
			opu_p = ptr (fnt.pt, font.oput_r);
			oput_p = opu.ref_p;

			do iii = 0 to oput.data_ct;
			   j = oput.which (iii);
			   if (j > 0)	\" is the char defined?
			   then do;	\"  YES
			      if (med_sel_tab.ref_r (j) = "0"b)
			      then do;  	\" but you can't get at it!
			         call ERROR_ (no_viewselect,
				  view.name (j), dvid.refname);
			         med_sel_tab.ref_r (j) = "000001"b3;
				\" don't want to say this again.
			         views_selected = views_selected + 1;
			      end;
			   end;
			end;
		       end;
		    end;
		 end;
		 if (comp_dvt.init_family = "")
		 then call ERROR (init_font_not_on_Device);
		 if (comp_dvt.foot_family = "")
		 then call ERROR (foot_font_not_on_Device);
		 Ptoken, Pthis_token = tp;
		 area_free_p = addr_inc (area_free_p, size (comp_dvt))]
				\" finish allocation
						      / stack_pop \

artproc
     / $ <ident> ;
	/LEX(1)	[prent.artproc = prent.artproc || "$";
		 prent.artproc = prent.artproc || token_value]
	 LEX(2)					       / Device_2 \
     /	/LEX(1)	[prent.artproc 
		    = prent.artproc || "$" || prent.artproc]   / Device_2 \

outproc
     / $ <ident> ;
	/LEX(1)	[prent.outproc = prent.outproc || "$" || token_value]
	 LEX(2)					       / Device_2 \
     / ;	/LEX(1)					       / Device_2 \
     /	/[call ERROR (syntax_outproc)] NEXT_STMT	       / Device_2 \

cleanup
     / ;	/LEX(1)	[comp_dvt.cleanup_r = rel (find_str (2))]      / Device_2 \
     /	/[call ERROR (syntax_cleanup)] NEXT_STMT	       / Device_2 \

add_family
     /	/	[new_family = "1"b]				       /\
family_1
     / <fam_bach>
          /	[if new_family
		 then do;
		    if (member_ptr = null ())
		    then mem_p = area1_p;
		    else mem_p = addr_inc ((member_ptr), size (member));
		    call link (meml_p, mem_p);
		    mem.seqno, mem.refno, mem_ct = mem_ct + 1;
		    member_ptr, mem.ref_p = addr (mem.dummy);
		    member.count = 0;
		    new_family = "0"b;
		 end;
		 comp_dvt.family_ct = comp_dvt.family_ct + 1;
		 comp_dvt.member_r (comp_dvt.family_ct) = rel (mem_p);
		 if ^bach_sw
		 then comp_dvt.family (comp_dvt.family_ct).name
		    = translate (token_value, az, AZ);
		 else comp_dvt.family (comp_dvt.family_ct).name
		    = token_value;
		 Scale_x, Scale_y = Scale_scale]
	 LEX(1)					       / family_2 \
     /	/[call ERROR (fam_bach_name_expected)]		      / stack_pop \
family_2
     / ,	/LEX(1)					       / family_1 \
     /	/					      / stack_pop \
family
     / ; member
          /LEX(1)					         / member \
family_err
     /    /[call ERROR (syntax_family)] NEXT_STMT		       / Device_2 \
member
     / member :
          /	[new_member = member.count+1]
	 LEX(2)					       / member_1 \
     /	/					         / endmem \

member_1
     / <membername>
          /	[member.count = member.count + 1;
		 member.font_r (member.count) = "0"b;
		 member.size_r (member.count) = "0"b;
		 member.name (member.count)
		    = translate (token_value, az, AZ)]
	 LEX(1)					       / member_2 \
     /    /[call ERROR (syntax_member)] NEXT_STMT		       / Device_2 \
member_2
     / ,	/LEX(1) 					       / member_1 \
     /	/PUSH (member) [call push ("member")]			       /\
member_3
     / <font_name>
	/LEX(1)					       / member_4 \
     /	/[call ERROR (no_fontname)]				       /\
member_4
     / ;	/LEX(1)					       / member_6 \
     /    /[call ERROR (syntax_member)] NEXT_STMT		      / stack_pop \
member_6
     / Scale :
	/LEX(2)					       / member_7 \
     /	/					       / member_A \
member_7
     / <num>
	/	[Scale_x, Scale_y
		    = convert (fd12_8, token_value)* Scale_scale]
	 LEX(1)					       / member_8 \
     /	/					      / Scale_err \
member_8
     / , <num>
	/LEX(1)	[Scale_y = convert (fd12_8, token_value) * Scale_scale]
	 LEX(1)					       / member_9 \
     /	/					      / Scale_err \
member_9
     / ;	/LEX(1)					       / member_A \
Scale_err
     /	/[call ERROR (syntax_Scale)] NEXT_STMT			       /\
member_A
     /	/	[the_string_r = rel (find_str (2));
		 do i = new_member to member.count;
		    member.font_r (i) = rel (the_font);
		    member.Scalex (i) = Scale_x;
		    member.Scaley (i) = Scale_y;
		    addr (member.size_r (i)) -> bfb = Sizes;
		 end]				      / stack_pop \

init_f0
     / <fam_mem>
	/LEX(1)	[initfamily = font_fam;
		 initmember = font_mem]		        / init_f2 \
     /	/[call ERROR (missing_font)]NEXT_STMT		       / Device_2 \
init_f2
     / <num>
	/	[comp_dvt.init_ps = scale_unit (1000)]
	 LEX(1)					        / init_f3 \
     /	/[call ERROR (no_init_ps)]NEXT_STMT		       / Device_2 \
init_f3
     / ;	/LEX(1)					       / Device_2 \
     /	/[call ERROR (missing_semicolon)]NEXT_STMT	       / Device_2 \

bachelor
     /	/	[new_member, member.count = 1;
		 member.font_r (1) = "0"b;
		 member.size_r (1) = "0"b;
		 member.Scalex (1) = Scale_x;
		 member.Scaley (1) = Scale_y;
		 member.name (1) = ""]
	PUSH (endmem) [call push ("endmem")]		       / member_3 \
endmem
     /	/	[done = "0"b;	\" put into "normal" form
		 do while (^done);
		    done = "1"b;
		    do i = 1 to member.count-1;
		       call memorder;
		    end;
		    if ^done
		    then do;
		       done = "1"b;
		       do i = member.count-1 to 1 by -1;
			call memorder;
		       end;
		    end;
		 end;
memorder: proc;
	if member.name (i) > member.name (i+1)
	then do;
	   member_hold = member.e (i);
	   member.e (i) = member.e (i+1);
	   member.e (i+1) = member_hold;
	   done = "0"b;
	end;
       end memorder;
		 tp = meml_p (2);	\" see if member is like a prior one
		 done = "0"b;
		 do mem_p = meml_p (1) repeat (mem.next)
		    while (mem_p ^= meml_p (2));
		    if (mem.seqno = mem.refno)
		    then do;	\" check only "real" ones
		       if (unspec (mem.ref_p -> mem)
		          = unspec (tp -> mem.ref_p -> mem))
		       then do;
		          tp -> mem.refno = mem.seqno; \"its a duplicate
		          done = "1"b;
		       end;
		    end;
		 end]				       / Device_2 \

\"dvc_1
\"     / <ident>$<ident>
\"	/	[dvcproc = token_value]
\"	 LEX(2)	[dvcentry = token_value]
\"	 LEX(1)					       	       /\
\"	/ <quoted-string>
\"	/	[the_string = token_value]
\"	 LEX(1)					       	       /\
\"     / ;	/LEX(1)	[the_string = ""] 			       / Device_2 \

footproc
     / <ident>
	/	[prent.footproc = token_value]
	 LEX(1)					         / foot_1 \
     / ,	/LEX(1)					         / foot_2 \
     / ;	/LEX(1)					       / Device_2 \
     /	/[call ERROR (syntax_footproc)] NEXT_STMT	       / Device_2 \

foot_1
     / $ <ident>
	/LEX(1)	[prent.footproc = prent.footproc || "$" || token_value]
	 LEX(1)						       /\

foot_2
     / , <fam_mem>
	/LEX(2)	[FootFamily = font_fam;
		 FootMember = font_mem]		         / foot_3 \
     /	/					         / foot_e \
foot_3
     / ;	/LEX(1)					       / Device_2 \
foot_e
     /	/[call ERROR (syntax_footproc)] NEXT_STMT	       / Device_2 \

viewselect
     / <is_viewname>
	/	[default_view = token.Nvalue;
		 this_view = -1]
	 LEX(1) PUSH (viewsel1)[call push ("viewsel1")]	      / mc_string \
viewselect_err
     /	/[call ERROR (syntax_viewselect)] NEXT_STMT	       / Device_2 \
viewsel1
     /	/	[med_sel_tab.ref_r (default_view) = rel (find_str (2))]  /\
     / ;	/LEX(1)					       / Device_2 \
     / ,	/LEX(1)					     / viewselect \
     /	/					 / viewselect_err \
++*/
%page;
compdv:
  proc;

    dcl version	   char (10) var static options (constant) init ("2.0a");
    dcl compdv_severity_
		   fixed bin (35) ext static;

    compstat$compconst.ptr = addr (compstat$compconst.ptr);
    dt_sw = "0"b;

/* initialize static on first call in the process */
    if first_time
    then
      do;
        breaks, ignored_breaks =
	   substr (collate (), 1, 33) || substr (collate (), 128, 1);
        breaks = breaks || ":,()$=";
        call lex_string_$init_lex_delims ("""", """", "/*", "*/", ";", "10"b,
				/* suppress quote, keep statement */
	   breaks, ignored_breaks, lex_delims, lex_ctl_chars);
        first_time = "0"b;		/* static init done, reset switch */
      end;

/* ******************** PROCESS COMMAND LINE******************** */

    call cu_$arg_count (nargs);	/* how many given? */

    compdv_severity_ = 5;		/* preset for command parser */

    if nargs = 0			/* if none are given ... */
    then
      do;
        call com_err_ (0, "compdv",
	   "(Vers. ^a) Proper usage is: compdv"
	   || " <input_pathname>{.compdv}^/^-[-check | -ck | -list | -ls]",
	   version);
        return;
      end;			/**/
				/* fetch input pathname */
    call cu_$arg_ptr (1, argp, argl, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compdv", "Reading input pathname.");
        return;
      end;

    if search ("<>", arg) = 0		/* if a search is needed */
    then
      do;				/* check entry name length */
        if length (before (arg, ".compdv")) > 25
        then
	do;
	  call com_err_ (0, "compdv", "Input entryname ""^a"" is too long",
	       rtrim (arg));
	  return;
	end;

        ename = before (arg, ".compdv");/* strip the suffix */

        call search_paths_$find_dir ("compose",
				/* use compose list */
	   null (), rtrim (ename) || ".compdv", "", dname, ercd);
        if ercd ^= 0
        then
	do;
	  call com_err_ (ercd, "compdv", "Searching for ""^a""",
	       rtrim (ename) || ".compdv");
	  return;
	end;
      end;

    else
      do;
        call expand_pathname_$add_suffix (arg, "compdv", dname, ename, ercd);
        if ercd ^= 0
        then
	do;
	  call com_err_ (ercd, "compdv", "Expanding path for ""^a""",
	       rtrim (arg));
	  return;
	end;			/**/
				/* trim the suffix */
        ename = before (ename, ".compdv");
      end;

    check_opt, list_opt = "0"b;	/* reset option flags */

    if nargs > 1			/* any control args? */
    then
      do;
        call cu_$arg_ptr (2, argp, argl, ercd);
        if ercd ^= 0
        then
	do;
	  call com_err_ (ercd, "compdv", "Reading control argument.");
	  return;
	end;

        if arg = "-check" | arg = "-ck"
        then check_opt = "1"b;

        else if arg = "-list" | arg = "-ls"
        then list_opt = "1"b;

        else
	do;
	  call com_err_ (error_table_$badopt, "compdv", """^a""", arg);
	  return;
	end;
      end;

    call hcs_$initiate_count (dname, rtrim (ename) || ".compdv", "",
         input_bitcount, 0, input_ptr, ercd);
    if input_ptr = null ()
    then
      do;
        call com_err_ (ercd, "compdv", "Initiating ^a>^a.compdv",
	   rtrim (dname), rtrim (ename));
        return;
      end;

    on condition (cleanup) call cleaner;/* we now need cleaning */

    input_charcount = divide (input_bitcount, 9, 24, 0);

    call translator_temp_$get_segment ("compdv", lex_temp_ptr, ercd);
    if ercd ^= 0			/* get a temp seg for lex_string_ */
    then
      do;
        call com_err_ (ercd, "compdv", "Getting a translator temp seg.");
        call cleaner;
        return;
      end;

    call get_temp_segments_ ("compdv", temp_ptrs, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compdv", "Getting temp segments");
        call cleaner;
        return;
      end;

/* ******************** INITIALIZE FOR EXECUTION ******************** */

    call ioa_ ("COMPDV ^a-^d", version, comp_dvid_version);

    compdv_severity_ = 0;		/* clear for execution */
    dcl_l_p (*) = null ();
    next_str_p = ptr (string_area_p, 1);/* next string definition */
    size_list_p = null ();

    area_free_p = area2_p;		/* next symbol declaration	       */
    mediachars_p = null ();		/* good housekeeping	       */
    media_p = null ();
    view_p = null ();
    Def_p = null ();

    dvid_ct = 0;
    dvidl_p (*) = null ();
    dvt_ct = 0;
    dvtl_p (*) = null ();

    font_count = 0;
    member_ptr = null ();
    fntl_p (*) = null ();
    meml_p (*) = null ();
    unil_p (*) = null ();
    opul_p (*) = null ();

    the_string = "";
    if rel (find_str (1))
    then ;			/* put null string as first	       */
    if rel (find_str (2))
    then ;			/*   string table entries	       */

    ArtProc, FootProc, OutProc, DisplayProc, OutEntry =
         rtrim (ename) || "_writer_";
    FootFamily, FootMember = "";
    Com_r, Clean_r = "0"b;
    Vscale = vscales (6);		/* default to points	       */
    Hscale = hscales (6);

    if input_charcount = 0
    then
      do;
        code = error_table_$zero_length_seg;
        goto empty_seg;
      end;

    call lex_string_$lex (input_ptr, input_charcount, 0, lex_temp_ptr, "1000"b,
         """", """", "/*", "*/", ";", breaks, ignored_breaks, lex_delims,
         lex_ctl_chars, null (), first_token_p, code);
    if code ^= 0
    then
      do;
empty_seg:
        if code = error_table_$zero_length_seg
        then call com_err_ (0, "compdv",
	        "Source contains no statements. ^a>^a.compdv.", dname, ename)
	        ;
        else call com_err_ (code, "compdv",
	        "^a does not end with a statement delimiter.",
	        pathname_ (dname, ename));
        call cleaner;
        return;
      end;

    Ptoken, Pthis_token = first_token_p;

/* ***************************** GO ***************************** */
    call SEMANTIC_ANALYSIS;
    compdv_severity_ = MERROR_SEVERITY;

    if MERROR_SEVERITY < 3
    then
      do;
        dvid_p = dvidl_p (1);
        ename = rtrim (dvid.refname) || ".comp_dsm";
        call iox_$attach_name ("comp_gen_", ALM,
	   "vfile_ " || rtrim (ename) || ".alm", null (), code);
        call iox_$open (ALM, 2, "0"b, code);
        call outputter;
        call iox_$close (ALM, code);
        call iox_$detach_iocb (ALM, code);

        if ^check_opt
        then
	do;
	  if list_opt
	  then call alm (ename, "-list");
	  else call alm (ename);

	  do dvid_p = dvidl_p (1) repeat (dvid.next)
	       while (dvid_p ^= null ());
	    call hcs_$chname_file (get_wdir_ (), ename, "",
	         rtrim (dvid.refname) || ".comp_dsm", code);
	    if (code = error_table_$segnamedup)
	    then code = 0;
	    if code ^= 0
	    then call com_err_ (code, "compdv",
		    "Trying to add name ^a.comp_dsm to ^a>^a",
		    dvid.refname, get_wdir_ (), ename);
	  end;
	end;
      end;

    call cleaner;
    return;

/**** +++[Syntax Function]++++++++ A_DEBUG +++++++++++++++++++++++++++++++++ */
/*							       */
/* This routine helps in debugging. To use it a change must be made to the   */
/* output of rdc before compilation. At the label RD_MATCH this must be put: */
/*      if db_sw then call a_debug;				       */

a_debug:
  proc;
    call ioa_$nnl (" ""^a""", token_value);
    if (token_value = ",") | (token_value = ";")
    then call ioa_$nnl ("^/");
  end a_debug;

/**** +++[Function]+++++++++++++++ ADDR_INC ++++++++++++++++++++++++++++++++ */
/*							       */
/* this is an addrel function which increments by double words	       */
addr_inc:
  proc (a_ptr, an_inc) returns (ptr);

    dcl a_ptr	   ptr,
        an_inc	   fixed bin (24);

    return (addrel (a_ptr, divide (an_inc + 1, 2, 17, 0) * 2));
  end addr_inc;

/**** +++[Syntax Function]++++++++ CHARLIST ++++++++++++++++++++++++++++++++ */
/*							       */
/* Tests for the chars of a quoted string being defined charnames. Each      */
/*  entry processes the next char in the list.			       */
/* USES:	token_value - current token				       */
/*	list_ndx - character to process this time		       */
/* SETS:	token.Nvalue - index of found charname			       */

charlist:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;

    if list_ndx > length (token_value)
    then return ("0"b);
    media2 = "[" || substr (token_value, list_ndx, 1) || "]";

    do i = 1 to mediachars.count;	/* look thru the mediachars list     */
      if (mediachars.name (i) = media2)
      then
        do;
	token.Nvalue = i;
	if dt_sw
	then call ioa_$nnl ("<charlist-^i>", list_ndx);
	list_ndx = list_ndx + 1;
	return ("1"b);
        end;
    end;
    call ERROR_ (not_charname, show_name (media2), "");
    return ("0"b);

  end charlist;

/**** +++[Syntax Function]++++++++ CHARNAME ++++++++++++++++++++++++++++++++ */
/*							       */
/* Tests for the token being a defined charname.			       */
/* USES:	token_value - current token				       */
/* SETS:	token.Nvalue - index of found charname			       */

charname:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;

    if input_ ()			/* (sets Input if true)	       */
    then media2 = "[" || Input || "]";
    else if ident_ ()
    then media2 = token_value;
    else return ("0"b);
    do i = 1 to mediachars.count;	/* look thru the mediachars list     */
      if (mediachars.name (i) = media2)
      then
        do;
	token.Nvalue = i;
	if dt_sw
	then call ioa_$nnl ("<charname>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end charname;

/**** +++[Procedure]++++++++++++++ CLEANER +++++++++++++++++++++++++++++++++ */
/*							       */
/* Does all the needed stuff for condition(cleanup). However, doesn't report */
/*  any errors since we may be in trouble.			       */

cleaner:
  proc;

    if db_sw
    then call ioa_ ("===cleaner");

    call hcs_$terminate_noname (input_ptr, code);

    if lex_temp_ptr ^= null ()
    then call translator_temp_$release_all_segments (lex_temp_ptr, code);

    if temp_ptrs (1) ^= null ()
    then call release_temp_segments_ ("compdv", temp_ptrs, code);

    if ALM ^= null ()
    then
      do;
        call iox_$close (ALM, code);
        call iox_$detach_iocb (ALM, code);
        ALM = null ();
      end;

    if ^check_opt
    then call delete_$path (get_wdir_ (), rtrim (ename) || ".alm", "100100"b,
	    "compdv", code);

  end cleaner;

/**** +++[Syntax Function]+++++++++ DCL_ED +++++++++++++++++++++++++++++++++ */

dcl_ed:
  proc returns (bit (1) aligned);

    do dcl_p = dcl_l_p (1) repeat (dcl_.next) while (dcl_p ^= null ());
      if (dcl_.dcl_name = token_value)
      then
        do;
	str_p = addr (dcl_.leng);
	if dt_sw
	then call ioa_$nnl ("<dcl_ed>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end dcl_ed;

/**** +++[Procedure]+++++++++++++++ ERROR_ +++++++++++++++++++++++++++++++++ */
/*							       */
/* This routine prints error messages which need "non-standard" insertions.  */

ERROR_:
  proc (Nerror, Arg1, Arg2);

    dcl Nerror	   fixed bin,
        Arg1	   char (*),	/* The need is currently for 2       */
        Arg2	   char (*);	/* arguments, (may need expansion).  */

    dcl Pstmt	   ptr,
        1 erring_token aligned based (Perring_token) like token,
        Perring_token  ptr,
        erring_token_value
		   char (erring_token.Lvalue) based (erring_token.Pvalue);
    dcl lex_error_	   entry options (variable);

    Perring_token = Pthis_token;

    if error_control_table.Soutput_stmt (Nerror)
    then Pstmt = erring_token.Pstmt;	/* addr statement descriptor.    */
    else Pstmt = null ();

    call lex_error_ (Nerror, SERROR_PRINTED (Nerror),
         (error_control_table.severity (Nerror)), MERROR_SEVERITY, Pstmt,
         null (), SERROR_CONTROL, (error_control_table.message (Nerror)),
         (error_control_table.brief_message (Nerror)), Arg1, Arg2);

    compdv_severity_ =
         max (compdv_severity_, error_control_table.severity (Nerror));
  end ERROR_;

/**** +++[Syntax Function]++++++++ FAM_MEM +++++++++++++++++++++++++++++++++ */

fam_mem:
  proc returns (bit (1) aligned);

    if token.quoted_string		/* quoted string?		       */
         | token_value = "SELF"	/* the reserved word?	       */
    then return ("0"b);		/* any of these, return false	       */
				/* extract the first name */
    font_fam = before (token_value, "/");
    if font_fam = ""		/* no family given */
    then return ("0"b);		/**/
				/* extract possible second name */
    font_mem = after (token_value, "/");/* invalid names? */
    if (verify (font_fam, az_AZ09) ^= 0) | (verify (font_mem, az_AZ09) ^= 0)
         | (search (font_fam, "0123456789_") = 1)
         | (search (font_mem, "0123456789_") ^= 0)
    then return ("0"b);

    if (index (token_value, "/") ^= 0)
    then
      do;
        font_mem = "/" || rtrim (font_mem);
        font_fam = translate (font_fam, az, AZ);
        font_mem = translate (font_mem, az, AZ);
      end;

    if dt_sw
    then call ioa_$nnl ("<fam_mem>");

    return ("1"b);

  end fam_mem;

/**** +++[Syntax Function]++++++++ FAM_BACH ++++++++++++++++++++++++++++++++ */

fam_bach:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;
    dcl name	   char (32);

    if token.quoted_string		/* quoted string?		       */
         | token_value = "SELF"	/* the reserved word?	       */
    then return ("0"b);		/* any of these, return false	       */
				/* invalid names? */
    if (verify (token_value, az_AZ09) ^= 0)
         | (search (token_value, "0123456789_") = 1)
    then return ("0"b);

    if ^bach_sw
    then name = translate (token_value, az, AZ);
    else name = token_value;

    do i = 1 to comp_dvt.family_ct;
      if name = comp_dvt.family (i).name
      then
        do;
	call ERROR (duplicate_font_name);
	return ("0"b);
        end;
    end;

    if dt_sw
    then call ioa_$nnl ("<fam_bach>");

    return ("1"b);

  end fam_bach;

/**** +++[Function]++++++++++++++ FIND_FONT ++++++++++++++++++++++++++++++++ */

find_font:
  proc (create) returns (ptr);

/* PARAMETERS */

    dcl create	   bit (1);	/* 1 = font is to be created */

/* LOCAL STORAGE */

    dcl tp	   ptr;
    dcl fname	   char (32);

    if db_sw
    then call ioa_ ("===find_font");

    fname = token_value;
    if token_value = "SELF"		/* can't use "SELF" as a font name */
         | token.quoted_string	/* can't be a literal */
         | octal_ ()		/* or an octal value */
         | num ()			/* or a numeric */
    then
      do;
        if create
        then goto bad_news;
        return (null ());
      end;			/* go thru all defined fonts	       */
    do tp = fntl_p (1) repeat (tp -> fnt.next) while (tp ^= null ());
      if tp -> fnt.name = token_value	/* is this the one we want?	       */
      then return (tp);		/*  YES, return its addr	       */
    end;

    if ^create			/* not found; if not creating	       */
    then return (null);		/* return a null value	       */

    if ^ident_ ()			/* but must be a legal name	       */
    then
      do;
bad_news:
        call ERROR (not_valid_Font_name);
        fname = "";			/* supply something		       */
      end;

    if (font_count > 0)
    then area_free_p = addr_inc (oput_p, size (oput));
    tp = area_free_p;
    area_free_p = addr (tp -> fnt.dummy);
    font_count = font_count + 1;	/* record new font info */
    call link (fntl_p, tp);
    tp -> fnt.name = fname;		/* fill in the internal font name    */
    tp -> fnt.refno = font_count;	/*  and the reference #	       */
    tp -> fnt.node = Ptoken;		/* keep statement ptr for error msgs */
    tp -> fnt.pt = null ();		/* no table started yet	       */

    return (tp);			/* return the new addr	       */

  end find_font;

/**** +++[Function]+++++++++++++++ FIND_STR ++++++++++++++++++++++++++++++++ */
/*							       */
/* Finds the location of a string in a string table. If the string is not    */
/* in the table, then it is entered.				       */

find_str:
  proc (which) returns (ptr);

    dcl which	   fixed bin;	/* 1- temporary string area	       */
				/* 2- DSM string area	       */

    dcl i		   fixed bin;

    if dt_sw
    then call ioa_$nnl ("`^a'", the_string);
    if (string_l (which) > 0) & (length (the_string) = 0)
    then
      do;
        if dt_sw
        then call ioa_ ("--is ^i,1", which);
        return (strl_p (which, 1));
      end;


    do i = 1 to string_l (which);
      str_p = strl_p (which, i);
      if (length (bstr.str) = length (the_string))
      then if (bstr.str = the_string)
	 then
	   do;
	     if dt_sw
	     then call ioa_ ("--found ^i,^i", which, i);
	     return (str_p);
	   end;
    end;
    str_p = next_str_p;
    bstr.leng = length (the_string);
    bstr.str = the_string;
    string_l (which), i = string_l (which) + 1;
    strl_p (which, i) = str_p;
    next_str_p = addr (bstr.dummy);
    if dt_sw
    then call ioa_ ("--new ^i,^i", which, i);
    return (strl_p (which, i));

  end find_str;

/**** +++[Syntax Function]+++++++ FONT_NAME ++++++++++++++++++++++++++++++++ */
/*							       */
/* Test for token being a defined fontname.			       */

font_name:
  proc returns (bit (1) aligned);

    the_font = find_font ("0"b);
    if the_font ^= null ()
    then
      do;
        if dt_sw
        then call ioa_$nnl ("<font_name>");
        return ("1"b);
      end;
    the_font = fntl_p (1);		/* fill in a value so program will   */
				/*  keep running		       */
    return ("0"b);

  end font_name;

/**** +++[Syntax Function]++++++ IDENT/IDENT2 ++++++++++++++++++++++++++++++ */
/*							       */
/* check for legal <name> string				       */

ident:
  proc returns (bit (1) aligned);

    ldt_sw = dt_sw;
    goto start;

ident2:
  entry returns (bit (1) aligned);

    ldt_sw = dt_sw;
    if (token.Lvalue = 1)
    then return ("0"b);
    goto start;

ident_:
  entry returns (bit (1) aligned);

    ldt_sw = "0"b;			/* never db displays */

    dcl ldt_sw	   bit (1);

start:
    if token.quoted_string		/* quoted string?		       */
         | token_value = "SELF"	/* the reserved word?	       */
         | verify (token_value, az_AZ09) ^= 0
				/* non-(alphanumeric or _)?    */
    then return ("0"b);		/* any of these, return false	       */

    if (index ("0123456789_", substr (token_value, 1, 1)) ^= 0)
    then return ("0"b);
    if ldt_sw
    then call ioa_$nnl ("<ident>");
    return ("1"b);			/* must not have leading number or _ */
  end ident;

/**** +++[Syntax Function]++++ INPUT/ALL_INPUT +++++++++++++++++++++++++++++ */
/*							       */
/* Tests for token being a single char in either octal or quoted form.       */
/* ALL_INPUT also checks for a whole slew of builtin char names.	       */
/* SETS:	Input - 9-bit char value which results			       */

input:
  proc returns (bit (1) aligned);

    dcl which	   char (12);
    dcl ldt_sw	   bit (1);

    which = "<input>";
    ldt_sw = dt_sw;
    goto some;

input_:
  entry returns (bit (1) aligned);

    ldt_sw = "0"b;			/* never db displays */
    goto some;

all_input:
  entry returns (bit (1) aligned);

    which = "<all_input>";
    ldt_sw = dt_sw;

    if (token_value = "EM")
    then Input = EM;

    else if (token_value = "EN")
    then Input = EN;

    else if (token_value = "THICK")
    then Input = THICK;

    else if (token_value = "MEDIUM")
    then Input = MEDIUM;

    else if (token_value = "THIN")
    then Input = THIN;

    else if (token_value = "HAIR")
    then Input = HAIR;

    else if (token_value = "DEVIT")
    then Input = DEVIT;

    else if (token_value = "STROKE")
    then Input = STROKE;

    else if (token_value = "EM-")
    then Input = EMdash;

    else if (token_value = "EN-")
    then Input = ENd;

    else if (token_value = "EM_")
    then Input = EM_;
    else if (token_value = "EN_")
    then Input = EN_;
    else if (token_value = "^0")
    then Input = sup0;
    else if (token_value = "^1")
    then Input = sup1;
    else if (token_value = "^2")
    then Input = sup2;
    else if (token_value = "^3")
    then Input = sup3;
    else if (token_value = "^4")
    then Input = sup4;
    else if (token_value = "^5")
    then Input = sup5;
    else if (token_value = "^6")
    then Input = sup6;
    else if (token_value = "^7")
    then Input = sup7;
    else if (token_value = "^8")
    then Input = sup8;
    else if (token_value = "^9")
    then Input = sup9;
    else if (token_value = "''")
    then Input = rquote;
    else if (token_value = "``")
    then Input = lquote;
    else if (token_value = "PS")
    then Input = PS;
    else if (token_value = "lslnt")
    then Input = lslnt;
    else if (token_value = "vrule")
    then Input = vrule;
    else if (token_value = "bullet")
    then Input = bullet;
    else if (token_value = "cright")
    then Input = cright;
    else if (token_value = "modmark")
    then Input = modmark;
    else if (token_value = "delmark")
    then Input = delmark;
    else if (token_value = "multiply")
    then Input = multiply;
    else if (token_value = "nabla")
    then Input = nabla;
    else if (token_value = "pl_mi")
    then Input = pl_mi;
    else
some:
         if token.quoted_string
    then
      do;
        if token.Lvalue ^= 1
        then return ("0"b);
        Input = token_value;
      end;
    else if ^octal_ ()
    then return ("0"b);
    if ldt_sw
    then call ioa_$nnl ("^a", which);
    return ("1"b);

  end input;

/**** +++[Syntax Function]+++++++ IS_DEFNAME +++++++++++++++++++++++++++++++ */
/*							       */
/* Tests for a token being a defined Defname.			       */
/* SETS:	token.Nvalue - index of the found Defname		       */

is_Defname:
  proc returns (bit (1) aligned);

    do i = 1 to Def.count;
      if (Def.name (i) = token_value)
      then
        do;
	token.Nvalue = i;
	if dt_sw
	then call ioa_$nnl ("<is_Defname>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end is_Defname;

/**** +++[Syntax Function]++++++ IS_VIEWNAME +++++++++++++++++++++++++++++++ */
/*							       */
/* Tests for token being a defined viewname.			       */
/* SETS:	token.Nvalue - index of the found viewname		       */

is_viewname:
  proc returns (bit (1) aligned);

    do i = 1 to view.count;
      if (view.name (i) = token_value)
      then
        do;
	token.Nvalue = i;
	if dt_sw
	then call ioa_$nnl ("<is_viewname>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end is_viewname;

/**** +++[Procedure]++++++++++++++++ LINK ++++++++++++++++++++++++++++++++++ */
/*							       */
/* link an element to end of a list				       */

link:
  proc (l_p, e_p);
    dcl l_p	   (2) ptr,	/* begin/end list ptrs	       */
        e_p	   ptr;		/* element to be linked	       */

    dcl next	   ptr based (e_p); /* first word of element -> next     */

    if (l_p (1) = null ())
    then l_p (*) = e_p;		/* initialize list		       */
    else
      do;
        l_p (2) -> next = e_p;	/* last one points to this one       */
        l_p (2) = e_p;		/* this one is now last	       */
      end;
    next = null ();			/* this one points nowhere	       */

  end link;

/**** +++[Syntax Function]+++++++ MEDIANAME ++++++++++++++++++++++++++++++++ */
/*							       */
/* Test for the token being a defined medianame			       */
/* SETS: token.Nvalue - the index of the found medianame		       */

medianame:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;

    if ^ident_ ()
    then return ("0"b);
    do i = 1 to media.count;
      if (media.name (i) = token_value)
      then
        do;
	token.Nvalue = i;
	if dt_sw
	then call ioa_$nnl ("<medianame>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end medianame;

/**** +++[Syntax Function]+++++++ MEMBERNAME +++++++++++++++++++++++++++++++ */

membername:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;

    if (substr (token_value, 1, 1) ^= "/")
    then return ("0"b);
    if (token.Lvalue > 32)
    then return ("0"b);
    if (token.Lvalue > 1)
    then
      do;
        if (index ("0123456789", substr (token_value, 2, 1)) ^= 0)
        then return ("0"b);
        if (verify (substr (token_value, 2), az_AZ09) ^= 0)
        then return ("0"b);
      end;
    if dt_sw
    then call ioa_$nnl ("<membername>");
    return ("1"b);

  end membername;

/**** +++[Syntax Function]+++++++++ NEGNUM +++++++++++++++++++++++++++++++++ */
/*							       */
/* Tests token for being a negative number			       */

negnum:
  proc returns (bit (1) aligned);	/* check negative decimal value */

    if (substr (token_value, 1, 1) ^= "-")
				/* must start with - sign       */
    then return ("0"b);
    if (token_value = "-.")		/* just in case they throw a curve   */
    then return ("0"b);
    if (verify (substr (token_value, 2), "0123456789.") ^= 0)
				/* and have  */
    then return ("0"b);		/* only legal decimal chars &	       */
    if (index (after (token_value, "."), ".") ^= 0)
				/* only 1 decimal pt  */
    then return ("0"b);
    if dt_sw
    then call ioa_$nnl ("<negnum>");
    token.Nvalue = convert (token.Nvalue, token_value);
    return ("1"b);

  end negnum;

/**** +++[Syntax Function]+++++++ NUM/LIMIT ++++++++++++++++++++++++++++++++ */
/*							       */
/* Tests token for being UNLIMITED or being a number		       */
/* Tests token for being a number				       */

limit:
  proc returns (bit (1) aligned);

    if (token_value = "unlimited")
    then
      do;
        token.Nvalue = -1;
        return ("1"b);
      end;

num:
  entry returns (bit (1) aligned);	/* check decimal value */

    if token_value = "."
    then return ("0"b);
    if verify (token_value, "0123456789.") ^= 0
				/* legal decimal chars */
    then return ("0"b);

    if (index (after (token_value, "."), ".") ^= 0)
				/* only 1 dec pt */
    then return ("0"b);
    if dt_sw
    then call ioa_$nnl ("<num>");
    token.Nvalue = convert (token.Nvalue, token_value);
    return ("1"b);

  end limit;

/**** +++[Syntax Function]+++++++++ OCTAL ++++++++++++++++++++++++++++++++++ */
/*							       */
/* Tests token for being an octal character representation		       */
/* SETS:	Input - 9-bit char gotten by converting the 3 octal digits	       */

octal:
  proc returns (bit (1) aligned);

    ldt_sw = dt_sw;
    goto start;

octal_:
  entry returns (bit (1) aligned);

    ldt_sw = "0"b;			/* never db displays */

    dcl ldt_sw	   bit (1);
    dcl 1 bits	   (3) unal,	/* copy of the 3 token chars as bits */
	2 f	   bit (6),
	2 b	   bit (3);

start:
    if token.Lvalue ^= 3		/* if token is not exactly 3 chars */
    then return ("0"b);		/* it can't be octal */

    if verify (token_value, "01234567") ^= 0
				/* it can't have any chars */
    then return ("0"b);		/* outside the octal range */

    string (bits) = unspec (token_value);
				/* copy token into structure */
    unspec (Input) = b (1) || b (2) || b (3);
				/* convert octal to binary */
    if ldt_sw
    then call ioa_$nnl ("<octal>");
    return ("1"b);

  end octal;

/**** +++[Procedure]+++++++++++++ OUTPUTTER ++++++++++++++++++++++++++++++++ */
/*							       */
/* Outputs the whole schmeer to an alm source file.		       */
/* USES:	most everything of value				       */

outputter:
  proc;

    dcl addname	   bit (1);	/* 1 = table name is an addname      */
/**** format: off */
dcl bitname	(0:511) char (16)	/* char names for tables	       */
		int static options (constant) init
  ("000", "001", "002", "003", "004", "005", "006", "007", "010 BSP", "011 HT",
   "012 NL", "013 VT", "014 FF", "015 CR", "016", "017", "020", "021 ctl-str",
   "022", "023", "024", "025", "026", "027", "030", "031", "032", "033 ESC",
   "034", "035", "036", "037", "040 SP", "041 !", "042 """, "043 #", "044 $",
   "045 %", "046 &", "047 '", "050 Lp", "051 Rp", "052 *", "053 +", "054 ,",
   "055 -", "056 .", "057 /", "060 0", "061 1", "062 2", "063 3", "064 4",
   "065 5", "066 6", "067 7", "070 8", "071 9", "072 :", "073 ;", "074 <",
   "075 =", "076 >", "077 ?", "100 @", "101 A", "102 B", "103 C", "104 D",
   "105 E", "106 F", "107 G", "110 H", "111 I", "112 J", "113 K", "114 L",
   "115 M", "116 N", "117 O", "120 P", "121 Q", "122 R", "123 S", "124 T",
   "125 U", "126 V", "127 W", "130 X", "131 Y", "132 Z", "133 [", "134 \",
   "135 ]", "136 ^", "137 _", "140 `", "141 a", "142 b", "143 c", "144 d",
   "145 e", "146 f", "147 g", "150 h", "151 i", "152 j", "153 k", "154 l",
   "155 m", "156 n", "157 o", "160 p", "161 q", "162 r", "163 s", "164 t",
   "165 u", "166 v", "167 w", "170 x", "171 y", "172 z", "173 {", "174 |",
   "175 }", "176 ~", "177 PAD", "200", "201", "202", "203", "204", "205",
   "206", "207", "210", "211", "212", "213", "214", "215", "216", "217",
   "220", "221", "222", "223", "224", "225", "226", "227", "230", "231",
   "232", "233", "234", "235", "236", "237", "240", "241", "242", "243",
   "244", "245", "246", "247", "250", "251", "252 mlpy", "253 +_", "254 nabla",
   "255 EMdash", "256", "257 slash", "260", "261 dagger", "262", "263", "264",
   "265", "266", "267", "270", "271", "272", "273 _|", "274", "275 /=", "276",
   "277", "300", "301 dbl dagger", "302", "303 copyright", "304 delta", "305",
   "306", "307", "310", "311", "312", "313", "314", "315 bullet", "316||",
   "317", "320 PI", "321", "322", "323", "324", "325", "326 therefore", "327",
   "330", "331", "332 = ", "333", "334", "335", "336", "337 infinity", "340",
   "341", "342", "343", "344", "345", "346", "347", "350", "351", "352 theta",
   "353", "354", "355", "356", "357", "360 pi", "361", "362", "363", "364",
   "365", "366", "367", "370", "371", "372", "373", "374", "375 square",
   "376 overbar", "377 punct SP", "400 superior 0", "401 superior 1",
   "402 superior 2", "403 superior 3", "404 superior 4", "405 superior 5",
   "406 superior 6", "407 superior 7", "410 superior 8", "411 superior 9",
   "412 EM", "413 EM _dash", "414 EN", "415 EN _dash", "416 EN dash",
   "417 thin space", "420", "421 ``", "422 ''", "423 1hi X", "424",
   "425 v|", "426", "427 dia left", "430 delete mark", "431 dia right",
   "432 dia top", "433 <", "434 1hi {", "435 1hi [", "436 left circle", "437",
   "440 ->", "441 1hi }", "442 1hi ]", "443 right circle", "444", "445 ^|",
   "446", "447", "450", "451", "452", "453", "454", "455", "456", "457",
   "460", "461", "462", "463", "464", "465", "466", "467", "470", "471",
   "472", "473", "474", "475", "476", "477", "500", "501", "502", "503",
   "504", "505", "506", "507", "510", "511", "512", "513", "514", "515",
   "516", "517", "520", "521", "522", "523", "524", "525", "526", "527",
   "530", "531", "532", "533", "534", "535", "536", "537", "540", "541",
   "542", "543", "544", "545", "546", "547", "550", "551", "552", "553",
   "554", "555", "556", "557", "560", "561", "562", "563", "564", "565",
   "566", "567", "570", "571", "572", "573", "574", "575", "576", "577",
   "600", "601", "602", "603", "604", "605", "606", "607", "610", "611",
   "612", "613", "614", "615", "616", "617", "620", "621", "622", "623",
   "624", "625", "626", "627", "630", "631", "632", "633", "634", "635",
   "636", "637", "640", "641", "642", "643", "644", "645", "646", "647",
   "650", "651", "652", "653", "654", "655", "656", "657", "660", "661",
   "662", "663", "664", "665", "666", "667", "670", "671", "672", "673",
   "674", "675", "676", "677", "700", "701", "702", "703", "704", "705",
   "706", "707", "710", "711", "712", "713", "714", "715", "716", "717",
   "720", "721", "722", "723", "724", "725", "726", "727", "730", "731",
   "732", "733", "734", "735", "736", "737", "740", "741", "742", "743",
   "744", "745", "746", "747", "750", "751", "752", "753", "754", "755",
   "756", "757", "760", "761", "762", "763", "764", "765", "766", "767",
   "770", "771", "772", "773", "774", "775", "776", "777");
/**** format:  on */

    dcl (i, j)	   fixed bin;	/* working index */
    dcl oct_p	   ptr;
    dcl jjj	   fixed bin;
    dcl 1 oct	   based,
	2 ct	   fixed bin (35),
	2 e	   (o_s) fixed bin (35);
    dcl o_s	   fixed bin;
    dcl out	   entry automatic options (variable);
    out = ioa_$ioa_switch;		/* to shrink the line size below     */

/* This writes things in this sequence:				       */
/*  1)  "include compdv"					       */
/*  2)  ({comp_dvid} segdef's)'s				       */
/*  3)  (comp_dvt member's med_sel)'s				       */
/*  4)  font's						       */
/*  5)  sizel's						       */
/*  6)  strings ...						       */
/*  7)  "end"						       */

    if db_sw
    then call ioa_ ("===outputter");

    call out (ALM, "^-include^-compdv");

    do dvid_p = dvidl_p (1) repeat (dvid.next) while (dvid_p ^= null ());
      if dvid.real
      then
        do;
	call out (ALM, "^/dvid.^i:", dvid.ndx);
	call out (ALM, "^-dvid.version^-^i", comp_dvid_version);
	call out (ALM, "^-dvid.devname^-^a,^i", dvid.devname,
	     length (dvid.devname));
	call out (ALM, "^-dvid.dvt_r^-dvt.^i", dvid.dvt_ndx);
        end;
      call out (ALM, "^/^-dvid_segdef^-^i,^a", dvid.ndx, dvid.refname);
    end;

    do dvt_p = dvtl_p (1) repeat (dvt.next) while (dvt_p ^= null ());
      prent_p = dvt.prent;
      const.devptr = dvt.ref;
      call out (ALM, "^-even^/dvt.^i:", dvt.ndx);
      call out (ALM, "^-dvt.devclass^-^a,^i", comp_dvt.devclass,
	 length (comp_dvt.devclass));
      call out (ALM, "^-dvt.outproc^-^a", prent.outproc);
      call out (ALM, "^-dvt.footproc^-^a", prent.footproc);
      call out (ALM, "^-dvt.artproc^-^a", prent.artproc);
      call out (ALM, "^-dvt.displayproc^-^a", DisplayProc || "$display");
      call out (ALM, "^-dvt.min_WS^-^i", comp_dvt.min_WS);
      call out (ALM, "^-dvt.min_lead^-^i", comp_dvt.min_lead);
      call out (ALM, "^-dvt.vmt_min^-^i", comp_dvt.vmt_min);
      call out (ALM, "^-dvt.vmb_min^-^i", comp_dvt.vmb_min);
      call out (ALM, "^-dvt.def_vmt^-^i", comp_dvt.def_vmt);
      call out (ALM, "^-dvt.def_vmh^-^i", comp_dvt.def_vmh);
      call out (ALM, "^-dvt.def_vmf^-^i", comp_dvt.def_vmf);
      call out (ALM, "^-dvt.def_vmb^-^i", comp_dvt.def_vmb);
      call out (ALM, "^-dvt.pdw_max^-^i", comp_dvt.pdw_max);
      call out (ALM, "^-dvt.pdl_max^-^i", comp_dvt.pdl_max);
      call out (ALM, "^-dvt.upshift^-^i", comp_dvt.upshift);
      call out (ALM, "^-dvt.init_ps^-^i", comp_dvt.init_ps);
      call out (ALM, "^-dvt.lettersp^-^i", comp_dvt.lettersp);
      call out (ALM, "^-dvt.max_pages^-^i", comp_dvt.max_pages);
      call out (ALM, "^-dvt.max_files^-^i", comp_dvt.max_files);
      call out (ALM, "^-dvt.init_fam^-^i", comp_dvt.init_fam);
      call out (ALM, "^-dvt.init_mem^-^i", comp_dvt.init_mem);
      call out (ALM, "^-dvt.foot_fam^-^i", comp_dvt.foot_fam);
      call out (ALM, "^-dvt.foot_mem^-^i", comp_dvt.foot_mem);
      call out (ALM, "^-dvt.init_family^-^a,^i", comp_dvt.init_family,
	 length (comp_dvt.init_family));
      call out (ALM, "^-dvt.init_member^-^a,^i", comp_dvt.init_member,
	 length (comp_dvt.init_member));
      call out (ALM, "^-dvt.atd_r^2-^a", fmt_str_r (comp_dvt.atd_r));
      call out (ALM, "^-dvt.dvc_r^2-dvc^.3b", comp_dvt.dvc_r);
      call out (ALM, "^-dvt.comment_r^-^a", fmt_str_r (comp_dvt.comment_r));
      call out (ALM, "^-dvt.cleanup_r^-^a", fmt_str_r (comp_dvt.cleanup_r));
      call out (ALM, "^-dvt.medsel_table_r^-med_sel.^d", dvt.ndx);
      call out (ALM, "^-dvt.foot_family^-^a,^i", comp_dvt.foot_family,
	 length (comp_dvt.foot_family));
      call out (ALM, "^-dvt.foot_member^-^a,^i", comp_dvt.foot_member,
	 length (comp_dvt.foot_member));
      call out (ALM, "^-dvt.sws^2-^w", string (comp_dvt.sws));
      call out (ALM, "^-dvt.open_mode^-.^[str^;seq^]_out.",
	 (comp_dvt.open_mode = 2));
      call out (ALM, "^-dvt.recleng^-^i", comp_dvt.recleng);
      call out (ALM, "^-dvt.family_ct^-^i", comp_dvt.family_ct);

      do family_i = 1 to comp_dvt.family_ct;
        mem_p = ptr (area1_p, comp_dvt.family (family_i).member_r);
        call out (ALM, "^-dvt..member_r^-mem.^d", mem.refno);
        call out (ALM, "^-dvt..name^2-^a,^i", comp_dvt.family (family_i).name,
	   32);
      end;

      call out (ALM, "^/med_sel.^i:", dvt.ndx);
      call out (ALM, "^-med_sel_tab.count^-^i", view.count);
      do med_sel_i = 1 to view.count;
        call out (ALM, "^-med_sel_tab..ref_r^-^a^-^i",
	   fmt_str_r (med_sel_tab.ref_r (med_sel_i)), med_sel_i);
      end;
    end;

    do mem_p = meml_p (1) repeat (mem.next) while (mem_p ^= null ());
      if mem.refno = mem.seqno
      then
        do;
	member_ptr = mem.ref_p;
	call out (ALM, "mem.^d:", mem.seqno);
	call out (ALM, "^-member.count^-^i", member.count);
	do mem_i = 1 to member.count;
	  fnt_p = ptr (area2_p, member.font_r (mem_i));
	  call out (ALM, "^-member..font_r^-f.^i", fnt.refno);
	  call out (ALM, "^-member..size_r^-size.^i",
	       addr (member.size_r (mem_i)) -> bfb);
	  call out (ALM, "^-member..Scale^-^i,^i", member.Scalex (mem_i),
	       member.Scaley (mem_i));
	  call out (ALM, "^-member..name^-^a,^i", member.name (mem_i),
	       length (member.name (mem_i)));
	end;
        end;
    end;

    call out (ALM, "^|");		/* eject page before fonts	       */
    do fnt_p = fntl_p (1) repeat (fnt.next) while (fnt_p ^= null ());
      font_ptr = fnt.pt;
      uni_p = ptr (fnt.pt, font.units_r);
      opu_p = ptr (fnt.pt, font.oput_r);
      call out (ALM, "f.^i:^2-""^a", fnt.refno, fnt.name);
      call out (ALM, "^-font.oput_r^-opu.^i", opu.refno);
      call out (ALM, "^-font.units_r^-uni.^i", uni.refno);
      call out (ALM, "^-font.rel_units^-^i", font.rel_units);
      call out (ALM, "^-font.footsep^-(^1a)", font.footsep);
      call out (ALM, "^-font.fill^-   ");
      call out (ALM, "^-font.min_wsp^-^i", font.min_wsp);
      call out (ALM, "^-font.avg_wsp^-^i", font.avg_wsp);
      call out (ALM, "^-font.max_wsp^-^i", font.max_wsp);
    end;

    do uni_p = unil_p (1) repeat (uni.next) while (uni_p ^= null ());
      if uni.refno = uni.seqno
      then
        do;
	call out (ALM, "uni.^i:", uni.seqno);
	units_ptr = uni.ref_p;
	mediawidth = units (0);
	dup_ct = 1;
	do i = 1 to 511;
	  if (mediawidth = units (i))
	  then dup_ct = dup_ct + 1;
	  else
	    do;
	      call out (ALM, "^-units (^i),^[0^;^i^]", dup_ct,
		 (mediawidth = nulwidth), mediawidth);
	      mediawidth = units (i);
	      dup_ct = 1;
	    end;
	end;
	call out (ALM, "^-units (^i),^[0^;^i^]", dup_ct,
	     (mediawidth = nulwidth), mediawidth);
        end;
    end;

    do opu_p = opul_p (1) repeat (opu.next) while (opu_p ^= null ());
      if opu.refno = opu.seqno
      then
        do;
	call out (ALM, "opu.^i:", opu.seqno);
	oput_p = opu.ref_p;
	call out (ALM, "^-oput.data_ct^-^i", oput.data_ct);
	skip_ct = 0;
	do i = 0 to oput.data_ct;
	  if (oput.what_r (i) = "0"b)
	  then skip_ct = skip_ct + 1;
	  else
	    do;
	      if (skip_ct > 0)
	      then call out (ALM, "^-no_ch (^i)", skip_ct);
	      skip_ct = 0;
	      call out (ALM, "^-ch    ^i,^a  [^[null^s^;^i^]]^-^a",
		 oput.which (i), fmt_str_r ((oput.what_r (i))),
		 (units (i) = nulwidth), units (i), bitname (i));
	    end;
	end;
        end;
    end;

    do i = 1 to size_list.count;
      sizel_p = size_list.pt (i);
      call out (ALM, "^/size.^i:", i);
      call out (ALM, "^-sizel.val_ct^-^i", sizel.val_ct);
      do j = 1 to sizel.val_ct;
        call out (ALM, "^-sizel..val^-^d", sizel.val (j));
      end;
    end;

    call out (ALM, "^/.nul_str.:^-zero");
    do j = 2 to string_l (2);
      oct_p = strl_p (2, j);
      o_s = divide (oct_p -> bstr.leng + 3, 4, 17, 0);
      if j = 2
      then call out (ALM, "^/str:^-dec^-^d", oct_p -> oct.ct);
      else call out (ALM, "^/^-dec^-^d^2-^a", oct_p -> oct.ct,
	      fmt_str_r (rel (oct_p)));
      do jjj = 1 to o_s;
        call out (ALM, "^-oct^-^w^-^a", oct_p -> oct.e (jjj),
	   fmt_str_cmt (oct_p -> oct.e (jjj),
	   oct_p -> oct.ct - 4 * (jjj - 1)));
      end;
    end;

    call out (ALM, "^/^-end");

  end outputter;

/**** +++ OUTPUTTER UTILITY:  FMT_STR_CMT ++++++++++++++++++++++++++++++++++ */

fmt_str_cmt:
  proc (Oct, Len) returns (char (4) aligned);

    dcl Oct	   fixed bin (35);
    dcl Len	   fixed bin;
    dcl idx	   fixed bin;
    dcl str	   char (4) aligned;

    unspec (str) = unspec (Oct);
    if Len < 4
    then substr (str, Len + 1) = "";

    do idx = 1 to min (4, Len);
      if substr (str, idx, 1) = ";"
	 | rank (substr (str, idx, 1)) < rank (" ")
	 | rank (substr (str, idx, 1)) > rank ("~")
      then substr (str, idx, 1) = ".";
    end;

    return (str);

  end fmt_str_cmt;

/**** +++ OUTPUTTER UTILITY: FMT_STR_R +++++++++++++++++++++++++++++++++++++ */

fmt_str_r:
  proc (Rel) returns (char (9));

    dcl Rel	   bit (18) aligned;
    dcl pic	   picture "99999";

    if Rel = ""b
    then return (".no_repl.");

    if Rel = rel (strl_p (2, 1))
    then return (".nul_str.");

    pic = binary (Rel, 18) - wordno (strl_p (2, 2));
    return ("str+" || pic);

  end fmt_str_r;

/**** +++[Syntax Function]++++++++++ PART ++++++++++++++++++++++++++++++++++ */

part:
  proc returns (bit (1) aligned);

/**** format:  off */
dcl art_tokens	char (388) init	/* token string */
   ("[   ]   {   }   (   )   |   ||  o   /   X   d   m   \   c   t   " ||
    "v   ^   <-  ->  D^  D<  D>  Dv  Clf Crt -str-rul-stp|rul/rul\rul" ||
    "[tp ]tp {tp }tp lptprptp|tp ||tp[ht ]ht {ht }ht lphtrpht|ht ||ht" ||
    "[md ]md {md }md lpmdrpmd|md ||md[hb ]hb {hb }hb lphbrphb|hb ||hb" ||
    "[bt ]bt {bt }bt lpbtrpbt|bt ||bt[fl ]fl {fl }fl lpflrpfl|fl ||fl" ||
    "PI  pi  bxtlbxt bxtrbxl bxx bxr bxblbxb bxbrlztllztrlzl lzr lzbllzbr");
dcl art_codes	(97) char (1) init	/* codes */
  (art.one (1), art.one (2), art.one (3), art.one (4), art.one (5),
   art.one (6), art.one (7), art.one (8), art.one (9), art.one (10),
   art.one (11), art.one (12), art.one (13), art.lslnt, cright, tmark,
   art.daro, art.uparo, art.laro, art.raro, art.diam.top, art.diam.lvert,
   art.diam.rvert, art.diam.bottom, art.lcirc, art.rcirc, art.horiz.start,
   art.horiz.line, art.horiz.term, art.vpart, art.rslnt, art.lslnt,
   art.top (1), art.top (2), art.top (3), art.top (4), art.top (5),
   art.top (6), art.top (7), art.top (8), art.half_top (1), art.half_top (2),
   art.half_top (3), art.half_top (4), art.half_top (5), art.half_top (6),
   art.half_top (7), art.half_top (8), art.middle (1), art.middle (2),
   art.middle (3), art.middle (4), art.middle (5), art.middle (6),
   art.middle (7), art.middle (8), art.half_bottom (1), art.half_bottom (2),
   art.half_bottom (3), art.half_bottom (4), art.half_bottom (5),
   art.half_bottom (6), art.half_bottom (7), art.half_bottom (8),
   art.bottom (1), art.bottom (2), art.bottom (3), art.bottom (4),
   art.bottom (5), art.bottom (6), art.bottom (7), art.bottom (8),
   art.other_part (1), art.other_part (2), art.other_part (3),
   art.other_part (4), art.other_part (5), art.other_part (6),
   art.other_part (7), art.other_part (8), art.PI, art.pi,
   art.box.tl, art.box.t, art.box.tr, art.box.l, art.box.x, art.box.r, 
   art.box.bl, art.box.b, art.box.br, art.loz.tl, art.loz.tr,
   art.loz.l, art.loz.r, art.loz.bl, art.loz.br);
/**** format:  on */

    dcl i		   fixed bin;	/* working index */
    dcl part_token	   char (4);	/* token expanded to 4 chars */

    part_token = token_value;		/* copy the token */

    i = index (art_tokens, part_token); /* scan art tokens */
    if i > 0			/* found? */
    then
      do;
        i = divide (i, 4, 17, 0) + 1;	/* calculate code index */
        Input = art_codes (i);	/* fetch the code */
        if dt_sw
        then call ioa_$nnl ("<part>");
        return ("1"b);		/* return true */
      end;

    else return ("0"b);
  end part;

/**** +++[Debug Routine]+++++++++ PUSH/POP +++++++++++++++++++++++++++++++++ */
/*							       */

    dcl Stack	   (20) char (16);


/**** +++[Function]++++++++++++++ SCALE_UNIT +++++++++++++++++++++++++++++++ */
/*							       */
/* convert units to millipoints				       */

scale_unit:
  proc (the_scale) returns (fixed bin (31));

    dcl the_scale	   fixed bin (31);
    dcl (pi, pt)	   char (10);

    if (the_scale > 0)		/* not pica/point form	       */
    then return (the_scale * bin (before (token_value, "."))
	    +
	    divide (the_scale
	    * bin (substr (after (token_value, ".") || "000", 1, 3)), 1000,
	    17, 0));

    return (hscales (5) * bin (before (token_value, "."))
         + hscales (6) * bin (after (token_value, ".")));

  end scale_unit;

/**** +++[Syntax Function]++++++++ SIZENAME ++++++++++++++++++++++++++++++++ */

sizename:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;

    do i = 1 to size_list.count;	/* scan sizetable names */
      if size_list.name (i) = token_value
      then
        do;
	token.Nvalue = i;		/* set index value */
	if dt_sw
	then call ioa_$nnl ("<sizename>");
	return ("1"b);
        end;
    end;

    return ("0"b);
  end sizename;

/**** +++[Function]++++++++++++++ SHOW_NAME ++++++++++++++++++++++++++++++++ */
/*							       */
/* converts a MediaChar name into display form if needed.		       */
show_name:
  proc (str) returns (char (32));

    dcl str	   char (*);

    dcl bits	   (3) bit (3) unal;
    dcl bins	   (3) fixed bin (3) unsigned unal based (addr (bits));

    if (substr (str, 1, 1) ^= "[")
    then return (str);
    if (substr (str, 2, 1) > " ") & (substr (str, 2, 1) <= "~")
    then return ("""" || substr (str, 2, 1) || """");
    string (bits) = unspec (substr (str, 2, 1));
    return (substr ("01234567", bins (1) + 1, 1)
         || substr ("01234567", bins (2) + 1, 1)
         || substr ("01234567", bins (3) + 1, 1));

  end show_name;

/**** +++[Syntax Function]+++++++++ SWITCH +++++++++++++++++++++++++++++++++ */
/*							       */
/* check for on/off						       */
/* SETS:	token.Nvalue - 0 (off) 1 (on)				       */
switch:
  proc returns (bit (1) aligned);

    if token_value = "on"
    then token.Nvalue = 1;
    else if token_value = "off"
    then token.Nvalue = 0;
    else return ("0"b);
    if dt_sw
    then call ioa_$nnl ("<switch>");
    return ("1"b);

  end switch;

/**** +++[Syntax Function]+++++++ TABLE_NAME +++++++++++++++++++++++++++++++ */

table_name:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;	/* scan dvid list names */
    do dvid_p = dvidl_p (1) repeat (dvid.next) while (dvid_p ^= null ());
      if dvid.refname = token_value
      then
        do;
	if dvid.dvt_ndx = dvt_ct + 1
	then
	  do;
	    call ERROR (circular_Device_def);
	    return ("0"b);
	  end;
	token.Nvalue = dvid.dvt_ndx;	/* set index value		       */
	if dt_sw
	then call ioa_$nnl ("<table_name>");
	return ("1"b);
        end;
    end;
    return ("0"b);

  end table_name;

/**** +++[Syntax Function]++++++++ UNITKEY +++++++++++++++++++++++++++++++++ */

unitkey:
  proc returns (bit (1) aligned);

    dcl i		   fixed bin;	/* working index */
    dcl unit_key_list  (7) char (2)	/* list of Units keywords */
		   static options (constant)
		   init ("pi", "el", "in", "mm", "pc", "pt", "pp");

    do i = 1 to hbound (unit_key_list, 1)
         while (token_value ^= unit_key_list (i));
    end;

    if i > hbound (unit_key_list, 1)
    then
      do;
        call ERROR (inv_Units_keyword);
        return ("0"b);
      end;

    token.Nvalue = i;
    if dt_sw
    then call ioa_$nnl ("<unitkey>");
    return ("1"b);

  end unitkey;

/**** +++[Syntax Function]+++	VALID_DEVICE_NAME ++++++++++++++++++++++++++++ */
/*							       */
/* Test for token being a valid Device name, i.e. an <ident> which is not    */
/*  already defined as a Device name.				       */

valid_Device_name:
  proc returns (bit (1) aligned);

    if ^ident_ ()
    then return ("0"b);
    do dvid_p = dvidl_p (1) repeat (dvid.next) while (dvid_p ^= null ());
      if token_value = dvid.refname
      then
        do;
	call ERROR (dup_Device);
	return ("0"b);
        end;
    end;
    if dt_sw
    then call ioa_$nnl ("<valid_Device_name>");
    return ("1"b);

  end valid_Device_name;

/**** +++[Syntax Function]+++	VALID_MEDIA_NAME ++++++++++++++++++++++++++++ */
/*							       */
/* Test for token being a valid Media name, i.e. an <ident> which is not    */
/*  already defined as a Media name.				       */

valid_Media_name:
  proc returns (bit (1) aligned);

    if ^ident_ ()
    then return ("0"b);
    do i = 1 to media.count;
      if (media.name (i) = token_value)
      then
        do;
	call ERROR (dup_Media);
	return ("0"b);
        end;
    end;
    if dt_sw
    then call ioa_$nnl ("<valid_Media_name>");
    return ("1"b);

  end valid_Media_name;

save_unref:
    if "0"b
    then call a_debug;
    goto save_unref;

dbs:
  entry (xxxx);
    db_start = xxxx;
    return;
    dcl xxxx	   char (*);

dbn:
  entry;
    db_sw = "1"b;
    return;
dbf:
  entry;
    db_sw = "0"b;
    return;

dtn:
  entry;
    dt_sw = "1"b;
    return;
dtf:
  entry;
    dt_sw = "0"b;
    return;

trn:
  entry;
    tr_sw = "1"b;
    return;
trf:
  entry;
    tr_sw = "0"b;
    return;
%page;
/* +++++++++++++++++++++++++++ LOOSE VARIABLES +++++++++++++++++++++++++++++ */

    dcl ALM	   ptr init (null ());
				/* iocb pointer for alm output file */
    dcl arg	   char (argl) based (argp);
				/* a command line argument */
    dcl argl	   fixed bin;	/* length of arg */
    dcl argp	   ptr;		/* pointer to arg */
    dcl ArtEntry	   char (32) var init ("artproc");
				/* artwork proc entry */
    dcl ArtProc	   char (32) varying;
				/* artwork procedure entryname */
    dcl Atd_r	   bit (18) init ("000000"b3);
				/* default attach descr relp */
    dcl attach	   char (256) var;
    dcl AvgWordsp	   fixed bin init (-1);
				/* global average wordspace */
    dcl az_AZ09	   char (64) int static options (constant)
		   init ("abcdefghijklmnopqrstuvwxyz_"
		   || "ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789");
    dcl bach_sw	   bit (1);
    dcl bfb	   fixed bin (35) based aligned;
    dcl bpptr	   ptr based;
    dcl breaks	   char (128) var static;
				/* control string for lex_string_ */
    dcl ch1	   char (1);
    dcl char_val	   fixed bin;
    dcl charid	   fixed bin;
    dcl charid_	   fixed bin;
    dcl check_opt	   bit (1) static;	/* check mode flag */
    dcl cleanup	   condition;
    dcl code	   fixed bin (35);
    dcl Com_r	   bit (18);
    dcl Clean_r	   bit (18);
    dcl db_start	   char (12) int static init ("");
    dcl db_sw	   bit (1) int static init ("0"b);
    dcl dclname	   char (8);
    dcl default_view   fixed bin;
    dcl DefVmt	   fixed bin (31) init (48000);
    dcl DefVmh	   fixed bin (31) init (24000);
    dcl DefVmf	   fixed bin (31) init (24000);
    dcl DefVmb	   fixed bin (31) init (48000);
    dcl DevClass	   char (24) init ("typewriter");
				/* default device class */
    dcl Device_Pthis_token
		   ptr;
    dcl DevName	   char (24) init ("ascii");
				/* default device name */
    dcl dname	   char (168);	/* name of dir containing ename */
    dcl done	   bit (1);
    dcl dt_sw	   bit (1);
    dcl dup_ct	   fixed bin;
    dcl dvt_i	   fixed bin;
    dcl ename	   char (32);	/* input entryname (no suffix) */
    dcl EndPage	   bit (9) init ("0"b);
    dcl ercd	   fixed bin (35);	/* error code */
    dcl family_i	   fixed bin;
    dcl fd12_8	   fixed dec (12, 8);
    dcl first_time	   bit (1) static init ("1"b);
				/* initing control switch */
    dcl first_token_p  ptr;
    dcl font_fam	   char (32);
    dcl font_mem	   char (32);
    dcl FootEntry	   char (32) varying/* footnote procedure entrypoint */
		   init ("footproc");
    dcl footentry	   char (32);
    dcl FootFamily	   char (32);	/* global footnote font family name  */
    dcl footfamily	   char (32);
    dcl FootMember	   char (32);	/* global footnote font member name  */
    dcl footmember	   char (32);
    dcl FootProc	   char (32) varying;
				/* footnote procedure entryname */
    dcl Footsep	   char (1) init (",");
    dcl held_Pthis_token
		   ptr;
    dcl hold_Pthis_token
		   ptr;
    dcl Hscale	   fixed bin (31);	/* global hor scale */
    dcl hscale	   fixed bin (31);	/* local hor scale */
    dcl hscales	   (7) fixed bin (31)
				/* hor scale factors */
		   static options (constant)
		   init (7200, 6000, 72000, 2834.65, 12000, 1000, 0);
    dcl i		   fixed bin;
    dcl ignored_breaks char (128) var static;
				/* control string for lex_string_ */
    dcl ii	   fixed bin;
    dcl iii	   fixed bin;
    dcl initfamily	   char (32);
    dcl initmember	   char (32);
    dcl Input	   char (1);
    dcl input_bitcount fixed bin (24);	/* bit count for ename segment */
    dcl input_charcount
		   fixed bin (24);	/* char count for ename segment */
    dcl input_file	   char (input_charcount)
				/* source file overlay */
		   based (input_ptr);
    dcl input_ptr	   ptr;		/* point to ename segment */
    dcl Interleave	   bit (1) init ("0"b);
    dcl j		   fixed bin;
    dcl jj	   fixed bin;
    dcl Justify	   bit (1) init ("0"b);
    dcl Letterspace	   fixed bin (31) init (0);
    dcl lex_ctl_chars  char (128) var static;
				/* control string for lex_string_ */
    dcl lex_delims	   char (128) var static;
				/* control string for lex_string_ */
    dcl lex_temp_ptr   ptr init (null ());
				/* temp seg for lex_string_ */
    dcl like_table	   fixed bin;
    dcl list_ndx	   fixed bin;
    dcl list_opt	   bit (1);	/* list option flag */
				/* font locator */
    dcl loc_font	   fixed bin (35) based;
    dcl (
        MaxFiles,			/* global maximum file/reel	       */
        MaxWordsp,			/* global maximum wordspace	       */
        MaxPages,			/* global maximum pages/file	       */
        MaxPageLength
        )		   fixed bin (31) init (-1);
    dcl MaxPageWidth   fixed bin (31) init (979200);
    dcl media1	   char (32);
    dcl media2	   char (32);
    dcl mediabase	   fixed bin;
    dcl mediact	   fixed bin;
    dcl mediawidth	   fixed bin;
    dcl media_	   char (32);
    dcl media_i	   fixed bin;
    dcl 1 member_hold  like member.e;
    dcl mem_i	   fixed bin;
    dcl med_sel_i	   fixed bin;
    dcl MinLead	   fixed bin (31) init (7200);
				/* global minimum lead */
    dcl MinSpace	   fixed bin (31) init (7200);
    dcl MinWordsp	   fixed bin init (-1);
				/* global minimum wordspace */
    dcl MinVmb	   fixed bin (31) init (0);
    dcl MinVmt	   fixed bin (31) init (0);
    dcl mw	   fixed bin;
    dcl nargs	   fixed bin;	/* command line arg count */
    dcl new_family	   bit (1);
    dcl new_member	   fixed bin;
    dcl next_dcl_p	   ptr;
    dcl next_str_p	   ptr;
    dcl nulwidth	   fixed bin int static options (constant) init (-100000);
    dcl o777	   char (1) int static options (constant) init ("");
    dcl Openmode	   fixed bin init (5);
				/* opening mode for compout file */
    dcl OutEntry	   char (32) var;
    dcl OutProc	   char (32) var;
    dcl DisplayProc	   char (32) var;
    dcl parenct	   fixed bin;	/* next 4 vars for "n(medchars)"     */
				/*  and "n(output)"		       */
    dcl part_repl	   (10) fixed bin;	/* replication count for a part      */
    dcl part_str	   (10) char (400) var;
				/* the string for a part	       */
    dcl part_width	   (10) fixed bin;	/* the width of a part	       */
    dcl part_nest	   fixed bin;	/* nesting of parts		       */
    dcl Scale_scale	   fixed bin (35) int static options (constant)
		   init (100000000);
    dcl Scale_x	   fixed bin (35);
    dcl Scale_y	   fixed bin (35);
    dcl self_sw	   bit (1);
    dcl self_ct	   fixed bin;	/* number of SELFs in mediachar list */
    dcl self_i	   (16) fixed bin;	/* location of these SELFs	       */
    dcl Sizes	   fixed bin init (0);
    dcl skip_ct	   fixed bin;
    dcl string_l	   (2) fixed bin init (0, 0);
    dcl Strokes	   fixed bin init (1);
    dcl TapeRec	   fixed bin init (-1);
    dcl testwidth	   fixed bin;
    dcl the_font	   ptr;
    dcl the_string	   char (8000) var;
    dcl the_string_r   bit (18) aligned;/* offset in string table	       */
    dcl this_view	   fixed bin;
    dcl top_dcl_p	   ptr init (null ());
    dcl tp	   ptr;
    dcl tr_sw	   bit (1) int static init ("0"b);
    dcl vals_ct	   fixed bin;	/* count of entries in vals array */
    dcl vals	   (1:512) fixed bin;
    dcl views_selected fixed bin;
    dcl viewname	   char (32);
    dcl Vscale	   fixed bin (31);	/* global vertical scale */
    dcl vscale	   fixed bin (31);	/* local vertical scale */
    dcl vscales	   (7) fixed bin (31)
				/* vertical scale factors */
		   static options (constant)
		   init (12000, 9000, 72000, 2834.65, 12000, 1000, 0);
    dcl Wordspace_p	   ptr init (null ());

    dcl (addr, addrel, after, before, bin, bit, byte, collate, convert, copy,
        dec, dimension, divide, fixed, hbound, index, length, ptr, rel, size,
        unspec, verify, max, null, rank, rtrim, search, string, substr,
        translate)	   builtin;
%page;
/* ++++++++++++++++++ ERROR CODES & EXTERNAL PROCEDURES ++++++++++++++++++++ */

    dcl error_table_$badopt
		   fixed bin (35) ext static;
    dcl error_table_$namedup
		   fixed bin (35) ext static;
    dcl error_table_$segnamedup
		   fixed bin (35) ext static;
    dcl error_table_$zero_length_seg
		   fixed bin (35) ext static;

    dcl alm	   entry options (variable);
    dcl az	   char (26) int static
		   init ("abcdefghijklmnopqrstuvwxyz");
    dcl AZ	   char (26) int static
		   init ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    dcl com_err_	   entry options (variable);
    dcl cu_$arg_count  entry (fixed bin);
    dcl cu_$arg_ptr	   entry (fixed bin, ptr, fixed bin, fixed (35));
    dcl delete_$path   entry (char (*), char (*), bit (6) aligned, char (*),
		   fixed bin (35));
    dcl expand_pathname_$add_suffix
		   entry (char (*), char (*), char (*), char (*),
		   fixed (35));
    dcl get_temp_segments_
		   entry (char (*), (*) ptr, fixed bin (35));
    dcl get_wdir_	   entry returns (char (168));
    dcl hcs_$chname_file
		   entry (char (*), char (*), char (*), char (*),
		   fixed bin (35));
    dcl hcs_$initiate_count
		   entry (char (*), char (*), char (*), fixed bin (24),
		   fixed bin (2), ptr, fixed bin (35));
    dcl hcs_$terminate_noname
		   entry (ptr, fixed bin (35));
    dcl ioa_$ioa_switch
		   entry options (variable);
    dcl iox_$attach_name
		   entry (char (*), ptr, char (*), ptr, fixed bin (35));
    dcl iox_$close	   entry (ptr, fixed bin (35));
    dcl iox_$detach_iocb
		   entry (ptr, fixed bin (35));
    dcl iox_$open	   entry (ptr, fixed bin, bit (1) aligned, fixed bin (35));
    dcl lex_string_$init_lex_delims
		   entry (char (*), char (*), char (*), char (*), char (*),
		   bit (*), char (*) var, char (*) var, char (*) var,
		   char (*) var);
    dcl lex_string_$lex
		   entry (ptr, fixed bin (24), fixed bin (24), ptr,
		   bit (*), char (*), char (*), char (*), char (*),
		   char (*), char (*) var, char (*) var, char (*) var,
		   char (*) var, ptr, ptr, fixed bin (35));
    dcl pathname_	   entry (char (*), char (*)) returns (char (168));
    dcl release_temp_segments_
		   entry (char (*), (*) ptr, fixed bin (35));
    dcl search_paths_$find_dir
		   entry (char (*), ptr, char (*), char (*), char (*),
		   fixed (35));
    dcl translator_temp_$get_segment
		   entry (char (*), ptr, fixed bin (35));
    dcl translator_temp_$release_all_segments
		   entry (ptr, fixed bin (35));
%page;
/* ++++++++++++++++++++++++++++++ STRUCTURES +++++++++++++++++++++++++++++++ */

    dcl temp_ptrs	   (4) ptr init ((4) null ());

/* These 4 segments are used to hold these structures (in order):	       */
/*	1	2	3	4			       */
/*	strl_p	bstr		dcl_			       */
/*				mediachars		       */
/*				media			       */
/*				view			       */
/*				Def			       */
/*				fnt, font, units, oput ...	       */
/*				size_list, sizel...		       */
/*			mem	dvid..., dvt, med_sel, comp_dvt ...  */

    dcl strl_p	   (2, 2000) ptr based (temp_ptrs (1));
				/* list of strings */
    dcl string_area_p  ptr defined (temp_ptrs (2));
				/* place to hold strings    */
    dcl area1_p	   ptr defined (temp_ptrs (3));
    dcl area2_p	   ptr defined (temp_ptrs (4));
    dcl area_free_p	   ptr;		/* next free location in area2       */

    dcl size_list_p	   ptr;
    dcl 1 size_list	   based (size_list_p),
	2 count	   fixed bin,
	2 free	   ptr,		/* where to put next list	       */
	2 e	   (50),
	  3 name	   char (32),	/* name of size list	       */
	  3 pt	   ptr,		/* point to size list	       */
	2 start	   ptr;		/* start of list area	       */

/*	        COMPDV/COMP_DSM STRUCTURE INTERCONNECTION		       */

/*		 TABLES USED BY compdv WHILE PARSING,		       */
/*		NAMES MARKED WITH * ARE internal ONLY		       */
/*	      Tables are generally shown in order generated	       */
/*	       (except for strings, which crop up all over)	       */

/*++
/* dcl_l_p(1)>----+						       */
/* dcl_l_p(2)>-+  |				dcls are made first.       */
/*	     |  |	 dcl_*	    		They are strings which     */
/*	     |  |	  ________    		are referenced by name     */
/*	     |  +->|next    >--+		as an aid to understanding */
/*	     |	 |dcl_name|  |		the DSM definition. They   */
/*	     |	 |leng    |  |		are not necessary to do    */
/*	     |	 |dcl_v   |  |		the job.		       */
/*	     |	 |________|  |				       */
/*	     |  +--------------+	Strings used by mediachars are       */
/*	     |  |  dcl_*	    	temporary, i.e. only used by compdv, */
/*	     |  |   ________    	pointers to these go in strl_p(1,*). */
/*	     |  +->|next    >--+				       */
/*	     |	 |dcl_name|  |	Strings used by font, cleanup, etc.  */
/*	     | 	 |leng    |  |	are permanent, i.e. they end up in   */
/*	     |	 |dcl_v   |  |	the DSM, pointers to these go in     */
/*	     |	 |________|  |	strl_p(2,*).		       */
/*	     |  +--------------+				       */
/*	     |  |  dcl_*		strl_p*			       */
/*	     |  |   ________	 _________ 	bstr*	       */
/*	     +--+->|next    >null	|1,1 |2,1 >...	 ____	       */
/*		 |dcl_name|	|1,2 |2,2 >-------->|leng|	       */
/*	 	 |leng    |	.    .    .	|str |	       */
/*	 	 |dcl_v   |	:    :    :	|____|	       */
/*	 	 |________|				       */

    dcl str_p	   ptr;
    dcl 1 bstr	   based (str_p),	/* based string used for building    */
	2 leng	   fixed bin,	/*  pseudo-char_var strings	       */
	2 str	   char (bstr.leng),
	2 dummy	   bit (36) aligned;/* where next structure will go      */

    dcl dcl_l_p	   (2) ptr;	/* dcl_ list begin/end	       */
    dcl dcl_p	   ptr;
    dcl 1 dcl_	   based (dcl_p),	/* ** symbol declaration	       */
	2 next	   ptr,		/* linked list next element	       */
	2 dcl_name   char (8),	/* declared name		       */
	2 leng	   fixed bin,	/* length of definition string       */
	2 dcl_v	   char (dcl_.leng),/* symbol definition string	       */
	2 dummy	   ptr;		/* where next one is based	       */
%page;
/*      mediachars*						       */
/*       _______			Next, all mediachars are defined     */
/*      |count=n|__ 		in terms of dcl'ed symbols or	       */
/*   (1)|name|out_r>----------+	literals.			       */
/*   (2)|name|out_r>...	|		 	bstr*	       */
/*      .    .     .	|		 	 ____	       */
/*      :    :     :	+---------------------------->|leng|	       */
/*   (n)|name|out_r>...			 	|str |	       */
/*      |____|_____|			 	|____|	       */
    dcl mediachars_p   ptr;
    dcl 1 mediachars   based (mediachars_p),
	2 count	   fixed bin,	/* how many have been defined	       */
	2 e	   (mediachars.count),
	  3 name	   char (32),	/* name of the char		       */
	  3 out_r	   bit (18) aligned;/* output string to get it	       */

/*	       media*			       
/*	        _______			Then, all media are	       */
/*	 ______|count=m|_________ ... ______	described in terms of the  */
/*     (1)|name|rel_units|w11 |w12 |... |w1n |	mediachars, with the       */
/*     (2)|name|rel_units|w21 |w22 |... | @  |	widths being defined for   */
/*	.    .         .    .    .    .    .	each. Values might not     */
/*	:    :         :    :    :    :    :	exist for all mediachars   */
/*     (m)|name|rel_units|wm1 | @  |... |wmn |	in all media (shown as @). */
/*	|____|_________|____|____|... |____|			       */
/*    mediachar # -->     (1)  (2)  ...  (n)			       */
    dcl media_p	   ptr;
    dcl 1 media	   based (media_p),
	2 count	   fixed bin,	/* how many have been defined	       */
	2 e	   (media.count),
	  3 name	   char (32),	/* name of the media	       */
	  3 rel_units
		   fixed bin,	/* its stroke value		       */
	  3 width	   (mediachars.count) fixed bin;
				/* for each mediachar    */
%page;
/*	 view*						       */
/*          _______				Views are then made up     */
/*         |count=k|__			from the defined media.    */
/*      (1)|view1|med4|			Views can share a media,   */
/*      (2)|view2|med2|			but will differ media      */
/*         .     .    .			select string. Each Device */
/*         :     :    :			specifies its own set of   */
/*      (k)|viewk|med4|			media select strings.
/*         |_____|____|					       */

    dcl view_p	   ptr;
    dcl 1 view	   based (view_p),
	2 count	   fixed bin,	/* how many defined		       */
	2 e	   (view.count),
	  3 name	   char (32),	/* viewname		       */
	  3 media	   fixed bin;	/* media being referenced	       */


/*	Def*						       */
/*	 _______		Def's are a sort of macro definition.	       */
/*	|count=d|_	Whenever a set of Multics chars have the same  */
/*     (1)|name1|pt1|	definition in several fonts, instead of	       */
/*     (2)|name2|pt2|	entering the description again and again, a    */
/*	.     .   .	Def is made containing the needed info and     */
/*	:     :   :	then they are ref'ed in each table as needed.  */
/*     (d)|named|ptd|					       */
/*	|_____|___|					       */

    dcl Def_p	   ptr;
    dcl 1 Def	   based (Def_p),
	2 count	   fixed bin,	/* how many Def's present	       */
	2 e	   (Def.count),
	  3 name	   char (32),	/* internal name of this Def	       */
	  3 pt	   ptr;		/* Points to the node in the	       */
				/*  lex_string_ list at which source */
				/*  of the Def begins.  At ref time, */
				/*  this source will be be re-parsed */
				/*  via this pointer.	       */
%page;
/* fntl_p(1)>----+						       */
/* fntl_p(2)>---)|(---------------------+			       */
/*    +----------+			|			       */
/*    |	fnt*		fnt*	|	fnt*		       */
/*    |	 _____		 _____	|	 _____		       */
/*    +-->|next >------------>|next >---+-------->|next >null	       */
/*	|name |		|name |		|name |		       */
/*	|refno|		|refno|		|refno|		       */
/*	|node >...	|node >...	|node >...	       */
/*	|pt   >---+	|pt   >...	|pt   >...	       */
/*	|_____|  	|	|_____|		|_____|		       */
/*    +-------------+					       */
/*    |    font			Fonts are made up by selecting one   */
/*    |    _________		or more mediachars from a view and   */
/*    +-->|units_r  >-----+		associating them to Multics (input)  */
/*	|oput_r   >--+  |		characters. To speed up measuring,   */
/*	|rel_units|  |  |		the width portion of the font table  */
/*	|footsep  |  |  |		is a fixed size.		       */
/*	|min_spb  |  |  |		  To save space, however, the output */
/*	|avg_spb  |  |  |		string portion of the font is only   */
/*	|max_spb  |  |  |		as long as the highest Multics char  */
/*	|_________|  |  |		defined.			       */
/*    +----------------+  |					       */
/*    |	 opu*	      |	 uni*		   The oput and units      */
/*    |    _____	      |	 _____         units   tables often end up     */
/*    +-->|next >...      +-->|next >...     _____   looking like others of  */
/*	|ref_p>---+    	|ref_p>------>|(0)  |  their kind. Thus when   */
/*	|seqno|	|	|seqno|	    |(1)  |  each is completed, it   */
/*	|refno|   |	|refno|	    .     .  is matched against all  */
/*	|_____|	|	|_____|	    :     :  prior ones & logically  */
/*    +-------------+    	       	    |(511)|  removed if already      */
/*    |  	oput	    	       	    |_____|  there, reducing DSM     */
/*    |	 ____________			   size.		       */
/*    +-->|data_count=k|		       			       */
/*     (0)|which|what_r>...					       */
/*     (1)|which|what_r>...			From compdv's point of     */
/*  	.     .      .	    medchar_sel 	view, medchar_sel is a     */
/*  	:     :      :	   ________..._	bstr.		       */
/*     (k)|which|what_r>------->|len|text... |			       */
/*        |_____|______|	  |___|________|			       */
/*		    					       */
/*		    		oput.which references an entry in    */
/*		    		the Device's med_sel_table.	       */
%page;
    dcl font_count	   fixed bin;	/* # font entries present	       */
    dcl fntl_p	   (2) ptr;	/* begin/end fnt list	       */
    dcl fnt_p	   ptr;
    dcl 1 fnt	   based (fnt_p),	/* === font info entry	       */
	2 next	   ptr,		/* next entry		       */
	2 name	   char (32),	/* internal reference only	       */
	2 refno	   fixed bin,	/* internal reference #	       */
	2 node	   ptr,		/* rdc node for Font: statement      */
				/*  used for error messages	       */
	2 pt	   ptr,		/* points to the font table	       */
	2 dummy	   ptr;		/* where next structure goes	       */

    dcl uni_ct	   fixed bin init (0);
    dcl unil_p	   (2) ptr;
    dcl uni_p	   ptr;
    dcl 1 uni	   based (uni_p),	/* === units entry		       */
	2 next	   ptr,		/* next entry		       */
	2 ref_p	   ptr,		/* points to units table	       */
	2 seqno	   fixed bin,	/* internal sequence #	       */
	2 refno	   fixed bin;	/* internal reference #	       */
				/* when seqno=refno this is a "real" */
				/* entry, otherwise it's a duplicate */

    dcl opul_p	   (2) ptr;
    dcl opu_p	   ptr;
    dcl 1 opu	   based (opu_p),	/* === oputs entry		       */
	2 next	   ptr,		/* next entry		       */
	2 ref_p	   ptr,		/* points to oput table	       */
	2 seqno	   fixed bin,	/* internal sequence #	       */
	2 refno	   fixed bin;	/* internal reference #	       */
				/* when seqno=refno this is a "real" */
				/* entry, otherwise it's a duplicate */

%page;
/* 		           dvid*				       */
/*		           _______				       */
/*  dvidl_p(1)>-------------->|next   >------+     dvid*		       */
/*  dvidl_p(2)>----------+	|ndx    |	     |     _______		       */
/*		     |	|real   |	     +--->|next   >null	       */
/*		     |    |refname|	     |	|ndx    |		       */
/*		     |	|devname|	     |	|real   |		       */
/*		     |	|dvt_ndx|	     |	|refname|		       */
/*		     |	|_______|	     |	|devname|		       */
/*		     |		     |	|dvt_ndx|		       */
/*		     +-------------------+	|_______|		       */
    dcl comp_dvid_new  bit (1);	/* a new comp_dvid is being started  */
    dcl comp_dvid_ct   fixed bin init (0);
				/* how many actual comp_dvid defined */
    dcl dvid_ct	   fixed bin;	/* # dvid entries present	       */
    dcl dvidl_p	   (2) ptr;	/* begin/end of dvid list	       */
    dcl dvid_p	   ptr;
    dcl 1 dvid	   based (dvid_p),	/* === comp_dvid data	       */
	2 next	   ptr,		/* link to next entry	       */
	2 ndx	   fixed bin,	/* which dvid being referenced       */
	2 real	   bit (1) aligned, /* 1- defines a comp_dvid	       */
	2 refname	   char (32),	/* external reference name	       */
	2 devname	   char (32),	/* comp_dvid.devname	       */
	2 dvt_ndx	   fixed bin,	/* comp_dvid.dvt_r derived from this */
	2 dummy	   ptr;		/* place where next structure goes   */

/* This structure contains all the info necessary to generate comp_dvid.     */
%page;
/*					     	 dvt*	       */
/*  dvtl_p(1) >------+			        	 _______	       */
/*  dvtl_p(2) >-----)|(-------------------------------+---->|next   >null    */
/*		 |        dvt*		    |	|ndx    |	       */
/*		 |        _______		    |	|prent  >-...    */
/*		 +------>|next   >----------------+	|med_sel>--...   */
/*		         |ndx    |		          |ref    >-...    */
/*		         |prent  >--------+	          |_______|	       */
/*		         |med_sel>-----+  |			       */
/*		         |ref    >--+  |  |	    	 prent*	       */
/*		         |_______|	|  |  |		 __________      */
/*	    +-------------------------+  |  +------------>|outproc   |     */
/*	    |			   |  		|artproc   |     */
/*	    |      comp_dvt	    	   |   med_sel	|footproc  |     */
/*	    |      _________    	   |    _________	|__________|     */
/*	    +---->| details	|   	   +-->| details |		       */
/*		| below   |	       |  below  |		       */
/*		|_________|   	       |_________|		       */

    dcl dvt_ct	   fixed bin;	/* # dvt entries present	       */
    dcl dvtl_p	   (2) ptr;	/* begin/end of dvt list	       */

    dcl dvt_p	   ptr;
    dcl 1 dvt	   based (dvt_p),	/* === comp_dvt reference info       */
	2 next	   ptr,		/* link to next entry	       */
	2 ndx	   fixed bin,	/* which index this represents       */
	2 prent	   ptr,		/* ptr to prent data	       */
	2 med_sel	   ptr,		/* ptr to associated med_sel array   */
	2 ref	   ptr,		/* ptr to comp_dvt		       */
	2 dummy	   ptr;		/* place where next structure goes   */

    dcl prent_p	   ptr;
    dcl 1 prent	   based (prent_p), /* === entryname strings, comp_dvt   */
	2 outproc	   char (68) var,
	2 artproc	   char (68) var,
	2 footproc   char (68) var,
	2 dummy	   ptr;		/* place where next structure goes   */

    dcl 1 med_sel_tab  aligned based (dvt.med_sel),
	2 count	   fixed bin,
	2 ref_r	   (med_sel_tab.count) bit (18) aligned;
%page;
/*	              mem*					       */
/*	         	    ______				       */
/*  meml_p(1) >----	+->|next  >--+				       */
/*  meml_p(2) >--+ 	   |ref_p >  |				       */
/* 	       |     |seqno |  |				       */
/*	       | 	   |______|  |				       */
/*	       +---------------+				       */
/*	       |     mem*					       */
/*	       |	   ______	          member			       */
/*	       +--->|next  >null      _________			       */
/*		  |ref_p >-------->| details |		       */
/*		  |seqno |         |  below  |		       */
/*		  |______|         |_________|		       */

    dcl meml_p	   (2) ptr;	/* begin/end member list	       */
    dcl mem_ct	   fixed bin init (0);
				/* internal sequence counter	       */
    dcl mem_p	   ptr;
    dcl 1 mem	   based (mem_p),	/* === member table (code gen only)  */
	2 next	   ptr,		/* next entry		       */
	2 ref_p	   ptr,		/* pointer to the member table       */
	2 seqno	   fixed bin,	/* internal sequence #	       */
	2 refno	   fixed bin,	/* internal reference #	       */
				/* when seqno=refno this is a "real" */
				/* entry, otherwise it's a duplicate */
	2 dummy	   ptr;		/* where next structure goes	       */

/*		 EXTERNAL INTERCONNECTION in the DSM		       */
/*  linkage						       */
/*  section				             comp_dvid     */
/*  ______     +-----------------------------------------+     _______       */
/* |      |    |				       +--->|       |      */
/* |name1 >----+				            |devname|      */
/* |name2 >---)|(-------------------+    comp_dvid            |dvt_r  >--+   */
/* |name3 >----+   comp_dvid	      |    _______	            |_______|  |   */
/* |name4 >--+     _______	      +-->|       |	    comp_dvt	   |   */
/* | etc. |  +--->|       |	          |devname|	    ________	   |   */
/* |______|       |devname|	          |dvt_r  >--->| ...		   |   */
/*                |dvt_r  >--+          |_______|    |		   |   */
/*                |_______|  |				   |   */
/*  +-------<----------------+-----------------------------------<-------+   */
/*  |    comp_dvt						       */
/*  |    _____________			             bstr	       */
/*  +-->|             |			             ___________   */
/*      |atd_r        >-------------------------------------->|len|str... |  */
/*      |dvc_r        >...			    	  |___|_______|  */
/*      |med_sel_tab_r>-----------------+    med_sel_tab		       */
/*      | ...         |		|    _______       	   med_sel       */
/*      |family_ct=F  |		+-->|count=K|      	   ___________   */
/*   (1)|.member_r    >--+		 (1)|ref_r>---------->|len|str... |  */
/*   (1)|.name        |  |		    .     .           |___|_______|  */
/*      | ...         |  |		 (n)|ref_r>nullo		       */
/*      |_____________|  |		    .     .		       */
/*       +---------------+		 (K)|ref_r>...	 sizel	       */
/*       |    member		    |_____| 	 ________	       */
/*       |    _______	 +--------------------------->|val_ct=S|       */
/*       +-->|count=L|_________|________________	       (1)|val     |       */
/*	(1)|font_r> size_r>  | lex|Scaley|name|		.        .       */
/*	(2)|font_r> size_r>--+ lex|Scaley|name|		:        :       */
/*	   .      .       .       .      .    .	       (S)|val     |       */
/*	   :      :       :       :      :    :		|________|       */
/*	(L)|font_r>---+   > Scalex|Scaley|name|			       */
/*	   |______|___|___|_______|______|____|			       */
/*	              |					       */
/*    +-----------------+					       */
/*    |    font					units	       */
/*    |    _________				_____	       */
/*    +-->|units_r  >-------------------------------------->|(0)  |	       */
/*	|oput_r   >---+ 				|(1)  |	       */
/*	|rel_units|   |		  		.     .	       */
/*	|footsep  |   |  	oput	    	       	:     :	       */
/*	|min_spb  |   |    _________		       	|(511)|	       */
/*	|avg_spb  |   +-->|data_ct=k|__		|_____|	       */
/*	|max_spb  |    (0)|which|what_r>...			       */
/*	|_________|    (1)|which|what_r>...			       */
/*		        .     .      .	    medchar_sel	       */
/*		        :     :      :	   ________..._	       */
/*		     (k)|which|what_r>--------->|len|text... |	       */
/*		        |_____|______|	  |___|________|	       */
/*							       */
/*		    		oput.which references an entry in    */
/*		    		the Device's med_sel_table.	       */
%page;
%include comp_art_parts;
%include comp_metacodes;
%include comp_dvid;
%include comp_dvt;
%include comp_fntstk;
%include comp_font;
%include compstat;
%include compdv_msgs;

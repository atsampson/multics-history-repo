&  **************************************************************
&  *                                                            *
&  * Copyright, (C) Massachusetts Institute of Technology, 1982 *
&  *                                                            *
&  **************************************************************
&  This ec creates the saved environment for the lisp assember program (lap).
&  It takes one argument, which is the pathname of the directory which contains
&  lap_, which it loads.  This argument is optional.  The saved environment is
&  lap.sv.lisp, created in the working directory.
&
&command_line off
discard_output -osw error_output "tmr lap_"
&input_line off
&attach
lisp lisp_nostartup
(progn
&if [exists argument &r1]
&then 	(load "&q1>lap_")
&else	(load "lap_")
	(*rset nil)
	(sstatus uuolinks t)
	(gctwa) (gc)
	(save lap))
&detach
&quit

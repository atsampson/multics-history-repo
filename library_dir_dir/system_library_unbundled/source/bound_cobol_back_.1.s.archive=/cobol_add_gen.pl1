/****^  ***********************************************************
        *                                                         *
        * Copyright, (C) BULL HN Information Systems Inc., 1989   *
        *                                                         *
        * Copyright, (C) Honeywell Information Systems Inc., 1982 *
        *                                                         *
        * Copyright (c) 1972 by Massachusetts Institute of        *
        * Technology and Honeywell Information Systems, Inc.      *
        *                                                         *
        *********************************************************** */




/****^  HISTORY COMMENTS:
  1) change(89-04-23,Zimmerman), approve(89-04-23,MCR8060),
     audit(89-05-05,RWaters), install(89-05-24,MR12.3-1048):
     MCR8060 cobol_add_gen.pl1 Added Trace statements.
                                                   END HISTORY COMMENTS */


/* Modified on 10/19/84 by FCH, [4.3-1], BUG563, new cobol_addr_tokens.incl.pl1
/* Modified on 08/31/83 by FCH, [5.2...], trace added */
/* Modified on 04/18/80 by FCH, new include file cobol_arith_util, fix not option */
/* Modified on 06/28/79 by FCH, [4.0-1], not option added for debug */
/* Modified since Version 4.0 */

/* format: style3 */
cobol_add_gen:
     proc (in_token_ptr, next_stmt_tag);
	/***.....	if Trace_Bit then call cobol_gen_driver_$Tr_Beg(MY_NAME);/**/
						/*
The ADD statement generator: cobol_add_gen

FUNCTION

The function of this procedure is to generate code for the
Cobol ADD statement.

*/

/*  DECLARATION OF THE PARAMETERS  */

/* dcl in_token_ptr ptr;  */
/*  DECLARED BELOW IN AN INCLUDE FILE  */
dcl	next_stmt_tag	fixed bin;

/*  DECLARATION OF EXTERNAL ENTRIES  */

dcl	cobol_num_to_udts	ext entry (ptr, ptr);
dcl	cobol_fofl_mask$on	ext entry;
dcl	cobol_fofl_mask$off ext entry;
dcl	cobol_build_resop	ext entry (ptr, ptr, fixed bin, ptr, bit (1), fixed bin, bit (1));
dcl	cobol_add3	ext entry (ptr, ptr, ptr, fixed bin);
dcl	cobol_add		ext entry (ptr, ptr, fixed bin);
dcl	cobol_define_tag	ext entry (fixed bin);
dcl	cobol_alloc$stack	ext entry (fixed bin, fixed bin, fixed bin);
dcl	cobol_addr	ext entry (ptr, ptr, ptr);
dcl	cobol_emit	ext entry (ptr, ptr, fixed bin);
dcl	cobol_arith_move_gen
			ext entry (ptr);
dcl	cobol_move_gen	ext entry (ptr);
dcl	cobol_make_type9$copy
			ext entry (ptr, ptr);
dcl	cobol_make_tagref	ext entry (fixed bin, fixed bin, ptr);
dcl	cobol_register$load ext entry (ptr);
dcl	cobol_make_type9$fixed_bin_35
			ext entry (ptr, fixed bin, fixed bin);
dcl	cobol_make_type9$type2_3
			ext entry (ptr, ptr);
dcl	cobol_binary_check$add
			ext entry (ptr, bit (1), fixed bin, fixed bin);
dcl	cobol_add_binary_gen
			ext entry (ptr, fixed bin, fixed bin, fixed bin, fixed bin);



/*  DECLARATIONS OF BUILTIN FUNCTIONS  */

dcl	addr		builtin;
dcl	fixed		builtin;
dcl	null		builtin;

/*  DECLARATION OF INTERNAL STATIC VARIABLES  */

dcl	first_meaningful_ptr_index
			fixed bin int static init (2);

dcl	add_code		fixed bin int static init (182);

/*  Definition of an EOS token to be used in calls to the move generator  */

dcl	1 move_eos	int static,
	  2 size		fixed bin (15) init (38),
	  2 line		fixed bin (15) init (0),
	  2 column	fixed bin (15) init (0),
	  2 txpe		fixed bin (15) init (19),	/*  EOS  */
	  2 verb		fixed bin (15) init (18),	/*  MOVE  */
	  2 e		fixed bin (15) init (0),
	  2 h		fixed bin (15) init (0),
	  2 i		fixed bin (15) init (0),
	  2 j		fixed bin (15) init (0),
	  2 a		bit (16) init ("0"b);

/*  Definition of a numeric literal zero  */

dcl	1 num_lit_zero	int static,
	  2 size		fixed bin (15) init (37),
	  2 line		fixed bin (15) init (0),
	  2 column	fixed bin (15) init (0),
	  2 type		fixed bin (15) init (2),
	  2 integral	bit (1) init ("1"b),
	  2 floating	bit (1) init ("0"b),
	  2 filler1	bit (5) init ("0"b),
	  2 subscript	bit (1) init ("0"b),
	  2 sign		char (1) init (" "),
	  2 exp_sign	char (1) init (" "),
	  2 exp_places	fixed bin (15) init (0),
	  2 places_left	fixed bin (15) init (1),
	  2 places_right	fixed bin (15) init (0),
	  2 places	fixed bin (15) init (1),
	  2 lit_val	char (1) init ("0");


/*  Declarations of initialized variables that define verb type codes in EOS token  */

dcl	add_vt		fixed bin (15) int static init (2);
dcl	subtract_vt	fixed bin (15) int static init (11);



/*  DECLARATION OF INTERNAL AUTOMATIC VARIABLES  */

dcl	ose_flag		bit (1);
dcl	addend_count	fixed bin;
dcl	receive_count	fixed bin;

dcl	fmt1		bit (1);

dcl	lop_ptr		ptr;
dcl	rop_ptr		ptr;
dcl	resultant_operand_ptr
			ptr;
dcl	minuend_token_ptr	ptr;
dcl	subtrahend_token_ptr
			ptr;


dcl	ix		fixed bin;
dcl	iy		fixed bin;
dcl	move_eos_ptr	ptr;
dcl	mv_ptr		ptr;

dcl	rdmax_value	fixed bin;
dcl	overflow_code_generated
			bit (1);
dcl	possible_ovfl_flag	bit (1);
dcl	receiving_is_not_stored
			bit (1);
dcl	overflow_possible	bit (1);
dcl	size_error_flag_defined
			bit (1);

dcl	temp_in_token	(1:10) ptr;
dcl	size_error_inst	bit (36);
dcl	size_error_inst_ptr ptr;
dcl	size_error_token_ptr
			ptr;
dcl	stored_token_ptr	ptr;
dcl	no_overflow_tag	fixed bin;
dcl	add_gen_code	fixed bin;
dcl	verb_type		fixed bin;
dcl	temp_ptr		ptr;
dcl	op1_token_ptr	ptr;
dcl	op2_token_ptr	ptr;
dcl	temp_resultant_operand_ptr
			ptr;
dcl	(binary_ok, not_bit)
			bit (1);
dcl	source_code	fixed bin;
dcl	target_code	fixed bin;

dcl	dn_ptr		ptr;


	/***.....	dcl cobol_gen_driver_$Tr_Beg entry(char(*));/**/
	/***.....	dcl cobol_gen_driver_$Tr_End entry(char(*));/**/

	/***.....	dcl Trace_Bit bit(1) static external;/**/
	/***.....	dcl Trace_Lev fixed bin static external;/**/
	/***.....	dcl Trace_Line char(36) static external;/**/
	/***.....	dcl ioa_ entry options(variable); /**/
	/***..... dcl MY_NAME char (13) int static init ("COBOL_ADD_GEN"); /**/


/**************************************************/
/*	START OF EXECUTION			*/
/*	cobol_add_gen			*/
/**************************************************/

/*  Get meaningful data from the EOS token.  */
	eos_ptr = in_token.token_ptr (in_token.n);	/*  Check to see if binary arithemtic and be done for this add/subtract statement  */
	call cobol_binary_check$add (in_token_ptr, binary_ok, target_code, source_code);

	if binary_ok
	then do;					/*  Binary arithmetic can be done.  */
		if end_stmt.verb = add_vt
		then add_gen_code = 1;
		else add_gen_code = 2;
		call cobol_add_binary_gen (in_token_ptr, next_stmt_tag, target_code, source_code, add_gen_code);
		return;
	     end;					/*  Binary arithmetic can be done.  */


/*  ON SIZE ERROR flag  */
	ose_flag = end_stmt.b;

/*  Number of operands to be added  */
	addend_count = end_stmt.e;

/*  Number of receiving operands  */
	receive_count = end_stmt.h;

/*  Verb type  */
	verb_type = end_stmt.verb;

/*  Determine the ADD or SUBTRACT statement format  */
	if end_stmt.a = "000"b
	then fmt1 = "1"b;				/*  Format 1 ADD  */
	else fmt1 = "0"b;				/*  Format 2 ADD  */


	if ose_flag
	then do;					/*  Reserve a tag to be associated with the next Cobol statement  */
		next_stmt_tag = cobol_$next_tag;
		cobol_$next_tag = cobol_$next_tag + 1;

	     end;					/*  Reserve a tag to be associated with the next Cobol statement  */

	resultant_operand_ptr = in_token.token_ptr (first_meaningful_ptr_index);

	iy = first_meaningful_ptr_index;

	if addend_count > 1
	then do;					/*  Generate code to add all of the operands together.  */


		do ix = 1 to addend_count - 1;	/*  Generate the add code.  */
		     iy = iy + 1;			/*  subscript of next addend pointer  */
		     lop_ptr = resultant_operand_ptr;
		     rop_ptr = in_token.token_ptr (iy);

/*  Build resultant operand to hold the result of the addition  */
		     call cobol_build_resop (lop_ptr, rop_ptr, add_code, resultant_operand_ptr, "0"b, rdmax_value,
			possible_ovfl_flag);

/*  Generate code to add the two operands  */
		     call cobol_add3 (lop_ptr, rop_ptr, resultant_operand_ptr, 1 /*ADD*/);

		end;				/*  Generate the add code.  */

	     end;					/*  Generate code to add all of the operands togenter.  */

	if resultant_operand_ptr -> data_name.type ^= rtc_dataname
	then do;					/*  A literal or fig constant ZERO is to be added in fmt 1 add  */

		if resultant_operand_ptr -> data_name.type = rtc_resword
		then temp_ptr = addr (num_lit_zero);	/*  Figurative constant ZERO
					is to be added.  */
		else temp_ptr = resultant_operand_ptr;	/*  A numeric literal is to be added.  */

/*  Pool the literal and make a type 9 token  */
		resultant_operand_ptr = null ();	/*  utility provides buffer for
					data name token  */
		call cobol_make_type9$type2_3 (resultant_operand_ptr, temp_ptr);
	     end;					/*  A literal or fig constant ZERO is to be added in fmt 1 add  */

/*
	At this point in processing, the following coonditions exist:
		1. Code has been generated to add together all operands to
		the left of "TO" (for format 1 ADD) or to he left of
		"GIVING" ( for format 2 ADD).
		2. The data name token that describes the sum of these
		operands is pointed at by the pointer resultant_operand_ptr.
		3. The variable "iy" contains the subscript of the in_token array element
		that points to the last addend.  ( i.e., iy + 1 is the subscript of the
		pointer to the first receiving token.)

	*/



/*  Now check to see if code is being generated for a format 2 subtract.  */

	if (verb_type = subtract_vt & ^fmt1)
	then do;					/*  Format 2 SUBTRACT, must generate code to subtract the sum calculated so far,
		from the minuend.  */

/*  Increment iy to become the subscript of the pointer to the minuend token.  */
		iy = iy + 1;

		subtrahend_token_ptr = resultant_operand_ptr;
		minuend_token_ptr = in_token.token_ptr (iy);
		call cobol_build_resop (minuend_token_ptr, subtrahend_token_ptr, add_code, resultant_operand_ptr,
		     "0"b, rdmax_value, possible_ovfl_flag);

/*  At this point in processing:

			1. minuend_token_ptr points to a token for the minuend.
			2. subtrahend_token_ptr points to a token for the result of adding all operands
			to the left of "TO".
			3. resultant_operand_ptr points to a token to receive the difference of the
			subtraction.
		*/

		call cobol_add3 (subtrahend_token_ptr, minuend_token_ptr, resultant_operand_ptr, 2 /*SUBTRACT*/);


	     end;					/*  Format 2 SUBTRACT, must generate code to stubract the sum calculated so far,
			from the minuend.  */

/*  Now we will get the result into the receiving operands.  */

	overflow_code_generated = "0"b;
	size_error_flag_defined = "0"b;

	do ix = 1 to receive_count;			/*  Generate code to get the sum into receiving operands.  */
	     mv_ptr = null ();
	     overflow_possible = "0"b;
	     receiving_is_not_stored = "0"b;
	     iy = iy + 1;				/*  Get subscript of pointer to "next" receiving operand token.  */
	     if ose_flag
	     then do;				/*  ON SIZE CHECKING required,  */
		     if fmt1
		     then overflow_possible = "1"b;	/*  Overflow always possible for format 1
			add or subtract.  */

		     else if (resultant_operand_ptr -> data_name.places_left
			> in_token.token_ptr (iy) -> data_name.places_left)
		     then overflow_possible = "1"b;	/*  Format 2, result left digits >
				receiving left digits.  */
		     else if resultant_operand_ptr -> data_name.sign_type = "111"b
		     then overflow_possible = "1"b;	/*  Resultant operand is floating decimal.  */
		end;				/*  ON SIZE checking required  */

	     if overflow_possible
	     then do;				/*  Store the receiving field into a temporary  */
		     overflow_code_generated = "1"b;
		     if ^size_error_flag_defined
		     then do;			/*  Define the size error fixed bin flag in the run-time stack.  */

			     size_error_inst_ptr = addr (size_error_inst);
			     call get_size_error_flag (size_error_token_ptr, size_error_inst_ptr);
			     size_error_flag_defined = "1"b;
			end;			/*  Define the size error fixed bin flag in the run-time stack.  */


/*  Store the receiving field into a temporary.  */
/*  Note that if the receiving field is numeric edited, or overpunch sign, then
			it is not stored into a temporary.  */


		     if in_token.token_ptr (iy) -> data_name.numeric_edited /*  Receiving is numeric edited.  */
			| (in_token.token_ptr (iy) -> data_name.display
			& in_token.token_ptr (iy) -> data_name.item_signed
			& in_token.token_ptr (iy) -> data_name.sign_separate = "0"b)
						/*  overpunch sign  */
		     then receiving_is_not_stored = "1"b;
		     else call receiving_field (in_token.token_ptr (iy), stored_token_ptr, 1);

/*  Reserve a tag to which to transfer if no overflow occurs.  */
		     no_overflow_tag = cobol_$next_tag;
		     cobol_$next_tag = cobol_$next_tag + 1;

/*  Generate code to turn the overflow mask indicator bit ON  */
		     call cobol_fofl_mask$on;

		end;				/*  Store the receiving field into a temporary  */


	     if fmt1
	     then do;				/*  Add sum to or SUBTRACT sum from the receiving field.  The
			result goes into the receiving field.  */

		     if verb_type = add_vt
		     then add_gen_code = 1;		/*  ADD  */
		     else add_gen_code = 2;		/*  SUBTRACT  */




		     if not_dec_operand (in_token.token_ptr (iy))
		     then do;			/*  The receiving operand is not decimal.  Must convert to decimal
			before performing the add or subtract.  */

			     op1_token_ptr = resultant_operand_ptr;
			     op2_token_ptr = in_token.token_ptr (iy);

/*  Convert the non-decimal operand(s) , and build a temporary
				into which to store the result of the computation.  */

			     call cobol_build_resop (op1_token_ptr, op2_token_ptr, add_code,
				temp_resultant_operand_ptr, "0"b, rdmax_value, possible_ovfl_flag);

/*  Generate code to add (or subtract) the two operands, and
				store the result into a temporary.  */
			     call cobol_add3 (op1_token_ptr, op2_token_ptr, temp_resultant_operand_ptr,
				add_gen_code);

/*  Generate code to move the result of the add/subtract to
				the receiving field.  */

			     move_eos_ptr = addr (move_eos);
			     move_eos_ptr -> end_stmt.e = 1;
			     mv_ptr = addr (temp_in_token (1));
			     mv_ptr -> in_token.n = 4;
			     mv_ptr -> in_token.token_ptr (1) = null ();
			     mv_ptr -> in_token.token_ptr (2) = temp_resultant_operand_ptr;
			     mv_ptr -> in_token.token_ptr (3) = in_token.token_ptr (iy);
			     mv_ptr -> in_token.token_ptr (4) = move_eos_ptr;

			     call cobol_arith_move_gen (mv_ptr);
			     if mv_ptr -> in_token.code ^= 0
			     then receiving_is_not_stored = "1"b;

			end;			/*  The receiving operand is not decimal.  Must convert
				to decimal before performing the add or subtract.  */

		     else do;			/*  Receiving operand is decimal.  */

			     if not_dec_operand (resultant_operand_ptr)
			     then do;		/*  Left operand is not decimal--convert to decimal.  */
				     op1_token_ptr = resultant_operand_ptr;
				     resultant_operand_ptr = null ();
				     call cobol_num_to_udts (op1_token_ptr, resultant_operand_ptr);


				end;		/*  Left operand is not decimal--convert to decimal.  */

			     call cobol_add (resultant_operand_ptr, in_token.token_ptr (iy), add_gen_code);

			end;			/*  Receiving operand is decimal.  */


		end;				/*  Add sum or SUBTRACT sum from the receiving field.  The
			result goes into the receiving field.  */

	     else do;				/*  Generate code to MOVE the sum to the receiving field  */
						/*  Set up an in_token structure for a move.  */

		     move_eos_ptr = addr (move_eos);
		     move_eos_ptr -> end_stmt.e = 1;	/*  Number of receiving operands.  */

		     mv_ptr = addr (temp_in_token (1));
		     mv_ptr -> in_token.n = 4;
		     mv_ptr -> in_token.token_ptr (1) = null ();
		     mv_ptr -> in_token.token_ptr (first_meaningful_ptr_index) = resultant_operand_ptr;
		     mv_ptr -> in_token.token_ptr (3) = in_token.token_ptr (iy);
						/*  Receiving field  */
		     mv_ptr -> in_token.token_ptr (4) = move_eos_ptr;

/*  Generate the move code  */
		     if (ose_flag & overflow_possible = "0"b)
		     then call cobol_move_gen (mv_ptr); /*  OSE present, but result will fit
				into the receiving filed with no possibility of overflow.  */
		     else call cobol_arith_move_gen (mv_ptr);

		     if mv_ptr -> in_token.code ^= 0
		     then receiving_is_not_stored = "1"b;
		end;				/*  Generate code to MOVE the sum to the receiving field.  */


	     if overflow_possible
	     then do;				/*  Generate code to test for overflow, and restore
			the original value to the receiving field if overflow occurred.  */

		     call test_for_overflow (no_overflow_tag, size_error_inst_ptr, mv_ptr);

/*  If the receiving field has been stored into a temporary, then resotre it.  */
		     if ^receiving_is_not_stored
		     then call receiving_field (in_token.token_ptr (iy), stored_token_ptr, 2);

/*  DEfine the no overflow tag at the instruction following the restore value code.  */
		     call cobol_define_tag (no_overflow_tag);

/*  Generate code to turn the overflow mask indicator bit OFF  */
		     call cobol_fofl_mask$off;

		end;				/*  Generate code to tst for overflow, and restore
			the original value to the receiving filed if overflow occurred.  */
	     else if receiving_is_not_stored		/*  Receiving field is numeric edited, and the
			result has been stored into a temporary to see if overflow will occur.  Now
			we must move the temporary into the numeric edited field.  */
	     then call cobol_move_gen (mv_ptr);

	end;					/*  Generate code to get the sum into receiving operands.  */

/*  At this point in processing, code has been generated to
		1. get the result into the receiving operands.
		2. test for possible overflow.
	*/

	if ose_flag
	then do;					/*  Generate code to test the size error flag, and transfer over the imperative stmt
		if no size error occurred.  */


/*[4.0-1]*/
		if end_stmt.f = "01"b
		then not_bit = "1"b;
		else not_bit = "0"b;
		call test_size_error (size_error_token_ptr, size_error_inst_ptr, next_stmt_tag,
		     overflow_code_generated, not_bit);
	     end;					/*  Generate code to test the size error flag, and transfer over the imperative stmt
		if no size error occurred.  */

	/***..... if Trace_Bit then call cobol_gen_driver_$Tr_End(MY_NAME);/**/
	return;



/**************************************************/
/*	END OF EXECUTABLE STATEMENTS		*/
/*	cobol_add_gen			*/
/**************************************************/

%include cobol_arith_util;

/*  INCLUDE FILES USED BY THIS PROCEDURE  */

%include cobol_type9;

%include cobol_in_token;

%include cobol_type19;

%include cobol_;

%include cobol_addr_tokens;



%include cobol_record_types;

/**************************************************/
/*	END OF EXTERNAL PROCEDURE		*/
/*	cobol_add_gen			*/
/**************************************************/

     end cobol_add_gen;

/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose subroutine to locate and access input files */

/* format: style2,ind2,ll79,dclind4,idind15,comcol41,linecom */

comp_get_file_:
  proc;
    return;			/* no entry here */

/* LOCAL STORAGE */

    dcl ctl_info_ptr   ptr;		/* for comp_report_$ctlstr */
    dcl (i, j, k)	   fixed bin;	/* working index */
    dcl 1 insert_status
		   aligned like status_branch;
				/* local insert block index */
    dcl insertndx	   fixed bin init (0);
    dcl itsptr	   ptr;		/* local ITS pointer */
    dcl pname_ptr	   ptr;		/* primary entryname */
    dcl pname	   char (32) aligned based (pname_ptr);
    dcl status_area	   area (4096);	/* work area for status */

    dcl (addr, before, bool, divide, hbound, index, null, pointer, rtrim, size,
        search)	   builtin;

    dcl comp_error_table_$limitation
		   fixed bin (35) ext static;
    dcl error_table_$no_r_permission
		   fixed bin (35) ext static;
    dcl error_table_$segknown
		   fixed bin (35) ext static;
    dcl error_table_$zero_length_seg
		   fixed bin (35) ext static;

    dcl com_err_	   entry options (variable);
    dcl expand_pathname_$add_suffix
		   entry (char (*), char (*), char (*) aligned,
		   char (*) aligned, fixed bin (35));
    dcl hcs_$status_long
		   entry (char (*) aligned, char (*) aligned,
		   fixed bin (1), ptr, ptr, fixed bin (35));
    dcl msf_manager_$get_ptr
		   entry (ptr, fixed bin, bit (1), ptr, fixed bin (24),
		   fixed bin (35));
    dcl msf_manager_$open
		   entry (char (*) aligned, char (*) aligned, ptr,
		   fixed bin (35));
    dcl pathname_	   entry (char (*) aligned, char (*) aligned)
		   returns (char (168));
    dcl search_paths_$find_dir
		   entry (char (*), ptr, char (*) aligned, char (*),
		   char (*) aligned, fixed bin (35));
    dcl suffixed_name_$make
		   entry (char (*) aligned, char (*), char (32) aligned,
		   fixed bin (35));

find:
  entry (given_name, fildataptr, refdir, source_file, suffix, ercd);

/* PARAMETERS */

    dcl given_name	   char (*);	/* given search name */
    dcl fildataptr	   ptr;		/* pointer to the file struc */
    dcl 1 fildata	   aligned like insert.file based (fildataptr);
    dcl refdir	   char (*);	/* dir for refdir rule */
    dcl source_file	   bit (1);	/* 1= source file, 0= insert file */
    dcl suffix	   char (*);	/* file suffix */
    dcl ercd	   fixed bin (35);	/* system error code */

    ctl_info_ptr = addr (ctl.info);
    ercd = 0;

    if (shared.bug_mode & dt_sw) | (source_file & sf_sw)
    then call ioa_ ("get_file_$find: (^[S^;I^] ^a)", source_file, given_name);
				/* force suffix, the dir may not */
    call				/* be the right one, tho */
         expand_pathname_$add_suffix (given_name, suffix, fildata.dir,
         fildata.entryname, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compose", "Expanding path for ^a", given_name);
        goto find_return;
      end;

    fildata.refname =
         substr (fildata.entryname, 1, length (rtrim (fildata.entryname)) - 7);

    if source_file			/* a command line file? */
    then fildata.insert_ptr = fildataptr;

    else				/* insert file */
      do;				/* search for given name */
        do i = 1 to insert_data.ref_area.count;
	refstr_ptr =		/* set search pointer & length */
	     addr (insert_data.ref_area.ptr (i) -> insert_refs.name (1));
	refstr_len = 32 * insert_data.ref_area.ptr (i) -> insert_refs.count;
				/* search name area */
	j = index (refname_str, fildata.entryname);

	if j > 0			/* did we find it? */
	then
	  do;			/* true name index */
	    j = divide (j, 32, 17, 0) + 1;
	    insertndx =		/* fetch insert index */
	         insert_data.ref_area.ptr (i) -> insert_refs.index (j);
	    goto name_found;
	  end;
        end;			/**/
				/* no luck, this is a new name */
        i = insert_data.ref_area.count; /* select last name area */
				/* fetch count */
        j = insert_data.ref_area.ptr (i) -> insert_refs.count;
				/* do we need a fresh name area? */
        if j = hbound (insert_data.ref_area.ptr (i) -> insert_refs.name, 1)
        then
	do;			/* last one full? */
	  if i = hbound (insert_data.ref_area.ptr, 1)
	  then
	    do;
	      call comp_report_$ctlstr (4, comp_error_table_$limitation,
		 ctl_info_ptr, ctl_line,
		 "Cant have more than ^d insert file names.",
		 60 * hbound (insert_data.ref_area.ptr, 1)
		 *
		 hbound (insert_data.ref_area.ptr (i) -> insert_refs.name,
		 1));
	      ercd = 1;
	      goto find_return;
	    end;

	  i, insert_data.ref_area.count = i + 1;
	  insert_data.ref_area.ptr (i) =
	       allocate (const.local_area_ptr, size (insert_refs));
	end;			/**/
				/* record new name */
        j = insert_data.ref_area.ptr (i) -> insert_refs.count + 1;
        insert_data.ref_area.ptr (i) -> insert_refs.name (j) =
	   fildata.entryname;
      end;			/**/
				/* if a search is needed */
    if search ("<>", given_name) = 0
    then
      do;
        if ^source_file		/* search only for insert files */
        then
	do;
	  call search_paths_$find_dir ("compose", null (), fildata.entryname,
	       refdir, fildata.dir, ercd);
	  if ercd ^= 0
	  then
	    do;
	      call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
		 "Searching for ^a", given_name);
	      goto find_return;
	    end;
	end;
      end;

/*    else				/* no search, expand it */
/*      do;
/*        call
/*	expand_pathname_$add_suffix (given_name, suffix, fildata.dir,
/*	fildata.entryname, ercd);
/*        if ercd ^= 0
/*        then
/*	do;
/*	  if source_file
/*	  then call
/*	         com_err_ (ercd, "compose", "Expanding path for ^a",
/*	         given_name);
/*	  else call
/*	         comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
/*	         "Expanding path for ^a", given_name);
/*	  goto find_return;
/*	end;
/*      end; */

    fildata.path = pathname_ (fildata.dir, fildata.entryname);

    if ^source_file			/* an insert file? */
    then
      do;
        insert_data.ref_area.ptr (i) -> insert_refs.count = j;
				/* find out everything we know */
        status_area_ptr = addr (status_area);
        call hcs_$status_long (fildata.dir, fildata.entryname, 0,
	   addr (insert_status), status_area_ptr, ercd);
        if ercd ^= 0
        then
	do;
sts_err:
	  call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
	       "Getting status of ^a", fildata.path);
	  goto find_return;
	end;

        if insert_status.type = Directory & insert_status.bit_count = 0
        then
	do;
dir_err:
	  call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
	       "Directories not allowed, ^a", fildata.path);
	  goto find_return;
	end;

        if insert_status.type = Link
        then
	do;			/* record link target path */
	  status_ptr = addr (insert_status);
	  fildata.path = status_pathname;
				/* now chase the link */
	  call hcs_$status_long (fildata.dir, fildata.entryname, 1,
	       addr (insert_status), null, ercd);
	  if ercd ^= 0
	  then goto sts_err;

	  if insert_status.type = Directory & insert_status.bit_count = 0
	  then goto dir_err;

	  call expand_pathname_$add_suffix ((fildata.path), suffix,
	       fildata.dir, "", ercd);
	  if ercd ^= 0
	  then
	    do;
	      call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
		 "Expanding path for ^a", given_name);
	      goto find_return;
	    end;
	end;

        do k = 1 to insert_data.count	/* do we know it by another name? */
	   while (insert_data.ptr (k) -> insert.path ^= fildata.path);
        end;

        if k <= insert_data.count	/* yes */
        then
	do;
	  fildata.charcount = insert_data.ptr (k) -> insert.charcount;
	  fildata.fcb_ptr = insert_data.ptr (k) -> insert.fcb_ptr;
	end;			/* need a new file data block */
				/* check limit */
        if insert_data.count = hbound (insert_data.ptr, 1)
        then
	do;
	  call comp_report_$ctlstr (4, comp_error_table_$limitation,
	       addr (ctl.info), ctl_line,
	       "Cant have more than ^d insert files.",
	       hbound (insert_data.ptr, 1));
	  goto find_return;
	end;

        insertndx, insert_data.count = insert_data.count + 1;
        insert_data.ptr (insertndx) =
	   allocate (const.global_area_ptr, size (insert));
				/* setup file data */
        shared.insert_ptr = insert_data.ptr (insertndx);
        insert.file = fildata;
        insert.insert_ptr = shared.insert_ptr;
        insert.charcount, insert.comp_no = 0;
        insert.fcb_ptr, insert.pointer = null;

        insert_data.ref_area.ptr (i) -> insert_refs.index (j) = insertndx;
				/* make sure user can access it */
        if bool (insert_status.mode, "01000"b, "0001"b) ^= "01000"b
        then
	do;
	  ercd = error_table_$no_r_permission;
	  if source_file
	  then call com_err_ (ercd, "compose", "^a", fildata.path);
	  else call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
		  "^a", fildata.path);
	  goto find_return;
	end;			/* and theres something in it */
        if insert_status.bit_count > 0
        then fildata.charcount = divide (insert_status.bit_count, 9, 21, 0);
        else
	do;
	  ercd = error_table_$zero_length_seg;
	  if source_file
	  then call com_err_ (ercd, "compose", "^a", fildata.path);
	  else call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line,
		  "^a", fildata.path);
	  goto find_return;
	end;
      end;

    call msf_manager_$open (fildata.dir, fildata.entryname, itsptr, ercd);
    if ercd ^= 0 & ercd ^= error_table_$segknown
    then
      do;
        if source_file
        then call com_err_ (ercd, "compose", "Input file ^a", fildata.path);
        else call comp_report_$ctlstr (3, ercd, ctl_info_ptr, ctl_line, "^a",
	        fildata.path);
        goto find_return;
      end;

    fildata.fcb_ptr = itsptr;
    ercd = 0;

    if ^source_file
    then insert.fcb_ptr = fildata.fcb_ptr;

name_found:
    if ^source_file
    then
      do;
        shared.insert_ptr = insert_data.ptr (insertndx);
        insert_data.index, insert.file.info.fileno = insertndx;
        fildata = insert.file;
      end;

find_return:
    if (shared.bug_mode & dt_sw) | (source_file & sf_sw)
    then call ioa_ ("^5x(get_file_$find: ^a)", fildata.path);

    return;			/* end of find */

open:
  entry (fildataptr, source_file, ercd);

/* LOCAL STORAGE */

    dcl bc	   fixed bin (24);	/* local bitcount */
    dcl chrcount	   fixed bin (21);	/* local character count */

    ercd = 0;

    if (shared.bug_mode & dt_sw) | (source_file & sf_sw)
    then call ioa_ ("get_file_$open: (^[S^;I^] ^a)", source_file, fildata.path)
	    ;

    if ^source_file			/* get status of insert file */
    then
      do;
        insertndx = insert_data.index;	/* copy insert data index */
				/* The char count & dtcm must checked
				   for every insertion of a file
				   because of the possibility of
				   dynamically changing insert files */
        call hcs_$status_long (fildata.dir, fildata.entryname, 1,
	   addr (insert_status), null (), ercd);
        if ercd ^= 0
        then
	do;
	  call comp_report_ (3, ercd, "Getting status of insert file.",
	       addr (ctl.info), ctl_line);
	  goto open_return;
	end;

        if insert_status.bit_count = 0
        then
	do;
	  ercd = error_table_$zero_length_seg;
	  call comp_report_ (3, ercd, "Insert file is empty.",
	       addr (ctl.info), ctl_line);
	  goto open_return;
	end;

        chrcount = divide (insert_status.bit_count, 9, 21, 0);
				/* if critical stuff has changed */
        if insert_data.ptr (insert_data.index) -> insert.charcount ^= chrcount
	   | insert_data.ptr (insert_data.index) -> insert.dtcm
	   ^= insert_status.dtcm
        then
	do;			/* record new data */
	  fildata.charcount = chrcount;
	  insert_data.ptr (insert_data.index) -> insert.dtcm =
	       insert_status.dtcm;	/* clear all labels */
	  insert_data.ptr (insert_data.index) -> insert.label.count = 0;
	end;			/* point to new file */
        shared.insert_ptr = insert_data.ptr (insert_data.index);
        insert.insert_ptr = shared.insert_ptr;
        insert.thrb = ctl.info.fileno;
        insert.callers_name = shared.input_filename;
      end;			/**/
				/* fetch component zero */
    call msf_manager_$get_ptr ((fildata.fcb_ptr), 0, "0"b, itsptr, bc, ercd);
    if ercd ^= 0
    then
      do;
        call com_err_ (ercd, "compose",
	   "msf_manager_ attempting to access"
	   || " component 0 of input file ^a", fildata.path);
        goto open_return;
      end;

    fildata.pointer = itsptr;
    if source_file			/* for source files */
    then fildata.charcount = divide (bc, 9, 21, 0);
				/* copy various data to shared */
    shared.input_dirname = rtrim (fildata.dir);
    shared.input_filename = fildata.refname;
open_return:
    if (shared.bug_mode & dt_sw) | (source_file & sf_sw)
    then call ioa_ ("^5x(get_file<_$open) (^d ^a ^p)", insertndx,
	    fildata.refname, fildata.pointer);

    return;

    dcl dt_sw	   bit (1) static init ("0"b);
dtn:
  entry;
    dt_sw = "1"b;
    return;
dtf:
  entry;
    dt_sw = "0"b;
    return;

    dcl sf_sw	   bit (1) static init ("0"b);
sfn:
  entry;
    sf_sw = "1"b;
    return;
sff:
  entry;
    sf_sw = "0"b;
    return;

allf:
  entry;
    dt_sw, sf_sw = "0"b;
    return;
%page;
%include comp_entries;
%include comp_fntstk;
%include comp_insert;
%include comp_option;
%include comp_shared;
%include comp_text;
%include compstat;
%include status_structures;
%include translator_temp_alloc;

  end comp_get_file_;

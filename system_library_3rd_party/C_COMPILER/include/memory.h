/* Begin include file memory.h	1.2	*/

extern char
	*memccpy(),
	*memchr(),
	*memcpy(),
	*memset();
extern int memcmp();

char *malloc();
void free();
char *realloc();
char * calloc();

/* End include file memory.h */

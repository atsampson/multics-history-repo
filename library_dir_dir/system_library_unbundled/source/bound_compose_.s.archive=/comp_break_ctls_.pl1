/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

/* compose suboutine impelmenting all break forms */

/* format: style2,ind3,ll79,dclind4,idind15,comcol41,linecom */

comp_break_ctls_:
   proc (ctl_index);

/* PARAMETERS */

      dcl ctl_index	     fixed bin;	/* control index */

/* LOCAL STORAGE */

      dcl blank_count    fixed bin;	/* blank pages to be skipped */
      dcl blank_ftr	     char (1020) var;
      dcl blank_hdr	     char (1020) var;
      dcl blkusd	     fixed bin (31);
      dcl break_type     fixed bin;	/* what flavor for .brc */
      dcl fnxt	     fixed bin (21);/* next variable field char */
				/* for debug at exit */
      dcl exit_str	     char (100) var init ("");
      dcl (i, j, k)	     fixed bin;	/* working index */
      dcl mode_str	     char (128) varying;
				/* mode for page & par numbers */
      dcl ms_mode	     char (2);	/* mode extracted from mode string */
      dcl needed	     fixed bin (31);/* space needed by break-need */
      dcl new_col	     fixed bin;	/* target column */
      dcl new_pageno     char (32) var; /* new page number for .brp */
      dcl old_col	     fixed bin;	/* column we're leaving */
      dcl page_incr	     fixed bin;	/* break number increment for break-page */
      dcl save_colno     fixed bin;	/* to save colno around page breaks */
      dcl save_ctl_line  char (1020) var;
      dcl save_ctl_index fixed bin;
      dcl save_input     bit (1);
      dcl 1 save_text_input
		     aligned like text_entry;
      dcl scale	     (1) fixed bin (31) static options (constant)
		     init (1000);
      dcl unscaled	     (1) fixed bin (31) static options (constant) init (1);
      dcl val	     char (32) var; /* value extracted from val string */
      dcl val_str	     char (128) var;/* value for page & par numbers */
      dcl varfld	     char (1020) var;
      dcl vscales	     (7) fixed bin (31) static options (constant)
		     init (12000, 9000, 72000, 2834.65, 12000, 1000, 0);

      dcl (index, length, max, mod, null)
		     builtin;
      dcl end_output     condition;
      dcl continue_to_signal_
		     entry (fixed bin (35));
      dcl ioa_$rsnnl     entry options (variable);

      if shared.bug_mode
      then call ioa_ ("break_ctls: (^d) ""^a""", ctl_index, ctl_line);

      on end_output			/* pass the buck */
         call continue_to_signal_ (0);

      goto ctl_ (ctl_index);

ctl_ (34):			/* ".br" = break-block */
ctl_ (35):			/* ".brb" = break-block */
brb_ctl:
      if shared.table_mode		/* map into a .brf */
      then goto brf_ctl;

      if shared.blkptr ^= null
      then if text.hdr.count > 0 | text.input_line ^= ""
	 then
	    do;			/**/
				/* pending caption? */
	       if text.parms.ftrptr ^= null & ^shared.inserting_hfc
	       then
		do;
		   if text.input_line ^= ""
		   then call comp_break_ (format_break, 0);
		   call comp_title_block_ (text.parms.ftrptr);
		end;

	       call comp_break_ (block_break, 0);
	    end;

      goto return_;

ctl_ (36):			/* ".brc" = break-column */
      if current_parms.keep
      then
         do;
	  call comp_report_ (2, 0, "break-column in a keep block ignored.",
	       addr (ctl.info), ctl_line);
	  goto return_;
         end;

      if shared.table_mode
      then
         do;
	  call comp_report_ (2, 0, "break-column in table mode ignored.",
	       addr (ctl.info), ctl_line);
	  goto return_;
         end;

      if page.parms.cols.count = 0	/* if not multicolumn */
      then new_col = 0;

      else			/* multicolumn */
         do;			/* if a column number is given */
	  if ctl.index <= length (ctl_line)
	  then
	     do;
	        new_col =
		   comp_read_$number (ctl_line, 1, ctl.index, ctl.index,
		   addr (ctl.info), 0);
	        if new_col < 0
	        then
		 do;
		    call comp_report_ (2, 0, "Positive value required.",
		         addr (ctl.info), ctl_line);
		    goto return_;
		 end;		/**/
				/* too big? */
	        if new_col > page.parms.cols.count
	        then
		 do;
		    call comp_report_ (2, 0, "Given column not defined.",
		         addr (ctl.info), ctl_line);
		    goto return_;
		 end;
	     end;

	  else new_col = mod (page.hdr.col_index, page.parms.cols.count) + 1;

join_brc:				/* dont balance this page */
	  if new_col > 0 & page.hdr.col_index > 0
	  then page.parms.cols.bal = "0"b;
         end;

      if shared.blkptr ^= null
      then
         do;			/**/
				/* pending header? */
	  if text.parms.hdrptr ^= null & ^shared.inserting_hfc
	  then call comp_title_block_ (text.parms.hdrptr);

	  blkusd =
	       text.hdr.used
	       + bin (text.input_line ^= "") * text.parms.linespace;
         end;
      else blkusd = 0;		/**/
				/* fill column with WS */
      if page.hdr.col_index > 0 & page.hdr.used > 0 & ^page.parms.cols.bal
	 & col.hdr.net - col.depth_adj > col.hdr.used + blkusd
      then call comp_space_ (col.hdr.net - col.depth_adj - col.hdr.used
	      - blkusd, shared.blkptr, "1"b, "1"b, "1"b, "0"b);

      old_col = page.hdr.col_index;	/* save exit column */
				/* entering or leaving col0? */
      if (old_col = 0 | new_col = 0) & page.hdr.used > 0
      then call comp_break_ (column_break, 0);
				/* * bin (^page.parms.cols.bal));*/

      if page.parms.cols.count > 0	/* if multicolumn and not moving */
	 & old_col ^= 0 & new_col ^= 0/* to/from column 0 */
      then			/* fill intervening columns */
         do;			/* with nontrimmable WS */
	  if old_col < page.parms.cols.count & new_col ^= old_col + 1
	  then
	     do i = old_col + 1 to page.parms.cols.count;
	        page.hdr.col_index = i;
	        shared.colptr = page.column_ptr (i);
	        if col.hdr.net - col.depth_adj > col.hdr.used
	        then call comp_space_ (col.hdr.net - col.depth_adj
		        - col.hdr.used, null, "1"b, "1"b, "1"b, "0"b);
	        call comp_break_ (block_break, 0);
	     end;

	  if new_col > 1
	  then
	     do i = 1 to new_col - 1;
	        page.hdr.col_index = i;
	        shared.colptr = page.column_ptr (i);
	        if col.hdr.net - col.depth_adj > col.hdr.used
	        then call comp_space_ (col.hdr.net - col.depth_adj
		        - col.hdr.used, null, "1"b, "1"b, "1"b, "0"b);
	        call comp_break_ (block_break, 0);
	     end;
         end;

      if new_col <= old_col & new_col ^= 0
      then call comp_break_ (page_break, new_col);

      if old_col = 0		/* if leaving column 0 */
      then
         do i = 1 to page.hdr.col_count;
	  page.column_ptr (i) -> col.hdr.depth = col0.hdr.depth;
         end;

      page.hdr.col_index = new_col;	/* set the new column */
      page.hdr.col_count = max (page.hdr.col_count, new_col);
      shared.colptr = page.column_ptr (new_col);

      if page.hdr.col_index = 0
      then current_parms.measure = page_parms.measure;
      else current_parms.measure = col.parms.measure;

      if shared.bug_mode
      then call ioa_$rsnnl ("col=^d u^f mrg=^f/^f/^f", exit_str, 0,
	      page.hdr.col_index, show (col.hdr.used, 12000),
	      show (col.margin.left, 12000), show (col.parms.measure, 12000),
	      show (col.margin.right, 12000));

      goto return_;

ctl_ (37):			/* ".brf" = break-format */
brf_ctl:
      if shared.blkptr ^= null
      then if length (text.input_line) > 0
	 then call comp_break_ (format_break, 0);

      goto return_;

ctl_ (38):			/* ".brn" = break-need */
      if option.galley_opt		/* ignore it in galley */
      then goto return_;

      else if current_parms.keep	/* needs within keeps not allowed */
      then call comp_report_ (2, 0, "break-need in a keep block ignored.",
	      addr (ctl.info), ctl_line);

      else if shared.ftn_mode		/* needs within ftns not allowed */
      then call comp_report_ (2, 0, "break-need in a footnote block ignored.",
	      addr (ctl.info), ctl_line);

      else
         do;
	  if ctl.index > length (ctl_line)
	  then needed = 12000;	/* default value */
	  else needed =
		  comp_read_$number (ctl_line, vscales, ctl.index,
		  ctl.index, addr (ctl.info), 0);

	  save_input = "0"b;
	  if shared.blkptr ^= null ()
	  then
	     do;
	        save_input = "1"b;
	        save_ctl_line = text.input_line;
	        save_text_input = text.input;
	        needed = needed + text.hdr.used;

	        if length (text.input_line) > 0
	        then needed = needed + 12000;
	     end;

	  if page.hdr.col_index = 0	/* need page space */
	  then
	     do;
	        if shared.bug_mode
	        then call ioa_ ("   (need page ^f have ^f)",
		        dec (divide (needed, 12000, 31, 10), 11, 3),
		        dec (
		        divide (col0.hdr.net - col0.depth_adj
		        - col0.hdr.used - col0.hdr.ftn.usd, 12000, 31, 10),
		        11, 3));

	        if needed
		   > col0.hdr.net - col0.depth_adj - col0.hdr.used
		   - col0.hdr.ftn.usd
	        then
		 do;
		    if ^save_input
		    then call comp_break_ (page_break, 0);

		    else
		       do;
			call comp_break_ (need_break, -2);

			if ^shared.end_output & shared.blkptr = null
			then
			   do;
			      call comp_head_page_ (0);
			      call comp_util_$getblk (0, shared.blkptr,
				 "tx", addr (current_parms), "0"b);
			      text.input = save_text_input;
			      text.input.ptr = addr (text.input_line);
			      text.input_line = save_ctl_line;
			   end;
		       end;
		 end;
	     end;

	  else
	     do;			/* need column space */
	        if shared.bug_mode
	        then call ioa_ ("   (need col ^f have ^f)",
		        dec (divide (needed, 12000, 31, 10), 11, 3),
		        dec (
		        divide (col.hdr.net - col.depth_adj
		        - col.hdr.used - col.hdr.ftn.usd, 12000, 31, 10),
		        11, 3));

	        if needed
		   > col.hdr.net - col.depth_adj - col.hdr.used
		   - col.hdr.ftn.usd
	        then
		 do;
		    new_col =
		         mod (page.hdr.col_index, page.parms.cols.count)
		         + 1;
		    goto join_brc;
		 end;
	     end;
         end;
      goto return_;

ctl_ (39):			/* ".brp" = break-page */
brp_ctl:
      if option.galley_opt		/* in galley, */
      then goto brb_ctl;		/* map into a block break */

      if current_parms.keep		/* no brp's in keep */
      then
         do;
	  call comp_report_ (2, 0, "break-page in a keep block ignored.",
	       addr (ctl.info), ctl_line);
	  goto return_;
         end;

      if shared.ftn_mode		/* no brp's in ftns */
      then
         do;
	  call comp_report_ (2, 0, "break-page in a footnote block ignored.",
	       addr (ctl.info), ctl_line);
	  goto return_;
         end;

      if shared.table_mode
      then
         do;
	  tblfmtndx = tbldata.ndx;	/* save table data */
	  tblfmtptr = tbldata.fmt (tblfmtndx).ptr;
	  save_colno = tblfmt.ccol;
         end;			/**/
				/* anything on the page? */
      if page.hdr.used > 0 | shared.blkptr ^= null ()
      then call comp_break_ (page_break, 0);
				/* if a param is given */
      if ctl.index <= length (rtrim (ctl_line))
      then
         do;
	  if substr (ctl_line, ctl.index) = "e"
	  then
	     do;			/* if param is "even" */
	        if ^page.hdr.frontpage/* and so it the last output page */
	        then call comp_util_$pageno (1000, new_pageno);
	        goto brp_exit;
	     end;

	  else if substr (ctl_line, ctl.index) = "o"
	  then
	     do;			/* if param is "odd" */
	        if page.hdr.frontpage /* and so it the last output page */
	        then call comp_util_$pageno (1000, new_pageno);
	        goto brp_exit;
	     end;

	  else			/* set to given parameter */
	     do;			/* if an increment */
	        if substr (ctl_line, ctl.index, 1) = "+"
	        then
		 do;
		    page_incr =
		         comp_read_$number (ctl_line, scale, ctl.index,
		         ctl.index, addr (ctl.info), 0) + 1000;
		    call comp_util_$pageno (page_incr, new_pageno);
		 end;

	        else
		 do;
		    val_str = before (substr (ctl_line, ctl.index), " ");
		    mode_str = after (substr (ctl_line, ctl.index), " ");

		    i, j = 0;	/* parse the given page number */
		    do while (length (val_str) > 0);
				/* look for a separator */
		       k = search (val_str, "-.()|");

		       if k > 0	/* is there one? */
		       then
			do;
			   val = substr (val_str, 1, k - 1);
			   if substr (val_str, k, 1) = "|"
			   then shared.pagenum.sep (i + 1) = "";
			   else shared.pagenum.sep (i + 1) =
				   substr (val_str, k, 1);
			   val_str = substr (val_str, k + 1);
			   ms_mode = before (mode_str, ",");
			   mode_str = after (mode_str, ",");
			end;

		       else
			do;
			   ms_mode = mode_str;
			   val = val_str;
			   val_str, mode_str = "";
			end;	/**/
				/* if there is no mode, */
		       if ms_mode = ""
		       then
			do;	/* val is what it appears to be */
				/* numeric */
			   if verify (val, "0123456789") = 0
			   then
			      do;
			         shared.pagenum.mode (i + 1) = 0;
				/* ar display */
			         shared.pagenum.nmbr (i + 1) =
				    1000 * bin (val);
			      end;/**/
				/* roman lower */
			   else if verify (val, "ixcmvld") = 0
			   then
			      do;
			         shared.pagenum.mode (i + 1) = 6;
				/* rl display */
			         shared.pagenum.nmbr (i + 1) =
				    read_roman (val, 6);
			      end;/**/
				/* roman upper */
			   else if verify (val, "IXCMVLD") = 0
			   then
			      do;
			         shared.pagenum.mode (i + 1) = 7;
				/* ru display */
			         shared.pagenum.nmbr (i + 1) =
				    read_roman (val, 7);
			      end;/**/
				/* apha lower */
			   else if
			        verify (val,
			        "abcdefghijklmnopqrstuvwxyz") = 0
			   then
			      do;
			         shared.pagenum.mode (i + 1) = 4;
				/* al display */
			         shared.pagenum.nmbr (i + 1) =
				    read_alpha (val, 4);
			      end;/**/
				/* alpha upper */
			   else if
			        verify (val,
			        "ABCDEFGHIJKLMNOPQRSTUVWXYZ") = 0
			   then
			      do;
			         shared.pagenum.mode (i + 1) = 5;
				/* au display */
			         shared.pagenum.nmbr (i + 1) =
				    read_alpha (val, 5);
			      end;
			end;

		       else	/* set index value for */
			do;	/* given mode */
			   shared.pagenum.mode (i + 1) =
			        index (mode_string, ms_mode);
				/* key not found */
			   if shared.pagenum.mode (i + 1) = 0
			   then call comp_report_ (2, 0,
				   "Unknown display mode keyword.",
				   addr (ctl.info), ctl_line);
				/* compute true index value */
			   shared.pagenum.mode (i + 1) =
			        divide (shared.pagenum.mode (i + 1), 2,
			        17);
				/* if a numeric is given */
			   if verify (val, "0123456789") = 0
			   then shared.pagenum.nmbr (i + 1) =
				   1000 * bin (val);

			   else if shared.pagenum.mode (i + 1) = 6
			   then shared.pagenum.nmbr (i + 1) =
				   read_roman (val, 6);

			   else if shared.pagenum.mode (i + 1) = 7
			   then shared.pagenum.nmbr (i + 1) =
				   read_roman (val, 7);

			   else if shared.pagenum.mode (i + 1) = 4
			   then shared.pagenum.nmbr (i + 1) =
				   read_alpha (val, 4);

			   else if shared.pagenum.mode (i + 1) = 5
			   then shared.pagenum.nmbr (i + 1) =
				   read_alpha (val, 5);
			end;

		       shared.pagenum.index, i = i + 1;
		    end;
		 end;		/**/
				/* use -1 here since head_page */
				/* bump it be one */
	        call comp_util_$pageno (-1000, new_pageno);
	        page_header.modified, page.hdr.modified = "0"b;
	     end;

	  page.hdr.pageno = new_pageno;
         end;

brp_exit:
      if page.parms.cols.count > 0	/* if a multi-column page */
      then
         do;
	  page.hdr.col_index = 1;
	  shared.colptr = page.column_ptr (1);
	  current_parms.measure = col.parms.measure;
         end;

      if shared.table_mode
      then
         do;
	  ctl_line = ".tac " || ltrim (char (save_colno));
	  ctl.index = 6;
	  call comp_tbl_ctls_ (tac_ctl_index);
         end;

      if shared.bug_mode
      then call ioa_$rsnnl ("col=^d pageno=^a ^[front^;back^]", exit_str, 0,
	      page.hdr.col_index, page.hdr.pageno, page.hdr.frontpage);

      goto return_;

read_roman:
   proc (rstr, rmode) returns (fixed bin (31));

      dcl rstr	     char (32) var;
      dcl rmode	     fixed bin (8);

      dcl rconstr	     (2) char (7) static options (constant)
		     init ("ivxlcdm", "IVXLCDM");
      dcl rvals	     (7) fixed bin static options (constant)
		     init (1, 5, 10, 50, 100, 500, 1000);
      dcl (ri, rj, rk)   fixed bin;
      dcl rval	     fixed bin (31);

      rval = 0;
      do ri = 1 to length (rstr);	/* which char do we have? */
         rj = index (rconstr (rmode - 5), substr (rstr, ri, 1));
				/* if the char following represents a
				   larger value, then the current char
				   subtracts its value from that one.
				   Luckily, Roman numbering defined only
				   one such subtraction!  */
         if ri < length (rstr)	/* if not the last char */
         then
	  do;
	     rk = index (rconstr (rmode - 5), substr (rstr, ri + 1, 1));
	     if rk > rj
	     then
	        do;
		 rval = rval + rvals (rk) - rvals (rj);
		 ri = ri + 1;
	        end;		/* otherwise, the char simply adds
				   its value */
	     else rval = rval + rvals (rj);
	  end;
         else rval = rval + rvals (rj); /* last char adds it value */
      end;

      return (1000 * rval);

   end read_roman;

read_alpha:
   proc (astr, amode) returns (fixed bin (31));

      dcl astr	     char (32) var;
      dcl amode	     fixed bin (8);

      dcl aconstr	     (2) char (26) static options (constant)
		     init ("abcdefghijklmnopqrstuvwxyz",
		     "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
      dcl ai	     fixed bin;
      dcl aval	     fixed bin (31);

      aval = 0;
      do ai = 1 to length (astr);
         aval = 26 * aval + index (aconstr (amode - 3), substr (astr, ai, 1));
      end;

      return (1000 * aval);

   end read_alpha;

ctl_ (40):			/* ".brs" = break-skip */
      if shared.blkptr ^= null ()
      then if text.parms.keep
	 then
	    do;
	       call comp_report_ (2, 0,
		  "break-skip in a keep block ignored.", addr (ctl.info),
		  ctl_line);
	       goto return_;
	    end;

      if page.hdr.used > 0		/* if anything on the page */
      then call comp_break_ (page_break, 0);

      else if shared.blkptr ^= null ()	/* or anything waiting */
      then if text.hdr.used > 0 | text.input_line ^= ""
	 then call comp_break_ (page_break, 0);

      varfld = substr (ctl_line, ctl.index);

      if varfld ^= ""		/* if there is a variable field */
      then
         do;			/* parameter = "even"? */
	  if substr (varfld, 1, 1) = "e"
	  then
	     do;
	        if ^page.hdr.frontpage
	        then blank_count = 1;
	        else blank_count = 0;
	        varfld = ltrim (substr (varfld, 2));
	     end;			/**/
				/* parameter = "odd"? */
	  else if substr (varfld, 1, 1) = "o"
	  then
	     do;
	        if page.hdr.frontpage
	        then blank_count = 1;
	        else blank_count = 0;
	        varfld = ltrim (substr (varfld, 2));
	     end;

	  else if index ("0123456789+", substr (varfld, 1, 1)) ^= 0
	  then
	     do;
	        blank_count =
		   comp_read_$number (varfld, unscaled, 1, fnxt,
		   addr (ctl.info), 0);
	        varfld = ltrim (substr (varfld, fnxt));
	     end;

	  else blank_count = 1;	/* no control parameter */
         end;

      else blank_count = 1;		/* set default if nothing given */

      if blank_count > 0		/* if any separating pages */
      then
         do;
	  if varfld ^= ""		/* any more in the variable field? */
	  then
	     do;			/* text is first */
				/* get a block for it */
	        call comp_util_$getblk (-1, shared.blank_text_ptr, "bt",
		   addr (current_parms), "0"b);
	        shared.blank_text_ptr -> text.input.quad = quadc;
	        shared.blank_text_ptr -> text.input.rmarg =
		   page.parms.measure;
				/* get the text */
	        shared.blank_text_ptr -> text.input_line =
		   comp_extr_str_ ("1"b, varfld, 1, fnxt, 0,
		   addr (ctl.info));
	        varfld = ltrim (substr (varfld, fnxt));
				/* page header is next */
	        if varfld ^= ""
	        then
		 do;		/* get a block for it */
		    call comp_util_$getblk (-1, shared.blank_header_ptr,
		         "bh", addr (current_parms), "0"b);
		    hfcblk_ptr = shared.blank_header_ptr;
				/* get the title */
		    blank_hdr =
		         comp_extr_str_ ("1"b, varfld, 1, fnxt, 0,
		         addr (ctl.info));
		    if blank_hdr ^= ""
		    then call comp_hft_ctls_$title (shared
			    .blank_header_ptr, addr (ctl), blank_hdr,
			    ctl.linespace);
		    varfld = ltrim (substr (varfld, fnxt));
		 end;		/**/
				/* page footer is last */
	        if varfld ^= ""
	        then
		 do;		/* get a block for it */
		    call comp_util_$getblk (-1, shared.blank_footer_ptr,
		         "bf", addr (current_parms), "0"b);
		    hfcblk_ptr = shared.blank_footer_ptr;
				/* get the title */
		    blank_ftr =
		         comp_extr_str_ ("1"b, varfld, 1, fnxt, 0,
		         addr (ctl.info));

		    if blank_ftr ^= ""
		    then call comp_hft_ctls_$title (shared
			    .blank_footer_ptr, addr (ctl), blank_ftr,
			    ctl.linespace);
		 end;
	     end;

	  if ^option.galley_opt
	  then
	     do;			/* write the blank pages */
	        do i = blank_count to 1 by -1;
		 page.hdr.blankpage = "1"b;
				/* head the page */
		 call comp_head_page_ (0);
				/* write any given text */
		 if shared.blank_text_ptr ^= null ()
		 then if shared.blank_text_ptr -> text.input_line ^= ""
		      then
		         do;	/* space down to page center */
			  call comp_space_ (
			       divide (page.hdr.net, 2, 31, 10),
			       shared.blkptr, "1"b, "1"b, "0"b, "0"b);
			  call comp_break_ (block_break, 0);
			  call comp_util_$getblk (0, shared.blkptr, "tx",
			       addr (current_parms), "0"b);
			  call comp_util_$add_text (shared.blkptr, "1"b,
			       "1"b, "1"b, "0"b,
			       addr (shared.blank_text_ptr -> text.input)
			       );
			  call comp_break_ (page_break, 0);
		         end;
		      else call comp_eject_page_;
		 else call comp_eject_page_;
	        end;

	        page.hdr.blankpage = "0"b;
	     end;

	  if shared.blank_header_ptr ^= null
	  then call comp_util_$relblk (-1, shared.blank_header_ptr);
	  if shared.blank_footer_ptr ^= null
	  then call comp_util_$relblk (-1, shared.blank_footer_ptr);
	  if shared.blank_text_ptr ^= null
	  then call comp_util_$relblk (-1, shared.blank_text_ptr);
         end;

return_:
      if shared.bug_mode
      then call ioa_ ("^5x(break_ctls:^[ ^a^])", exit_str ^= "", exit_str);

      return;
%page;
show:
   proc (datum, scale) returns (fixed dec (11, 3));
      dcl datum	     fixed bin (31);
      dcl scale	     fixed bin (31);

      return (
	 round (dec (round (divide (datum, scale, 31, 11), 10), 11, 4), 3));
   end show;
%page;
%include comp_brktypes;
%include comp_column;
%include comp_ctl_index;
%include comp_entries;
%include comp_fntstk;
%include comp_option;
%include comp_page;
%include comp_shared;
%include comp_table;
%include comp_text;
%include compstat;

   end comp_break_ctls_;

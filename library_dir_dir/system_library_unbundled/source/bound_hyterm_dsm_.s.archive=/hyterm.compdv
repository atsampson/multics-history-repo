/* ***********************************************************
   *                                                         *
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1981 *
   * Copyright, (C) Honeywell Information Systems Inc., 1980 *
   *                                                         *
   *                                                         *
   *********************************************************** */

Wordspace: 3,6,9,SP;
Letterspace: 1;

MediaChars:
010	SELF,	HT	011,	FF	014,	016	SELF,
017	SELF,	STROKE	030,	030	SELF,	ESC	033,
033       SELF,	PLOT      033 "3",	UNPLOT    033 "4",  "!":"~"	SELF,
177	SELF,	NIL	"",
277	"",		/* blind escape */

/*	The font change strings expected by hyterm_writer_ are of the form:
		ooo "h" "w"
	where ooo is the octal representation of the wheel needed.
	      "h" is the character to give to the device
	      "w" is the decimal digit representing the width of a WSP */

Pwheel	001,	HMI6	015,	W6	"6",
Awheel	002,	HMI5	013,	W5	"5",
Ewheel	003,	HMI4	011,	W4	"4",

/* definitions to move all 8 directions */
			 RHLF 033 "D",	/* may NOT be used in PLOT mode */
 BSPRLF 010 033 012,            RLF 033 012,        SPRLF 040 033 012,
 BSP  010,                                         SP  040,
 BSPLF 010 012,                 LF  012,            SPLF 040 012,
			 HLF 033 "U",	/* may NOT be used in PLOT mode */
		/* These are needed by Cleanup: */
037	SELF,	015	SELF,	036	SELF,	011	SELF;

Strokes: 6;

Media:    mASC10,   mASC12,   mASC15,   mAPL10;
NIL       0,        0,        0,        0;
BSP      -6,       -5,       -4,       -6;
010      -6,       -5,       -4,       -6;
HT        0,        0,        0,        0;
011       0,        0,        0,        0;
FF        0,        0,        0,        0;
015       0,        0,        0,        0;
016       0,        =,        =,        0;
017       0,        =,        =,        =;
STROKE    1,	=,	=,	=;
030	1,	=,	=,	=;
ESC      -6,       -5,       -4,       -6;
033      -6,       -5,       -4,       -6;
036       0,        0,        0,        0;
037       0,        0,        0,        0;
SP        6,        5,        4,        6;
"!":"~"   6,        5,        4,        6;
177       0,        =,        =,        0;
Pwheel    0,        =,        =,        0;
Awheel    0,        =,        =,        0;
Ewheel    0,        =,        =,        0;
HMI6      0,        =,        =,        0;
HMI5      0,        =,        =,        0;
HMI4      0,        =,        =,        0;
W6        0,        =,        =,        0;
W5        0,        =,        =,        0;
W4        0,        =,        =,        0;
PLOT      0,        =,        =,        0;
UNPLOT    0,        =,        =,        0;
RLF       0,        =,        =,        0;
RHLF      0,        =,        =,        0;
BSPRLF   -1,        =,        =,       -1;
SPRLF     1,        =,        =,        =;
BSPLF    -1,        =,        =,       -1;
SPLF      1,        =,        =,        1;
LF        0,        =,        =,        0;

View:	vASC10	mASC10;
View:	vASC12	mASC12;
View:	vASC15	mASC15;
View:	vAPL10	mAPL10;

Def: ascii;
"!":"~"	SELF;	010	SELF;

Def: ascii_;
	/* all overstrikes will have underscore last to optimize daisy     */
	/* wheel motion. hyterm_writer_ will separate underscored parts   */
	/* into three strings with all BSP together.  By placing _ last    */
	/* in overstrike, all _ will then be together in the third string. */
	/*  This prints faster.				       */
"!":"~"	SELF BSP "_";
375	"Z" BSP "N";
EM-       (PLOT "_" 4(RLF) UNPLOT 2("_") BSP PLOT 4(LF) UNPLOT "_")=12;
EN-       (PLOT 4(RLF) "_" 4(LF) UNPLOT "_")=6;

240				/* "printing" \040 */ 
 (PLOT BSP "[" 2(SP) "]" BSP UNPLOT SP)=6;
''	/*, 6,*/ """" BSP "_";
``	/*, 6,*/ """" BSP "_";

Def: etc;
STROKE	030;	177	SELF;	277	"";
377 	SP;		/* punctuation space */
375	"Z" BSP "N";	/* square */
417	"(" "T" "M" ")";	/* trademark */
EN 	SP;
EM 	2(SP);
EM_	"_" "_";
EN_	"_";
THIN	(SP)=3;
DEVIT	(SP)=1;

Def: art;
/* MATH SYMBOLS
   1 - [  left square bracket	     | In the plot which follows each string
   2 - ]  right square bracket     | you will see these characters in use:
   3 - {  left curly bracket	     |    .  a position in the "character space"
   4 - }  right curly bracket	     |    o  where a "dit" will appear
   5 - (  left paren	     |    X  initial position of print head
   6 - )  right paren	     |    +  final position of print head
   7 - |  Boolean OR
   8 - ||  concatenate  */

art [tp				/* top parts */ 	
 (PLOT 8(RLF) 4(SP) "." 4(BSP ".") 8(LF ".") UNPLOT SP)=6;
art ]tp	
 (PLOT "." 8(RLF ".") 4(BSP ".") 4(SP) 8(LF) UNPLOT SP)=6;
art {tp	
 (PLOT "." 6(RLF ".") 2(SPRLF ".") 2(SP ".") 2(SP) 8(LF) UNPLOT)=6;
art }tp	
 (PLOT "." 6(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(SP) 8(LF) UNPLOT SP)=6;
art lptp	
 (PLOT "." 6(RLF ".") 2(SPRLF ".") 2(SP ".") 2(SP) 8(LF) UNPLOT)=6;
art rptp	
 (PLOT "." 6(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(SP) 8(LF) UNPLOT SP)=6;
art |tp	
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ||tp	
 (PLOT 2(BSP)  "." 8(RLF ".") 3(SP)  "." 8(LF ".") BSP UNPLOT SP)=6;

art [ht				/* half-top parts */
 (PLOT "." 4(RLF ".") 4(SP ".") 2(SPLF) 2(LF) UNPLOT)=6;
art ]ht	
 (PLOT "." 4(RLF ".") 4(BSP ".") 4(SPLF) UNPLOT SP)=6;
art {ht	
 (PLOT "." 2(RLF ".") 2(SPRLF ".") 2(SP ".") 2(SPLF) 2(LF) UNPLOT)=6;
art }ht	
 (PLOT "." 2(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(SPLF) UNPLOT SP)=6;
art lpht	
 (PLOT "." 2(RLF ".") 2(SPRLF ".") 2(SP ".") 2(SPLF) 2(LF) UNPLOT)=6;
art rpht	
 (PLOT "." 2(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(SPLF) UNPLOT SP)=6;
art |ht	
 (PLOT "." 4(RLF ".") 4(LF) UNPLOT SP)=6;
art ||ht	
 (PLOT 2(BSP)  "." 4(RLF ".") 3(SP)  "." 4(LF ".") BSP UNPLOT SP)=6;

art [md				/* middle parts */
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ]md
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art {md
 (PLOT "." 2(RLF ".") 2(BSPRLF ".") 2(SPRLF ".") 2(RLF ".") 8(LF) UNPLOT SP)=6;
art }md
 (PLOT "." 2(RLF ".") 2(SPRLF ".") 2(BSPRLF ".") 2(RLF ".") 8(LF) UNPLOT SP)=6;
art lpmd
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art rpmd
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art |md
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ||md
 (PLOT 2(BSP)  "." 8(RLF ".") 3(SP)  "." 8(LF ".") 5(SP) UNPLOT)=6;

art [hb				/* half-bottom parts */
 (PLOT 4(SPLF) "." 4(BSP ".") 4(RLF ".") UNPLOT)=0;
art ]hb
 (PLOT 4(BSPLF) "." 4(SP ".") 4(RLF ".") UNPLOT)=0;
art {hb
 (PLOT 4(SPLF) "." 2(BSP ".") 2(BSPRLF ".") 2(RLF ".") UNPLOT)=0;
art }hb
 (PLOT 4(BSPLF) "." 2(SP ".") 2(SPRLF ".") 2(RLF ".") UNPLOT)=0;
art lphb
 (PLOT 4(SPLF) "." 2(BSP ".") 2(BSPRLF ".") 2(RLF ".") UNPLOT)=0;
art rphb
 (PLOT 4(BSPLF) "." 2(SP ".") 2(SPRLF ".") 2(RLF ".") UNPLOT)=0;
art |hb
 (PLOT 4(LF) "." 4(RLF ".") UNPLOT)=0;
art ||hb
 (PLOT 2(BSPLF) 2(LF) "." 4(RLF ".") 3(SP) "." 4(LF ".") BSPRLF 3(RLF) UNPLOT)=0;

art [bt				/* bottom parts */
 (PLOT 4(SP) "." 4(BSP ".") 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ]bt
 (PLOT 4(BSP) "." 4(SP ".") 8(RLF ".") 8(LF) UNPLOT SP)=6;
art {bt
 (PLOT 4(SP) "." 2(BSP ".") 2(BSPRLF ".") 6(RLF ".") 8(LF) UNPLOT SP)=6;
art }bt
 (PLOT 4(BSP) "." 2(SP ".") 2(SPRLF ".") 6(RLF ".") 8(LF) UNPLOT SP)=6;
art lpbt
 (PLOT 4(SP) "." 2(BSP ".") 2(BSPRLF ".") 6(RLF ".") 8(LF) UNPLOT SP)=6;
art rpbt
 (PLOT 4(BSP) "." 2(SP ".") 2(SPRLF ".") 6(RLF ".") 8(LF) UNPLOT SP)=6;
art |bt
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ||bt
 (PLOT 2(BSP)  "." 8(RLF ".") 3(SP)  "." 8(LF ".") 5(SP) UNPLOT)=6;

art [fl				/* filler parts */
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ]fl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art {fl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art }fl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art lpfl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art rpfl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art |fl
 (PLOT "." 8(RLF ".") 8(LF) UNPLOT SP)=6;
art ||fl
 (PLOT 2(BSP)  "." 8(RLF ".") 3(SP)  "." 8(LF ".") 5(SP) UNPLOT)=6;

art [				/* one-highs */
 (PLOT 4(SPLF) "." 4(BSP ".") 12(RLF ".") 4(SP ".") 6(LF) 2(SPLF) UNPLOT)=6;
art ]
 (PLOT 4(BSPLF) "." 4(SP ".") 12(RLF ".") 4(BSP ".") 8(SPLF) 2(SP) UNPLOT)=6;
art {
 (PLOT 4(SPLF) "." 2(BSP ".") 2(BSPRLF ".") 2(RLF ".") 2(BSPRLF ".") 2(SPRLF ".") 2(RLF ".") 2(SPRLF ".") 2(SP ".") 4(LF) 2(SPLF) 2(LF) UNPLOT)=6;
art }
 (PLOT 4(BSPLF) "." 2(SP ".") 2(SPRLF ".") 2(RLF ".") 2(SPRLF ".") 2(BSPRLF ".") 2(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(LF) 4(SPLF) 6(SP) UNPLOT)=6;
art (
 (PLOT 4(SPLF) "." 2(BSP ".") 2(BSPRLF ".") 8(RLF ".") 2(SPRLF ".") 2(SP ".") 4(LF) 2(SPLF) 2(LF) UNPLOT)=6;
art )
 (PLOT 4(BSPLF) "." 2(SP ".") 2(SPRLF ".") 8(RLF ".") 2(BSPRLF ".") 2(BSP ".") 4(LF) 4(SPLF) 6(SP) UNPLOT)=6;
art |
 (PLOT 4(LF) "." 12(RLF ".") 4(LF) 4(SPLF) 2(SP) UNPLOT)=6;
art ||
 (PLOT 2(BSPLF) 2(LF) "." 12(RLF ".") 3(SP) "." 12(LF ".") 4(SPRLF) SP UNPLOT)=6;
art o
 (PLOT BSP "." 2(SP ".") SPRLF "." 4(BSP ".") RLF "." 4(SP ".") BSPRLF "." 2(BSP ".") 3(SPLF) 4(SP) UNPLOT)=6;
art /
 (PLOT BSPLF 2(BSP) "." 9(SPRLF ".") 4(LF) 4(LF) UNPLOT)=6;
art X
 (PLOT 2(BSP) "." 6(SPRLF ".") 6(BSP) "." 6(SPLF ".") 2(SP) UNPLOT)=6;
art d
 (PLOT BSP "." 4(SPRLF ".") 4(LF) "." 4(BSPRLF ".") BSPLF LF "." 6(SP ".") 2(SPLF) UNPLOT)=6;
art m
 (PLOT 2(LF) "." 8(RLF ".") SP "." 8(LF ".") 2(SPRLF) 3(SP) UNPLOT)=6;

art |rul
 (PLOT 6(RLF) "." 8(LF ".") 2(RLF) UNPLOT)=0;
art v
 (PLOT 2(LF)  "." BSPRLF "." 2(SP ".") SPRLF "." 4(BSP ".") BSPRLF "." 6(SP ".") BSPLF 2(BSP) UNPLOT)=0;
art ^
 (PLOT 3(BSPRLF) "." 6(SP ".") BSPRLF "." 4(BSP ".") SPRLF "." 2(SP ".") BSPRLF "." 6(LF) UNPLOT)=0;
art -str
 (PLOT 2(LF) UNPLOT)=0;
art -rul
 (PLOT 5("." SP ".") UNPLOT)=6;
art -stp
 (PLOT 2(RLF) UNPLOT)=0;
art <-
 (PLOT 5(LF) "." 6(RLF ".") BSPLF "." 4(LF ".") BSPRLF "." 2(RLF ".") BSPLF "." 2(SPRLF) SP UNPLOT)=0;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ...o..             */
	/*             X..oo..+            */
	/*              ooo               */
	/*             oooo               */
	/*              ooo               */
	/*               oo               */
	/*                o               */


art ->
 (PLOT 4(LF) BSPLF 2(BSP) "." 6(RLF ".") SPLF "." 4(LF ".") SPRLF "." 2(RLF ".") SPLF "." 2(SPRLF) 4(SP) UNPLOT)=6;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*          o  ......             */
	/*          oo X......+            */
	/*          ooo                   */
	/*          oooo                  */
	/*          ooo                   */
	/*          oo                    */
	/*          o                     */

art D^
 (PLOT 2(LF)  "." 3(SPRLF ".") 3(SPLF ".") 2(RLF) UNPLOT)=6;
art D<
 (SP PLOT 2(LF) "." 4(BSPRLF ".") 4(SPRLF ".") 6(LF) UNPLOT)=6;
art D>
 (PLOT 2(LF)  "." 4(SPRLF ".") 4(BSPRLF ".") 6(LF) UNPLOT SP)=6;
art Dv
 (PLOT 6(RLF) "." 3(SPLF ".") 3(SPRLF ".") 6(LF) UNPLOT)=6;

art \rul	(PLOT 4(RLF) 2(BSPRLF) "." 8(SPLF ".") 2(RLF) UNPLOT)=6;
	/*             ......             */
	/*           o ......             */
	/*            o......             */
	/*             o.....             */
	/*             .o....             */
	/*             ..o...             */
	/*             ...o..             */
	/*             X....o.+            */
	/*                  o             */
	/*                   o            */


art /rul	(PLOT BSPLF LF "." 8(SPRLF ".") 4(LF) BSPLF LF UNPLOT)=6;
	/*             ......             */
	/*             ...... o           */
	/*             ......o            */
	/*             .....o             */
	/*             ....o.             */
	/*             ...o..             */
	/*             ..o...             */
	/*             X.o....+            */
	/*             o                  */
	/*            o                   */


art Clf
 (PLOT 4(LF) 4(LF) 2(SPLF) 4(SP) "." 3(BSP ".") BSPRLF "." 2(BSP ".") 3(BSPRLF ".") RLF "." BSPRLF "." 2(RLF ".") 2(RLF ".") SPRLF "." RLF "." 3(SPRLF ".") 2(SP ".") SPRLF "." 3(SP ".") 4(LF) 2(LF) UNPLOT)=6;
	/*             ......             */
	/*             ...oooo            */
	/*             ooo...             */
	/*            o......             */
	/*           o ......             */
	/*          o  ......             */
	/*          o  ......             */
	/*         o   X......+            */
	/*         o                      */
	/*         o                      */
	/*         o                      */
	/*         o                      */
	/*          o                     */
	/*          o                     */
	/*           o                    */
	/*            o                   */
	/*             ooo                */
	/*                oooo            */


art Crt
 (PLOT 4(LF) 4(LF) 2(BSPLF) 4(BSP) "." 3(SP ".") SPRLF "." 2(SP ".") 3(SPRLF ".") RLF "." SPRLF "." 2(RLF ".") 2(RLF ".") BSPRLF "." RLF "." 3(BSPRLF ".") 2(BSP ".") BSPRLF "." 3(BSP ".") 4(LF) 2(SPLF) 10(SP) UNPLOT)=6;
	/*             ......             */
	/*       oooo  ......             */
	/*           ooo.....             */
	/*             .o....             */
	/*             ..o...             */
	/*             ...o..             */
	/*             ...o..             */
	/*             X....o.+            */
	/*                 o              */
	/*                 o              */
	/*                 o              */
	/*                 o              */
	/*                o               */
	/*                o               */
	/*               o                */
	/*              o                 */
	/*           ooo                  */
	/*       oooo                     */

art c
 (PLOT SPRLF 2(SP) UNPLOT "c" PLOT BSP "." 2(RLF ".") 2(BSPRLF ".") BSP "." BSPRLF "." 2(BSP ".") BSPLF "." BSP "." 2(BSPLF ".") 2(LF ".") 2(SPLF ".") SP "." SPLF "." 2(SP ".") SPRLF "." SP "." SPRLF "." 5(SP) UNPLOT)=12;
	/*             ......             */
	/*             ..ooo.             */
	/*             oo...oo            */
	/*            o...... o           */
	/*           o ......  o          */
	/*           o ......  o          */
	/*           o ......  o          */
	/*            oX...... o    +      */
	/*             oo   oo            */
	/*               ooo              */

art t				/* trademark */
    (PLOT 3(SP) 5("." RLF) 3(BSP) "." 6(SP ".") 5(LF ".") 2(RLF) SPRLF "." 
    2(SPLF ".") 3(SPRLF ".") 4(LF ".") 3(SP) UNPLOT)=12;
          /*       ...............          */
          /*       ...............          */
          /*       ooooooo........          */
          /*       ...o..o.....o..          */
          /*       ...o..oo...oo..          */
          /*       ...o..o.o.o.o..          */
          /*       ...o..o..o..o..          */
          /*       +..o..o.....o..+         */

/* daggar */ 261
 (PLOT "|" RLF "|" 2(RLF)  "-" 3(SPLF) 3(SP) UNPLOT)=6;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             o.....             */
	/*             ......             */
	/*             o.....             */
	/*             o......+            */


/* double daggar */ 301
 (PLOT "|" RLF "|" 2(RLF)  "-" 4(LF)  "-" SPRLF 5(SP) UNPLOT)=6;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             o.....             */
	/*             ......             */
	/*             o.....             */
	/*             o......+            */
	/*             o                  */


/* del */ 304
 (PLOT  "/" SPRLF RLF  "_" SP "_" SPLF LF  "\" 6(SP) UNPLOT)=9;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             .oo...             */
	/*             ......             */
	/*             o...o..   +         */


/* nabla */ 254
 (PLOT  "\" 4(RLF) 2(SPRLF) RLF  "_" SP  "_" 4(LF) 2(SPLF) LF  "/" 5(SP) UNPLOT)=10;
	/*             ..oo..             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             o.....o    +        */

art bxtl	(PLOT 2(LF) "." 6(SP ".") UNPLOT)=6;
art bxt	SP;
art bxtr	(PLOT 2(RLF) UNPLOT)=0;

art bxl	(PLOT 6(RLF) "." 8(LF ".") 6(SP ".") UNPLOT)=6;
art bxx	(PLOT 6(RLF) "." 8(LF ".") 2(RLF) UNPLOT SP)=6;
art bxr	(PLOT 8(RLF) "." 8(LF ".") 2(RLF) UNPLOT)=0;

art bxbl	(PLOT 6(RLF) "." 8(LF ".") 6(SP ".") UNPLOT)=6;
art bxb	(PLOT 6(RLF) "." 8(LF ".") 2(RLF) UNPLOT SP)=6;
art bxbr	(RLF PLOT "." 8(LF ".") 2(RLF) UNPLOT)=0;

art lztl	(PLOT 2(LF) "." 6(SP ".") UNPLOT)=6;
art lztr	(PLOT 6(SP ".") 4(BSP) 2(BSPRLF) UNPLOT)=0;
art lzl	(PLOT BSPLF LF "." 8(SPRLF ".") 5(LF) BSPLF UNPLOT)=6;
art lzr	(PLOT 4(RLF) 2(BSPRLF) "." 8(SPLF ".") 2(RLF) UNPLOT)=6;
art lzbl	(PLOT 2(BSP) 6(BSPRLF) "." 8(SPLF ".") 6(SP ".") UNPLOT)=6;
art lzbr	(PLOT 6(SP ".") 8(SPRLF ".") 8(BSP) 6(BSPLF) UNPLOT)=0;

''	/*, 6,*/ """";
``	/*, 6,*/ """";

Def: UCbold;
"a" "A"; "b" "B"; "c" "C"; "d" "D"; "e" "E"; "f" "F"; "g" "G";
"h" "H"; "i" "I"; "j" "J"; "k" "K"; "l" "L"; "m" "M"; "n" "N";
"o" "O"; "p" "P"; "q" "Q"; "r" "R"; "s" "S"; "t" "T"; "u" "U";
"v" "V"; "w" "W"; "x" "X"; "y" "Y"; "z" "Z";

Def: OSbold;
"!":"~" SELF BSP SELF BSP SELF;

Def: OSboldital;
"!":"^" SELF BSP SELF BSP SELF BSP "_";
"_" SELF BSP SELF BSP SELF;
"`":"~" SELF BSP SELF BSP SELF BSP "_";

Font: asc10 vASC10;
ref:	ascii;	ref:	etc;	ref:	art;
EN- "-";
EM- (PLOT 3("-" 4(SP)) UNPLOT)=12;
EM_ (PLOT RLF RLF 2("_" 5(SP)) LF LF UNPLOT )=12;
EN_ (PLOT RLF RLF "_" LF LF UNPLOT SP)=6;
422 """";
421 """";

''	/**/ """";
``	/**/ """";
240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 5(SP) UNPLOT)=6;

Font: apl vAPL10;
ref:	ascii;	ref:	etc;	ref:	art;

Font: ASC10 vASC10;
ref: ascii;   ref: etc;   ref: art;   ref: UCbold;

Font: ASC10OS vASC10;
ref:	etc;	ref:	art;	ref:	OSbold;

Font: asc10_ vASC10;	/* pick all the artwork */
ref:	etc;	ref:	art;
	/* all overstrikes will have underscore last to optimize daisy     */
	/* wheel motion. hyterm_writer_ will separate underscored parts   */
	/* into three strings with all BSP together.  By placing _ last    */
	/* in overstrike, all _ will then be together in the third string. */
	/*  This prints faster.				       */
"!":"~"	/*, 6,*/ SELF BSP "_";
375	/*, 6,*/ "Z" BSP "N";
EM-	
 (PLOT 3("-" 4(SP)) UNPLOT 2(BSP) PLOT 2("_" 5(SP)) UNPLOT)=12;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             X......      +      */

EN-	"-" BSP "_";

240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 5(SP) UNPLOT)=6;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*            oX.o....+            */

''	/*, 6,*/ """" BSP "_";
``	/*, 6,*/ """" BSP "_";

Font: ASC10_ vASC10;
ref:	etc;	ref:	art;	ref:	ascii_;
"a" 101 BSP "_";	"b" "B" BSP "_";	"c" "C" BSP "_";	"d" "D" BSP "_";	
"e" "E" BSP "_";	"f" "F" BSP "_";	"g" "G" BSP "_";	"h" "H" BSP "_";	
"i" "I" BSP "_";	"j" "J" BSP "_";	"k" "K" BSP "_";	"l" "L" BSP "_";	
"m" "M" BSP "_";	"n" "N" BSP "_";	"o" "O" BSP "_";	"p" "P" BSP "_";	
"q" "Q" BSP "_";	"r" "R" BSP "_";	"s" "S" BSP "_";	"t" "T" BSP "_";	
"u" "U" BSP "_";	"v" "V" BSP "_";	"w" "W" BSP "_";	"x" "X" BSP "_";
"y" "Y" BSP "_";	"z" "Z" BSP "_";	

Font: ASC10_OS vASC10;
ref:	etc;	ref:	art;	ref:	OSboldital;

Font: asc12 vASC12;
ref:	ascii;
ref:	etc;	ref:	art;
EN- "-";
EM- (PLOT 3("-" 3(SP)) SP UNPLOT)=10;

240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 5(SP) UNPLOT)=6;

Font: ASC12 vASC12;
ref:	etc;	ref:	art;	ref:	UCbold;

Font: asc12_ vASC12;	/* pick all the artwork */
ref:	etc;	ref:	art;
"!":"~"	 SELF BSP "_";
375	 "Z" BSP "N";
EM- (PLOT 3("-" 3(SP)) SP UNPLOT 2(BSP) 2("_"))=10;
EN-	 "-" BSP "_";

240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 4(SP) UNPLOT)=5;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*            oX.o...._.             */


''	 """" BSP "_";
``	 """" BSP "_";

Font: ASC12_ vASC12;
ref:	etc;	ref:	art;
"a" "A" BSP "_";
"b" "B" BSP "_";
"c" "C" BSP "_";
"d" "D" BSP "_";
"e" "E" BSP "_";
"f" "F" BSP "_";
"g" "G" BSP "_";
"h" "H" BSP "_";
"i" "I" BSP "_";
"j" "J" BSP "_";
"k" "K" BSP "_";
"l" "L" BSP "_";
"m" "M" BSP "_";
"n" "N" BSP "_";
"o" "O" BSP "_";
"p" "P" BSP "_";
"q" "Q" BSP "_";
"r" "R" BSP "_";
"s" "S" BSP "_";
"t" "T" BSP "_";
"u" "U" BSP "_";
"v" "V" BSP "_";
"w" "W" BSP "_";
"x" "X" BSP "_";
"y" "Y" BSP "_";
"z" "Z" BSP "_";

Font: asc15 vASC15;
ref:	ascii;
ref:	etc;	ref:	art;

`` (PLOT BSP "`" 3(SP) "`" 2(SP) UNPLOT)=4;
'' (PLOT BSP "'" 3(SP) "'" 2(SP) UNPLOT)=4;
EM- (PLOT 2("-" 4(SP)) UNPLOT)=8;
EN- "-";

240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 3(SP) UNPLOT)=4;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*            oX.o..._..             */



Font: ASC15 vASC15;
ref:	etc;	ref:	art;	ref:	UCbold;

Font: asc15_ vASC15;	/* pick all the artwork */
ref:	etc;	ref:	art;
"!":"~"  SELF BSP "_";
''  """" BSP "_";
EM_ 2("_");
EN_  "_";
375  "Z" BSP "N";
EM- (PLOT 2("-" 4(SP)) UNPLOT 2(BSP) 2("_"))=8;
EN- "-" BSP "_";

240		/* "printing" \040 */ 
 (PLOT BSP  "[" 2(SP)  "]" 3(SP) UNPLOT)=4;
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*             ......             */
	/*            oX.o..._..             */


``  """" BSP "_";
Font: ASC15_ vASC15;
"a" 101 BSP "_";
"b" "B" BSP "_";
"c" "C" BSP "_";
"d" "D" BSP "_";
"e" "E" BSP "_";
"f" "F" BSP "_";
"g" "G" BSP "_";
"h" "H" BSP "_";
"i" "I" BSP "_";
"j" "J" BSP "_";
"k" "K" BSP "_";
"l" "L" BSP "_";
"m" "M" BSP "_";
"n" "N" BSP "_";
"o" "O" BSP "_";
"p" "P" BSP "_";
"q" "Q" BSP "_";
"r" "R" BSP "_";
"s" "S" BSP "_";
"t" "T" BSP "_";
"u" "U" BSP "_";
"v" "V" BSP "_";
"w" "W" BSP "_";
"x" "X" BSP "_";
"y" "Y" BSP "_";
"z" "Z" BSP "_";

Font: ASC15OS vASC15;
wordspace: 2,4,8,SP;
ref:	etc;	ref:	art;	ref:	OSbold;

Font: ASC15_OS vASC15;
wordspace: 2,4,8,SP;
ref:	etc;	ref:	art;	ref:	OSboldital;

Font: elite vASC12;
Font: elite_ vASC12;
Font: ELITE vASC12;
Font: ELITE_ vASC12;
Font: elite15 vASC15;
Font: elite15_ vASC15;
Font: ELITE15 vASC15;
Font: ELITE15_ vASC15;


Size: onesize, 7.2;
MinLead: 1.5;
MinSpace: 1.2;
MaxPageWidth: 950.4;
Outproc: hyterm_writer_;
DefaultMargs: 48,24,24,48;
Cleanup: UNPLOT 033 037 015 033 036 011;
Comment:
" Type Wheel Identification
 1 - 38101-01	PICA 10
 2 - 38510	APL 10
 3 - 38102-01	ELITE 12
DB: hyterm_writer_$display
";
DevName: "hyterm";
DevClass: "diablo";
Interleave: on;

Device: hyterm, HYTERM, diablo1620;	init: text 7.2;
viewselect: vASC10 Pwheel HMI6 W6,
	  vASC12 Ewheel HMI5 W5,
	  vASC15 Pwheel HMI4 W4,
	  vAPL10 Awheel HMI6 W6;
attach: "syn_ user_output";

family:	centuryschoolbook, cs,	helvetica, h;
  member: /medium, /m, /,	/roman, /r	asc10;
  member: /italic, /i,	/mediumitalic, /mi	asc10_;
  member: /bold, /b,	/boldroman, /br	ASC10OS;
  member: /bolditalic, /bi			ASC10_OS;

family:	pica10;
   member: /medium, /m, /,	/roman, /r	asc10;
   member: /italic, /i,	/mediumitalic, /mi	asc10_;
   member: /bold, /b,	/boldroman, /br	ASC10OS;
   member: /bolditalic, /bi			ASC10_OS;
   member:    /caps		ASC10;
   member:    /caps_	ASC10_;

bachelor: ascii, l4font, l3exact, l4exact, text, footnote, footref	asc10;
bachelor: l0exact, CSR, HR, typ, small_typ, ascii9		asc10;
bachelor: l0font, l3font, ASCII				ASC10;
bachelor: bold, CSBR, HBR, HBBl				ASC10OS;
bachelor: italic, l2font, l1exact, l2exact, CSI, HmI, ascii_	asc10_;
bachelor: l1font, ASCII_					ASC10_;
bachelor: CSBI, HBI, bolditalic				ASC10_OS;
bachelor: apl, APL 						apl;

family:	pica_12;
   member: /medium, /m, /,	/roman, /r	asc12;
   member: /italic, /i,	/mediumitalic, /mi	asc12_;
   member: /bold, /b,	/boldroman, /br	ASC12;
   member: /bolditalic, /bi			ASC12_;
   member:    /caps		ASC12;
   member:    /caps_	ASC12_;

bachelor:	pica12	asc12;
bachelor:	pica12_	asc12_;
bachelor:	PICA12	ASC12;
bachelor:	PICA12_	ASC12_;

Device: hyterm_draft;	init: text 7.2;
viewselect: vASC10 Pwheel HMI6 W6,
	  vASC12 Ewheel HMI5 W5,
	  vASC15 Pwheel HMI4 W4,
	  vAPL10 Awheel HMI6 W6;
attach: "syn_ user_output";

family:	centuryschoolbook, cs,	helvetica, h;
  member: /medium, /m, /,	/roman, /r	asc10;
  member: /italic, /i,	/mediumitalic, /mi	asc10_;
  member: /b ,/bold,	/boldroman, /br	ASC10OS;
  member: /bi,/bolditalic			ASC10_OS;

family:	pica10, pica;
   member: /medium, /m, /,	/roman, /r	asc10;
   member: /italic, /i,	/mediumitalic, /mi	asc10_;
   member: /bold, /b,	/boldroman, /br	ASC10OS;
   member: /bolditalic, /bi			ASC10_OS;
   member:    /caps		ASC10;
   member:    /caps_	ASC10_;

bachelor: text, footnote, footref, CSR, HR, ascii, typ, small_typ, ascii9	asc10;
bachelor: l0exact						asc10;
bachelor: l0font, ASCII					ASC10;
bachelor: bold, CSBR, HBR, HBBl, l4font, l3exact, l4exact		ASC10OS;
bachelor: italic, l2font, l1exact, l2exact, CSI, HmI, ascii_	asc10_;
bachelor: l1font, ASCII_					ASC10_;
bachelor: CSBI, HBI, l3font, bolditalic				ASC10_OS;
bachelor: apl, APL 						apl;

family:	pica_12;
   member: /medium, /m, /,	/roman, /r	asc12;
   member: /italic, /i,	/mediumitalic, /mi	asc12_;
   member: /bold, /b,	/boldroman, /br	ASC12;
   member: /bolditalic, /bi			ASC12_;
   member:    /caps		ASC12;
   member:    /caps_	ASC12_;

bachelor:	pica12	asc12;
bachelor:	pica12_	asc12_;
bachelor:	PICA12	ASC12;
bachelor:	PICA12_	ASC12_;

Device: vhyt;		init: text 7.2;
viewselect: vASC12 Ewheel HMI5 W5,
	  vASC15 Pwheel HMI4 W4;
devname: "vhyt";
devclass: "photocomp";
attach: "syn_ user_output";

family:	centuryschoolbook, cs;
  member:	/medium, /m, /,	/roman, /r	asc15;
  member:	/italic, /i,	/mediumitalic, /mi	asc15_;
  member:	/bold, /b,	/boldroman, /br	ASC15;
  member:	/bolditalic, /bi			ASC15_;

bachelor:	CSR, HR, text, l4font, footnote, footref, l0exact		asc15;
bachelor:	l3font, bold, CSBR, HBR, HBBl				ASC15OS;
bachelor:	italic, l2font, CSI, HmI				asc15_;
bachelor:	l0font, l1font, CSBI, HBI, bolditalic			ASC15_OS;

family:	pica;
   member: /medium, /m, /,	/roman, /r	asc12;
   member: /italic, /i,	/mediumitalic, /mi	asc12_;
   member: /bold, /b,	/boldroman, /br	ASC12;
   member: /bolditalic, /bi			ASC12_;
   member:    /caps		ASC12;
   member:    /caps_	ASC12_;

bachelor:	pica12,  ascii			asc12;
bachelor:	pica12_, ascii_			asc12_;
bachelor:	PICA12,  ASCII			ASC12;
bachelor:	PICA12_, ASCII_			ASC12_;

Device: nec5525, spinwriter like hyterm;
devname: "nec5525";

&command_line on
&if [equal &n 0] &then &goto explain_this_exec_com
&if [and [equal &n 2] [equal &1 "DuMmY_ArG"]] &then &goto invoke_mrpg
&print run_mrpg_examples.ec:  Argument error.
&print ^23xNo arguments should be supplied when this exec_com is initially invoked.
&print ^23xNeed to supply a number when asked to enter example number.
&quit


&label explain_this_exec_com
&print ^2/A menu of available examples will be displayed.
&print Typing in the number of one of the examples causes this exec_com to:
&print ^5xA.   Extract the selected example from an archive,
&print ^5xB.   Invoke MRPG for that example,
&print ^5xC.   In most cases, execute the example.
&print Information about any report segments produced will be displayed.
&print You may then select another example, if you wish.


&label display_menu
&print MENU:^/
&print No.  Example
&print ---  ------------------
&print
&print 1    filing_cabinet
&print 2    two_reports
&print 3    hold_and_sort
&print 4    begin_hold_assign
&print 5    negative_parameter


&label select_example
&detach
&command_line off
exec_com &ec_name DuMmY_ArG [response [format_line "^/Enter the number of the example you want to run.^/"]]
&command_line on

&  Control returns here when the second-level invocation of this exec_com ends.

&if [query [format_line "^2/Do you want to run another example?^/"]]
&then &else &quit
&if [query [format_line "Do you remember its number?  (Type  no  to see the menu again.)^/"]]
&then &goto select_example
&else &goto display_menu


&			 first-level invocation of this exec_com is above this line.
&---------------------------------------------------------------------------------------------
&			second-level invocation of this exec_com is below this line.


&label invoke_mrpg
&command_line on
&print
&if [equal &2 1] &then &goto menu_1
&if [equal &2 2] &then &goto menu_2
&if [equal &2 3] &then &goto menu_3
&if [equal &2 4] &then &goto menu_4
&if [equal &2 5] &then &goto menu_5
&--&if [equal &2 6] &then &goto menu_6
&--&if [equal &2 7] &then &goto menu_7
&--&if [equal &2 8] &then &goto menu_8

&print run_mrpg_examples.ec:  Menu number selected (&2) is not in the menu.
&quit


&label menu_1
archive xf >unbundled>mrpg_examples filing_cabinet.mrpg filing_cabinet.mrpg.input
mrpg filing_cabinet -table
&print ^/filing_cabinet has been generated and compiled.
&print It may be executed by typing either
&print ^10xfiling_cabinet
&print ^5xor
&print ^10xfiling_cabinet -file
&print If the   -file   is omitted, the report will be displayed on your terminal.
&print If the   -file   is typed, the report will be written into a segment named filing_cabinet.report
&print If the   -file   choice is selected, then:
&print ^5xTyping      print filing_cabinet.report     will display the report on your terminal.
&print ^5xTyping     dprint filing_cabinet.report     will send the report to a line printer.
&quit


&label menu_2
archive xf >unbundled>mrpg_examples two_reports.mrpg two_reports.mrpg.input
mrpg two_reports -table
two_reports
&print ^/two_reports has been generated, compiled, and executed.
&print The output is in   two_reports.file_one.report   and   two_reports.file_two.report
&quit


&label menu_3
archive xf >unbundled>mrpg_examples hold_and_sort.mrpg hold_and_sort.mrpg.input
mrpg hold_and_sort -table
hold_and_sort
&print ^/hold_and_sort has been generated, compiled, and executed.
&print Print hold_and_sort.report
&quit


&label menu_4
archive xf >unbundled>mrpg_examples begin_(1 8)_hold_assign.mrpg begin_9_hold_assign.mrpg.input
mrpg begin_(1 8)_hold_assign -table
begin_(1 8)_hold_assign
&print ^/begin_1_hold_assign has been generated, compiled, and executed.
&print   begin_8_hold_assign has been generated, compiled, and executed.
&print Print begin_1_hold_assign.in.report  and  begin_1_hold_assign.lv.report
&print Print begin_8_hold_assign.in.report  and  begin_8_hold_assign.lv.report
&quit


&label menu_5
archive xf >unbundled>mrpg_examples negative_parameter.mrpg negative_parameter.mrpg.input
mrpg negative_parameter -table
&print ^2/negative_parameter has been generated and compiled.
&print    Execute the object program without supplying any parameter.^2/
negative_parameter
&print ^2/Execute it with a positive argument.^2/
negative_parameter 5
&print ^2/Now execute it using the "negative" argument.^2/
negative_parameter 6 -neg
&print ^2/The parameters can be reversed.^2/
negative_parameter -neg 7
&quit
